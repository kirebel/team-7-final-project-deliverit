const dateFormatter = (dateString) => {
  if (dateString) {
    const dateStringArr = dateString.split('/');
    const year = dateStringArr.pop();
    dateStringArr.unshift(year);
    return dateStringArr.join('-');
  }

  return dateString;
};

export default dateFormatter;
