import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Main from './Components/Main/Main.jsx';
import GuardedRoute from './providers/GuardedRoute';
import { getUserFromToken } from './Common/token';
import { useEffect, useState } from 'react';
import AuthContext from './providers/AuthContext';
import React from 'react';
import EmployeeView from './Components/EmployeeView/EmployeeView';
import CustomerView from './Components/CustomerView/CustomerView';
import MyProfileView from './Components/CustomerView/MyProfileView';
import Footer from './Components/Footer/Footer';
import MapsView from './Components/MapsView/MapsView';
import CustomerSupport from './Components/CustomerView/CustomerSupport';

function App() {
  const [user, setUser] = useState(
    getUserFromToken(localStorage.getItem('token'))
  );

  useEffect(() => {
    if (!user) return;

    const timer = setTimeout(() => {
      setUser(null);
      localStorage.removeItem('token');
    }, 3600 * 60 * 60);

    return () => clearTimeout(timer);
  }, [user]);

  return (
    <>
      <main className='all-wrapper'>
        <AuthContext.Provider value={{ user, setUser }}>
          <BrowserRouter>
            {!user ? (
              <Switch>
                <Route exact path='*' component={Main} />
              </Switch>
            ) : (
              <Switch>
                <GuardedRoute
                  component={MapsView}
                  auth={user.role === 'customer'}
                  path='/maps'
                />

                <GuardedRoute
                  component={CustomerSupport}
                  auth={user.role === 'customer' || user.role === 'support'}
                  path='/customer-support'
                />

                <GuardedRoute
                  component={MyProfileView}
                  auth={user.role === 'customer'}
                  path='/my-profile'
                />

                {user.role === 'employee' ? (
                  <GuardedRoute
                    component={EmployeeView}
                    auth={user.role === 'employee'}
                    path='*'
                  />
                ) : user.role === 'customer' ? (
                  <GuardedRoute
                    component={CustomerView}
                    auth={user.role === 'customer'}
                    path='*'
                  />
                ) : (
                  <GuardedRoute
                    component={CustomerSupport}
                    auth={user.role === 'support'}
                    path='*'
                  />
                )}
              </Switch>
            )}
          </BrowserRouter>
        </AuthContext.Provider>
      </main>
      <Footer />
    </>
  );
}
export default App;
