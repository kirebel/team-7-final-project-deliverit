import { Button } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import { addParcelToShipment } from '../../Requests/Parcels-requests';
import { getShipments } from '../../Requests/Shipments-requests';
import { deleteParcel } from '../../Requests/Parcels-requests';
import PropTypes from 'prop-types';

const WaitingParcels = ({
  setWaitingParcels,
  waitingParcels,
  attachParcelToShipment,
  setAttachParcelToShipment,
}) => {
  const [updateParcel, setUpdateParcel] = useState({
    weight: '',
    home_delivery: '',
    user_id: '',
    shipment_id: '',
    warehouse_id: '',
    category_id: '',
    isDeleted: 0,
  });

  const [currentButtonIndex, setCurrentButtonIndex] = useState(0);

  const [shipments, setShipments] = useState([]);

  useEffect(() => {
    getShipments('').then((data) => setShipments([...data]));
  }, []);

  const setParcelDeleted = (parcelId) => {
    deleteParcel(parcelId).then((response) => {
      setWaitingParcels([
        ...waitingParcels.map((element) =>
          element.id === parcelId ? { ...element, isDeleted: 1 } : element
        ),
      ]);
    });
  };

  const updateWaitingParcelInfo = (parcelId) => {
    addParcelToShipment(updateParcel, parcelId).then((response) => {
      setWaitingParcels([
        ...waitingParcels.map((element) =>
          element.id === parcelId ? { ...element, ...response } : element
        ),
      ]);
      setShipments([
        ...shipments.map((element) =>
          element.shipment_id === response.shipment_id
            ? { ...element, parcels_count: element.parcels_count + 1 }
            : element
        ),
      ]);
    });
  };

  const handleShipmentsDropdown = (e, index) => {
    if (e.target.value.length !== 0) {
      const [shipmentId, warehouseId] = e.target.value.split(',');

      setAttachParcelToShipment({
        shipment_id: +shipmentId,
        warehouse_id: +warehouseId,
      });
    }
    setCurrentButtonIndex(index);
  };

  const handleUpdateButton = (parcel) => {
    setUpdateParcel({
      weight: parcel.weight,
      home_delivery: parcel.home_delivery,
      user_id: parcel.user_id,
      shipment_id: attachParcelToShipment.shipment_id,
      warehouse_id: attachParcelToShipment.warehouse_id,
      category_id: parcel.category_id,
    });
  };

  const handleAddButton = (parcel) => {
    updateWaitingParcelInfo(parcel.id);
    setAttachParcelToShipment({
      shipment_id: '',
      warehouse_id: '',
    });
    setUpdateParcel({
      weight: '',
      home_delivery: '',
      user_id: '',
      shipment_id: '',
      warehouse_id: '',
      category_id: '',
    });
  };

  return (
    <div className='waiting-parcels-div'>
      <table className='waiting-parcels-wrapper'>
        <tbody>
          <tr>
            <th colSpan='7' className='form-titles'>
              Waiting Parcels
            </th>
          </tr>
          <tr>
            <th>#</th>
            <th>customer</th>
            <th>category</th>
            <th>weight</th>
            <th className='shipment-dropdown-column'>shipment </th>
            <th>home delivery</th>
            <th></th>
          </tr>
          {waitingParcels
            .sort((a, b) => a.id - b.id)
            .filter(
              (element) => !element.shipment_id && element.isDeleted !== 1
            )
            .map((parcel, index) => (
              <tr key={parcel.id}>
                <td>{parcel.id}</td>
                <td>
                  {parcel.user}&nbsp;
                  {parcel.customer_country},{parcel.customer_city}
                </td>
                <td>{parcel.category}</td>
                <td>{parcel.weight} kg</td>
                <th>
                  <select onClick={(e) => handleShipmentsDropdown(e, index)}>
                    <option value='default'>Choose here</option>
                    {shipments.map((element, index) => {
                      if (
                        element.shipment_status === 1 &&
                        element.parcels_count <= 9
                      ) {
                        return (
                          <option
                            onClick={() => setCurrentButtonIndex(index)}
                            key={element.shipment_id}
                            value={`${element.shipment_id},${element.destination_warehouse_id}`}
                          >
                            #{element.shipment_id}&nbsp;{element.to_city_name},
                            {element.to_country_name}
                            {element.shipment_departure_time
                              ? new Date(element.shipment_departure_time)
                                  .toLocaleString()
                                  .slice(0, 8)
                              : null}
                            ,&nbsp;
                            {element.parcels_count >= 10
                              ? 'full'
                              : `parcels to full:${10 - element.parcels_count}`}
                          </option>
                        );
                      }
                      return null;
                    })}
                  </select>
                </th>

                <th>{parcel.home_delivery !== 1 ? 'yes' : 'no'}</th>
                <th>
                  {!updateParcel.home_delivery ||
                  index !== currentButtonIndex ? (
                    <>
                      <Button
                        style={{ marginTop: '5px', marginRight: '3px' }}
                        disabled={
                          attachParcelToShipment.shipment_id === '' ||
                          index !== currentButtonIndex ||
                          isNaN(attachParcelToShipment.shipment_id)
                        }
                        onClick={() => handleUpdateButton(parcel)}
                      >
                        Update
                      </Button>
                      <Button
                        onClick={() => setParcelDeleted(parcel.id)}
                        style={{ marginTop: '5px' }}
                      >
                        Delete
                      </Button>
                    </>
                  ) : (
                    <Button
                      variant='success'
                      onClick={() => handleAddButton(parcel)}
                    >
                      Add
                    </Button>
                  )}
                </th>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

WaitingParcels.propTypes = {
  setWaitingParcels: PropTypes.func,
  waitingParcels: PropTypes.array,
  attachParcelToShipment: PropTypes.object,
  setAttachParcelToShipment: PropTypes.func,
};

export default WaitingParcels;
