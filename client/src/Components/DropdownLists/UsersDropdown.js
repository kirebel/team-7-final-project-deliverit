import React, { useState, useEffect } from 'react';
import { getUsers } from '../../Requests/Users-requests';
import PropTypes from 'prop-types';

const UsersDropdown = ({
  updateInfo,
  createParcelDropdownValues,
  updateDropdown,
}) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers('').then((data) => setUsers(data));
  }, []);

  const handleDropdown = (e) => {
    const selectedValue = e.target.value.split(',');
    updateDropdown({ customerDropdown: e.target.value });
    updateInfo('user_id', +selectedValue[0]);
    updateInfo('customer_city', selectedValue[1]);
    updateInfo('customer_country', selectedValue[2]);
  };

  return (
    <>
      <select
        style={{ width: '300px' }}
        value={createParcelDropdownValues.customerDropdown}
        onChange={(e) => handleDropdown(e)}
      >
        <option value='default'>Choose here</option>
        {users
          .filter((user) => user.role !== 'support' && user.role !== 'employee')
          .map((element) => (
            <option
              key={element.id}
              value={`${element.id},${element.country},${element.city}`}
            >
              {element.id}, {element.first_name}&nbsp;{element.last_name}&nbsp;
              {element.country},{element.city}
            </option>
          ))}
      </select>
    </>
  );
};

UsersDropdown.propTypes = {
  updateInfo: PropTypes.func,
  createParcelDropdownValues: PropTypes.object,
  updateDropdown: PropTypes.func,
};

export default UsersDropdown;
