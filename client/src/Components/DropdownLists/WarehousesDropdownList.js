import React, { useEffect, useState } from 'react';
import { getWarehouses } from '../../Requests/Warehouses-requests';
import PropTypes from 'prop-types';

const WarehousesDropdown = ({
  setWarehouse,
  checkBoxStatus,
  dropdownValues,
  setDropdownValues,
}) => {
  const [warehouses, setWarehouses] = useState([]);

  useEffect(() => {
    getWarehouses().then((data) => setWarehouses(data));
  }, []);

  const handleDropdown = (e) => {
    setDropdownValues({ warehouseDropdown: e.target.value });
    if (checkBoxStatus && e.target.value !== 'default') {
      setWarehouse('warehouse_id=' + e.target.value + '&');
    } else {
      setWarehouse('');
    }
  };

  return (
    <select
      className='dropdown'
      value={dropdownValues.warehouseDropdown}
      onChange={(e) => handleDropdown(e)}
    >
      <option value='default'>Choose here</option>
      {warehouses.map((element) => (
        <option key={element.warehouse_id} value={element.warehouse_id}>
          {element.warehouse_city},{element.warehouse_country}
        </option>
      ))}
    </select>
  );
};

WarehousesDropdown.propTypes = {
  setWarehouse: PropTypes.func,
  checkBoxStatus: PropTypes.bool,
  dropdownValues: PropTypes.object,
  setDropdownValues: PropTypes.func,
};

export default WarehousesDropdown;
