import React, { useEffect, useState } from 'react';
import { getCountries } from '../../Requests/Countries-requests';
import PropTypes from 'prop-types';

const CountriesDropdownList = ({ dispatchSearchTerms }) => {
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    getCountries().then((data) => setCountries(data));
  }, []);

  const handleDropdown = (e) => {
    if (e.target.value !== 'default') {
      dispatchSearchTerms({
        type: 'input-country',
        payload: `country_id=${e.target.value}`,
      });
    } else {
      dispatchSearchTerms({
        type: 'input-country',
        payload: '',
      });
    }
  };

  return (
    <select onChange={(e) => handleDropdown(e)}>
      <option value='default'>Choose here</option>
      {countries.map((element) => (
        <option key={element.country_id} value={element.country_id}>
          {element.country_name}
        </option>
      ))}
    </select>
  );
};

CountriesDropdownList.propTypes = {
  dispatchSearchTerms: PropTypes.func,
};

export default CountriesDropdownList;
