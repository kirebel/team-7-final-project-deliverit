import React from 'react';
import PropTypes from 'prop-types';

const ParcelStatus = ({ status }) => {
  return (
    <>
      <div>
        <b>Filter by status</b>
      </div>
      <select
        onClick={(e) => {
          status(e.target.value.toLowerCase());
        }}
      >
        <option>Show all</option>
        <option>Preparing</option>
        <option>On the way</option>
        <option>Completed</option>
      </select>
    </>
  );
};

ParcelStatus.propTypes = {
  status: PropTypes.func,
};

export default ParcelStatus;
