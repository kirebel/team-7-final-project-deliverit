import React, { useEffect, useState } from 'react';
import { getShipments } from '../../Requests/Shipments-requests';
import PropTypes from 'prop-types';

const ShipmentsDropdown = ({
  updateInfo,
  waitingParcelCheckBoxState,
  createParcelDropdownValues,
  updateDropdown,
  setShipmentIdState,
}) => {
  const [shipments, setShipments] = useState([]);

  useEffect(() => {
    getShipments('').then((data) => setShipments(data));
  }, []);

  const handleDropdown = (e) => {
    updateDropdown({ shipmentsDropdown: e.target.value });
    if (e.target.value.length !== 0) {
      const [shipmentId, warehouseId] = e.target.value.split(',');
      if (!waitingParcelCheckBoxState) {
        setShipmentIdState(+shipmentId);
        updateInfo('shipment_id', +shipmentId);
        updateInfo('warehouse_id', +warehouseId);
      } else {
        updateInfo('shipment_id', null);
        updateInfo('warehouse_id', null);
      }
    }
  };

  return (
    <select
      value={createParcelDropdownValues.shipmentsDropdown}
      onChange={(e) => handleDropdown(e)}
    >
      <option value='default'>Choose here</option>
      {shipments.map((element) => {
        if (element.shipment_status === 1) {
          return (
            <option
              key={element.shipment_id}
              value={`${element.shipment_id},${element.destination_warehouse_id}`}
            >
              #{element.shipment_id}&nbsp;{element.to_city_name},
              {element.to_country_name},&nbsp;
              {10 - element.parcels_count === 0
                ? 'full'
                : `parcels to full:${10 - element.parcels_count}`}
            </option>
          );
        }
        return null;
      })}
    </select>
  );
};

ShipmentsDropdown.propTypes = {
  updateInfo: PropTypes.func,
  waitingParcelCheckBoxState: PropTypes.bool,
  createParcelDropdownValues: PropTypes.object,
  updateDropdown: PropTypes.func,
};

export default ShipmentsDropdown;
