import React, { useEffect, useState } from 'react';
import { getCategories } from '../../Requests/Categories-requests';
import PropTypes from 'prop-types';

const CategoryDropdownList = ({
  setCategory,
  checkBoxStatus,
  dropdownValues,
  setDropdownValues,
}) => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getCategories().then((data) => setCategories(data));
  }, []);

  const handleDropdown = (e) => {
    setDropdownValues({ categoryDropdown: e.target.value });
    if (checkBoxStatus && e.target.value !== 'default') {
      setCategory('category_id=' + e.target.value + '&');
    } else {
      setCategory('');
    }
  };

  return (
    <>
      <select
        className='dropdown'
        value={dropdownValues.categoryDropdown}
        onChange={(e) => handleDropdown(e)}
      >
        <option value='default'>Choose here</option>
        {categories.map((element) => (
          <option key={element.id} value={element.id}>
            {element.name}
          </option>
        ))}
      </select>
    </>
  );
};

CategoryDropdownList.propTypes = {
  setCategory: PropTypes.func,
  checkBoxStatus: PropTypes.bool,
  dropdownValues: PropTypes.object,
  setDropdownValues: PropTypes.func,
};

export default CategoryDropdownList;
