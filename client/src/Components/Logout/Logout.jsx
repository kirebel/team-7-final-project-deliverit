import { Button } from 'react-bootstrap';
import React, { useContext } from 'react';
import AuthContext from '../../providers/AuthContext';
import PropTypes from 'prop-types';

const Logout = ({ classname }) => {
  const { setUser } = useContext(AuthContext);


  const triggerLogout = () => {
    setUser(null);

    localStorage.removeItem('token');
  };

  return (
    <>
      <Button size='lg' className={classname} onClick={triggerLogout}>
        Logout
      </Button>
    </>
  );
};

Logout.propTypes={
  classname: PropTypes.string,
}

export default Logout;
