import React from 'react';
import CountriesDropdownList from '../DropdownLists/CountriesDropdownList';
import PropTypes from 'prop-types';

const SearchByCountry = ({
  filterState,
  dispatchFilterState,
  dispatchSearchTerms,
}) => {
  const handleCheckbox = () => {
    dispatchFilterState({
      type: 'filter-by-country',
      payload: filterState.filterByCountry,
    });
    if (filterState.filterByCountry) {
      dispatchSearchTerms({ type: 'input-country', payload: '' });
    }
  };

  return (
    <div>
      <input type='checkbox' onChange={handleCheckbox} />
      <span>Country</span>
      {filterState.filterByCountry ? (
        <div>
          <CountriesDropdownList dispatchSearchTerms={dispatchSearchTerms} />
        </div>
      ) : null}
    </div>
  );
};

SearchByCountry.propTypes = {
  filterState: PropTypes.object,
  dispatchFilterState: PropTypes.func,
  dispatchSearchTerms: PropTypes.func,
};

export default SearchByCountry;
