import React from 'react';
import PropTypes from 'prop-types';

const SearchByEmail = ({
  filterState,
  dispatchFilterState,
  dispatchSearchTerms,
}) => {
  const handleCheckbox = () => {
    dispatchFilterState({
      type: 'filter-by-email',
      payload: filterState.filterByEmail,
    });
    if (filterState.filterByEmail) {
      dispatchSearchTerms({ type: 'input-email', payload: '' });
    }
  };

  return (
    <div>
      <input
        disabled={filterState.filterAllFields}
        type='checkbox'
        onChange={handleCheckbox}
      />
      <span>Email</span>
      {filterState.filterByEmail && !filterState.filterAllFields ? (
        <div>
          <input
            maxLength={15}
            onChange={(e) => {
              dispatchSearchTerms({
                type: 'input-email',
                payload: `email=${e.target.value}`,
              });
            }}
            placeholder={`Type here`}
          ></input>
        </div>
      ) : null}
    </div>
  );
};

SearchByEmail.propTypes = {
  filterState: PropTypes.object,
  dispatchFilterState: PropTypes.func,
  dispatchSearchTerms: PropTypes.func,
};

export default SearchByEmail;
