import React from 'react';
import PropTypes from 'prop-types';

const SearchByUsername = ({
  filterState,
  dispatchSearchTerms,
  dispatchFilterState,
}) => {
  const handleCheckbox = () => {
    dispatchFilterState({
      type: 'filter-by-username',
      payload: filterState.filterByUsername,
    });
    if (filterState.filterByUsername) {
      dispatchSearchTerms({ type: 'input-username', payload: '' });
    }
  };

  return (
    <div>
      <input
        type='checkbox'
        disabled={filterState.filterAllFields}
        onChange={handleCheckbox}
      />
      <span>Username</span>
      {filterState.filterByUsername && !filterState.filterAllFields ? (
        <div>
          <input
            disabled={filterState.filterAllFields}
            maxLength={15}
            onChange={(e) => {
              dispatchSearchTerms({
                type: 'input-username',
                payload: `username=${e.target.value}`,
              });
            }}
            placeholder={`Type here`}
          ></input>
        </div>
      ) : null}
    </div>
  );
};

SearchByUsername.propTypes = {
  filterState: PropTypes.object,
  dispatchFilterState: PropTypes.func,
  dispatchSearchTerms: PropTypes.func,
};

export default SearchByUsername;
