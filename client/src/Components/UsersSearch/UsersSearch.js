import React, { useEffect, useReducer, useState } from 'react';
import { getUsers } from '../../Requests/Users-requests';
import CustomerIncomingParcels from '../CustomersIncomingParcels/CustomerIncomingParcels';
import SearchAllFieldsWithOneString from './SearchAllFieldsWithOneString';
import SearchByCountry from './SearchByCountry';
import SearchByEmail from './SearchByEmail';
import SearchByFirstName from './SearchByFirstName';
import SearchByLastName from './SearchByLastName';
import SearchByUsername from './SearchByUsername';
import SearchResults from './SearchResults';
import './UsersSearch.css';
import PropTypes from 'prop-types';

const filterReducer = (state, action) => {
  switch (action.type) {
    case 'filter-by-username': {
      return {
        ...state,
        filterByUsername: !action.payload,
      };
    }
    case 'filter-by-first_name': {
      return {
        ...state,
        filterByFirstName: !action.payload,
      };
    }
    case 'filter-by-last_name': {
      return {
        ...state,
        filterByLastName: !action.payload,
      };
    }
    case 'filter-by-email': {
      return {
        ...state,
        filterByEmail: !action.payload,
      };
    }
    case 'filter-by-country': {
      return {
        ...state,
        filterByCountry: !action.payload,
      };
    }
    case 'all-fields': {
      return {
        ...state,
        filterAllFields: !action.payload,
      };
    }
    default:
      return state;
  }
};

const searchTermsReducer = (state, action) => {
  switch (action.type) {
    case 'input-username': {
      return {
        ...state,
        username: action.payload,
      };
    }

    case 'input-first-name': {
      return {
        ...state,
        first_name: action.payload,
      };
    }
    case 'input-last-name': {
      return {
        ...state,
        last_name: action.payload,
      };
    }
    case 'input-email': {
      return {
        ...state,
        email: action.payload,
      };
    }
    case 'input-in-all-fields': {
      return {
        ...state,
        allFields: action.payload,
        username: '',
        last_name: '',
        first_name: '',
        email: '',
      };
    }
    case 'input-country': {
      return {
        ...state,
        country_id: action.payload,
      };
    }
    default:
      return state;
  }
};

const UsersSearch = ({
  setShipmentForUpdate,
  dispatchActionsState,
  dispatchSmallButtonsState,
}) => {
  const [users, setUsers] = useState([]);
  const [filterState, dispatchFilterState] = useReducer(filterReducer, {});
  const [searchTerms, dispatchSearchTerms] = useReducer(searchTermsReducer, {});
  const [
    customerUsernameForQueryIncomingParcels,
    setCustomerUsernameForQueryIncomingParcels,
  ] = useState('');
  const [showIncomingParcelsState, setShowIncomingParcelsState] =
    useState(false);

  const [windowHeight, setWindowHeight] = useState(0);

  const searchQueryString =
    Object.values(searchTerms).filter((element) => element).length === 0
      ? ''
      : Object.values(searchTerms)
          .filter((element) => element)
          .join('&');

  const handleHeight = (e) => {
    setWindowHeight(e.clientY);
  };

  useEffect(() => {
    getUsers(searchQueryString).then((data) => setUsers(data));
  }, [searchQueryString]);

  return (
    <>
      <div className='user-search-wrapper'>
        <div className='search-engine'>
          <div className='username-first-last-name'>
            <SearchByUsername
              filterState={filterState}
              dispatchFilterState={dispatchFilterState}
              dispatchSearchTerms={dispatchSearchTerms}
            />
            <SearchByFirstName
              filterState={filterState}
              dispatchFilterState={dispatchFilterState}
              dispatchSearchTerms={dispatchSearchTerms}
            />
            <SearchByLastName
              filterState={filterState}
              dispatchFilterState={dispatchFilterState}
              dispatchSearchTerms={dispatchSearchTerms}
            />
          </div>

          <div className='email-all-fields'>
            <SearchByEmail
              filterState={filterState}
              dispatchFilterState={dispatchFilterState}
              dispatchSearchTerms={dispatchSearchTerms}
            />
            <SearchAllFieldsWithOneString
              filterState={filterState}
              dispatchFilterState={dispatchFilterState}
              dispatchSearchTerms={dispatchSearchTerms}
              searchQueryString={searchQueryString}
            />
          </div>

          <div>
            <SearchByCountry
              filterState={filterState}
              dispatchFilterState={dispatchFilterState}
              dispatchSearchTerms={dispatchSearchTerms}
            />
          </div>
        </div>

        <div className='results-and-incoming-parcels'>
          <SearchResults
            handleHeight={handleHeight}
            users={users}
            setCustomerUsernameForQueryIncomingParcels={
              setCustomerUsernameForQueryIncomingParcels
            }
            showIncomingParcelsState={showIncomingParcelsState}
            setShowIncomingParcelsState={setShowIncomingParcelsState}
          />
        </div>
      </div>
      {showIncomingParcelsState ? (
        <CustomerIncomingParcels
          windowHeight={windowHeight}
          customerUsernameForQueryIncomingParcels={
            customerUsernameForQueryIncomingParcels
          }
          setShipmentForUpdate={setShipmentForUpdate}
          dispatchActionsState={dispatchActionsState}
          dispatchSmallButtonsState={dispatchSmallButtonsState}
        />
      ) : null}
    </>
  );
};

UsersSearch.propTypes = {
  setShipmentForUpdate: PropTypes.func,
  dispatchActionsState: PropTypes.func,
};

export default UsersSearch;
