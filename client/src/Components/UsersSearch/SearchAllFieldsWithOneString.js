import React from 'react';
import PropTypes from 'prop-types';

const SearchAllFieldsWithOneString = ({
  filterState,
  dispatchFilterState,
  dispatchSearchTerms,
}) => {
  const handleCheckbox = () => {
    dispatchFilterState({
      type: 'all-fields',
      payload: filterState.filterAllFields,
    });

    dispatchSearchTerms({
      type: 'input-in-all-fields',
      payload: '',
    });
  };

  return (
    <div>
      <input type='checkbox' onChange={handleCheckbox} />
      <span>All fields</span>
      {filterState.filterAllFields ? (
        <div>
          <input
            maxLength={15}
            onChange={(e) => {
              dispatchSearchTerms({
                type: 'input-in-all-fields',
                payload: `last_name=${e.target.value}&first_name=${e.target.value}&username=${e.target.value}&email=${e.target.value}`,
              });
            }}
            placeholder={`Type here`}
          ></input>
        </div>
      ) : null}
    </div>
  );
};

SearchAllFieldsWithOneString.propTypes = {
  filterState: PropTypes.object,
  dispatchFilterState: PropTypes.func,
  dispatchSearchTerms: PropTypes.func,
};

export default SearchAllFieldsWithOneString;
