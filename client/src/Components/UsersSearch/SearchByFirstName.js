import React from 'react';
import PropTypes from 'prop-types';

const SearchByFirstName = ({
  filterState,
  dispatchFilterState,
  dispatchSearchTerms,
}) => {
  const handleCheckbox = () => {
    dispatchFilterState({
      type: 'filter-by-first_name',
      payload: filterState.filterByFirstName,
    });
    if (filterState.filterByFirstName) {
      dispatchSearchTerms({ type: 'input-first-name', payload: '' });
    }
  };

  return (
    <div>
      <input
        disabled={filterState.filterAllFields}
        type='checkbox'
        onChange={handleCheckbox}
      />
      <span>First name</span>
      {filterState.filterByFirstName && !filterState.filterAllFields ? (
        <div>
          <input
            maxLength={15}
            onChange={(e) => {
              dispatchSearchTerms({
                type: 'input-first-name',
                payload: `first_name=${e.target.value}`,
              });
            }}
            placeholder={`Type here`}
          ></input>
        </div>
      ) : null}
    </div>
  );
};

SearchByFirstName.propTypes = {
  filterState: PropTypes.object,
  dispatchFilterState: PropTypes.func,
  dispatchSearchTerms: PropTypes.func,
};

export default SearchByFirstName;
