import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import './SearchResults.css';
import PropTypes from 'prop-types';

const SearchResults = ({
  users,
  setCustomerUsernameForQueryIncomingParcels,
  showIncomingParcelsState,
  setShowIncomingParcelsState,
  handleHeight,
}) => {
  const [currentButtonIndex, setCurrentButtonIndex] = useState(0);

  const handleIncomingParcelsButton = (e, customer, index) => {
    handleHeight(e);
    setCurrentButtonIndex(index);
    setShowIncomingParcelsState(!showIncomingParcelsState);
    if (!showIncomingParcelsState) {
      setCustomerUsernameForQueryIncomingParcels(customer.username);
    } else {
      setCustomerUsernameForQueryIncomingParcels('');
    }
  };

  return (
    <table>
      <tbody>
        <tr>
          <th className='customer-number'>customer #</th>
          <th className='name-country-username'>Name</th>
          <th className='email'>email</th>
          <th className='username'>username</th>
          <th className='country-city'>Country/City</th>
          <th className='customer-number'></th>
        </tr>
        {users
          .filter((element) => element.role !== 'employee')
          .map((customer, index) => (
            <tr key={customer.id}>
              <td>{customer.id}</td>
              <td>
                {customer.first_name}
                <br />
                {customer.last_name}
              </td>
              <td>{customer.email}</td>
              <td>{customer.username}</td>
              <td>
                {customer.country},{customer.city}
              </td>
              <td>
                <Button
                  id='yey'
                  variant={
                    showIncomingParcelsState && currentButtonIndex === index
                      ? 'success'
                      : 'primary'
                  }
                  onClick={(e) =>
                    handleIncomingParcelsButton(e, customer, index)
                  }
                >
                  Incoming
                  <br /> parcels
                </Button>
              </td>
            </tr>
          ))}
      </tbody>
    </table>
  );
};

SearchResults.propTypes = {
  users: PropTypes.array,
  setCustomerUsernameForQueryIncomingParcels: PropTypes.func,
  showIncomingParcelsState: PropTypes.bool,
  setShowIncomingParcelsState: PropTypes.func,
  handleHeight: PropTypes.func,
};

export default SearchResults;
