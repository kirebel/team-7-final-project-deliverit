import React from 'react';
import PropTypes from 'prop-types';

const SearchByLastName = ({
  filterState,
  dispatchFilterState,
  dispatchSearchTerms,
}) => {
  const handleCheckbox = () => {
    dispatchFilterState({
      type: 'filter-by-last_name',
      payload: filterState.filterByLastName,
    });
    if (filterState.filterByLastName) {
      dispatchSearchTerms({ type: 'input-last-name', payload: '' });
    }
  };

  return (
    <div>
      <input
        disabled={filterState.filterAllFields}
        type='checkbox'
        onChange={handleCheckbox}
      />
      <span>Last name</span>
      {filterState.filterByLastName && !filterState.filterAllFields ? (
        <div>
          <input
            maxLength={15}
            onChange={(e) => {
              dispatchSearchTerms({
                type: 'input-last-name',
                payload: `last_name=${e.target.value}`,
              });
            }}
            placeholder={`Type here`}
          ></input>
        </div>
      ) : null}
    </div>
  );
};

SearchByLastName.propTypes = {
  filterState: PropTypes.object,
  dispatchFilterState: PropTypes.func,
  dispatchSearchTerms: PropTypes.func,
};

export default SearchByLastName;
