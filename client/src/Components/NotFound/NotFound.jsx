import React from 'react';
import './NotFound.css';

const NotFound = () => {
  return <h1 className='not-found'>Not found!</h1>;
};

export default NotFound;
