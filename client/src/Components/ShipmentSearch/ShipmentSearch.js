import { Button } from 'react-bootstrap';
import React, { useContext, useEffect, useState } from 'react';
import { getShipments } from '../../Requests/Shipments-requests';
import { getWarehouses } from '../../Requests/Warehouses-requests';
import AuthContext from '../../providers/AuthContext';
import './ShipmentSearch.css';
import PropTypes from 'prop-types';

const ShipmentSearch = ({ dispatchActionsState, setShipmentForUpdate }) => {
  const [shipments, setShipments] = useState([]);
  const [warehouses, setWarehouses] = useState([]);
  const [resetUsernameInput, setResetUsernameInput] = useState('');
  const [customerUsername, setCustomerUsername] = useState('');
  const [warehouse, setWarehouse] = useState('');
  const [resetWarehouseDropdown, setResetWarehouseDropdown] = useState('');

  const { user } = useContext(AuthContext);

  const handleKeypress = (e) => {
    if (e.charCode === 13) {
      if (e.target.value.length === 0) {
        setCustomerUsername('');
      }
      setCustomerUsername(`user=${e.target.value}`);
      setWarehouse('');
      setResetWarehouseDropdown('');
    }
  };

  useEffect(() => {
    getWarehouses().then((data) => setWarehouses(data));
    if (!customerUsername) {
      getShipments(warehouse).then((data) => setShipments(data));
    } else if (customerUsername) {
      getShipments(customerUsername).then((data) => setShipments(data));
    } else {
      getShipments().then((data) => setShipments(data));
    }
  }, [customerUsername, warehouse]);

  return (
    <div className='shipments-search-wrapper'>
      <div className='shipment-search'>
        <span className='search-condition-text'>Filter by:&nbsp;</span>
        <span>Includes customer:</span>&nbsp;
        <input
          className='inputs'
          maxLength={15}
          value={resetUsernameInput}
          onChange={(e) => {
            if (e.target.value.length === 0) {
              setCustomerUsername('');
            }
            setResetUsernameInput(e.target.value);
          }}
          placeholder={`Customer's username`}
          onKeyPress={(e) => handleKeypress(e)}
        ></input>
        &nbsp;&nbsp;&nbsp;
        <span>To warehouse:</span>&nbsp;
        <select
          value={resetWarehouseDropdown}
          onChange={(e) => {
            if (e.target.value !== 'default') {
              setWarehouse(`destination_warehouse_id=  ${e.target.value}`);
            } else {
              setWarehouse('');
            }
            setResetWarehouseDropdown(e.target.value);
            setCustomerUsername('');
            setResetUsernameInput('');
          }}
        >
          <option value='default'>Choose here</option>
          {warehouses.map((element) => (
            <option key={element.warehouse_id} value={element.warehouse_id}>
              {element.warehouse_city},{element.warehouse_country}
            </option>
          ))}
        </select>
      </div>

      <div className='shipment-search-results'>
        <table>
          <tbody>
            <tr>
              <th>shipment #</th>
              <th>from</th>
              <th>to</th>
              <th>destination warehouse #</th>
              <th>departure date</th>
              <th>arrival date</th>
              <th>parcels count</th>
              <th>status</th>
              {user.role !== 'support' ? <th></th> : null}
            </tr>

            {shipments.map((el, index) => (
              <tr key={index}>
                <td>{el.shipment_id}</td>
                <td>
                  {el.from_city_name},{el.from_country_name}
                </td>
                <td>
                  {el.to_city_name},{el.to_country_name}
                </td>
                <td>{el.destination_warehouse_id}</td>
                <td>
                  {el.departure_time
                    ? new Date(el.departure_time).toLocaleDateString()
                    : null}
                </td>
                <td>
                  {el.arrival_time
                    ? new Date(el.arrival_time).toLocaleDateString()
                    : null}
                </td>
                <td>{el.parcels_count}</td>
                <td>
                  {el.shipment_status === 1
                    ? 'preparing'
                    : el.shipment_status === 2
                    ? 'on the way'
                    : 'completed'}
                </td>
                {user.role !== 'support' ? (
                  <td>
                    <Button
                      variant='primary'
                      onClick={() => {
                        dispatchActionsState({ type: 'shipment-create-form' });
                        setShipmentForUpdate(el.shipment_id);
                      }}
                    >
                      Details
                    </Button>
                  </td>
                ) : null}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

ShipmentSearch.propTypes = {
  dispatchActionsState: PropTypes.func,
  setShipmentForUpdate: PropTypes.func,
};

export default ShipmentSearch;
