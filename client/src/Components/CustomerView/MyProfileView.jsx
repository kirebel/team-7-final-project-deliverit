import React from 'react';
import { useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import AuthContext from '../../providers/AuthContext';
import Header from '../Main/Header';
import './CustomerView.css';
import MyProfileCity from './MyProfileCity';
import MyProfileCountry from './MyProfileCountry';
import MyProfileRowInfo from './MyProfileRowInfo';

const MyProfileView = () => {
  const { user, setUser } = useContext(AuthContext);
  const history = useHistory();

  const [userInfo, setUserInfo] = useState({
    username: {
      name: 'username',
      value: user.username,
      valid: true,
      update: false,
      isTouched: false,
      error: 'Username name must be between 2 and 20 characters!',
    },
    first_name: {
      name: 'first_name',
      value: user.first_name,
      valid: true,
      update: false,
      error: 'First name must be between 2 and 20 characters!',
    },
    last_name: {
      name: 'last_name',
      value: user.last_name,
      valid: true,
      update: false,
      error: 'Last name must be between 2 and 20 characters!',
    },
    street: {
      name: 'street',
      value: user.street,
      valid: true,
      update: false,
      error: 'Street must be between 2 and 60 characters',
    },

    email: {
      name: 'email',
      value: user.email,
      valid: true,
      update: false,
      error: 'Not a valid email!',
    },
  });

  return (
    <>
      <div className='my-profile-wrapper'>
        <div className='my-profile-header'>
          <Header />
        </div>
        <div className='my-profile-body-wrapper'>
          <div className='my-profile-backbutton'>
            <Button size='lg' onClick={() => history.push('/customer-view')}>
              Back
            </Button>
          </div>
          <div className='profile-info'>
            <div className='profile-title-string'>User Info</div>

            <div className='profile-info-row'>
              <div>Username</div>
              <div>
                <MyProfileRowInfo
                  userInfo={userInfo}
                  setUserInfo={setUserInfo}
                  rowName={'username'}
                  user={user}
                  setUser={setUser}
                />
              </div>
            </div>

            <div className='profile-info-row'>
              <div>Email</div>
              <div>
                <MyProfileRowInfo
                  userInfo={userInfo}
                  setUserInfo={setUserInfo}
                  rowName={'email'}
                  user={user}
                  setUser={setUser}
                />
              </div>
            </div>

            <div className='profile-info-row'>
              <div>First Name</div>
              <div>
                <MyProfileRowInfo
                  userInfo={userInfo}
                  setUserInfo={setUserInfo}
                  rowName={'first_name'}
                  user={user}
                  setUser={setUser}
                />
              </div>
            </div>

            <div className='profile-info-row'>
              <div>Last Name</div>
              <div>
                <MyProfileRowInfo
                  userInfo={userInfo}
                  setUserInfo={setUserInfo}
                  rowName={'last_name'}
                  user={user}
                  setUser={setUser}
                />
              </div>
            </div>

            <div className='profile-info-row'>
              <div>Country</div>
              <div>
                <MyProfileCountry user={user} setUser={setUser} />
              </div>
            </div>

            <div className='profile-info-row'>
              <div>City</div>
              <div>
                <MyProfileCity user={user} setUser={setUser} />
              </div>
            </div>

            <div className='profile-info-row'>
              <div>Street</div>
              <div>
                <MyProfileRowInfo
                  userInfo={userInfo}
                  setUserInfo={setUserInfo}
                  rowName={'street'}
                  user={user}
                  setUser={setUser}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MyProfileView;
