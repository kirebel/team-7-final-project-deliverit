import React from 'react';
import { useState } from 'react';
import { Button } from 'react-bootstrap';
import { updateUserInfo } from '../../Requests/Auth';
import { bgCities } from '../../Common/bg-cities';
import { espCities } from '../../Common/esp-cities';
import { createCity } from '../../Requests/City';
import image from './update-icon.jpg';
import PropTypes from 'prop-types';

const MyProfileCity = ({ user, setUser }) => {
  const [updateCity, setUpdateCity] = useState(false);
  const [city, setCity] = useState(user.city);

  const update = async (city) => {
    setUpdateCity(false);
    if (user.city !== city) {
      await updateUserInfo({ ...user, city }, user.id).then((data) => {
        setUser(data);
        const token = data.token;
        localStorage.setItem('token', token);
      });
    }
  };
  return (
    <>
      {!updateCity ? (
        <>
          {city}{' '}
          <button
            className='profile-update-btn'
            onClick={() => setUpdateCity(true)}
          >
            {' '}
            <img src={image} alt='update-icon' width='20rem' />
          </button>
        </>
      ) : (
        <>
          <select
            value={city}
            onChange={(e) => setCity(e.target.value)}
            onClick={createCity(city, user.country)}
          >
            {(user.country === 'Bulgaria' ? bgCities : espCities).map(
              (city) => (
                <option key={city} value={city}>
                  {city}
                </option>
              )
            )}
          </select>
          <Button onClick={(e) => update(city)}>Done</Button>
        </>
      )}
    </>
  );
};

MyProfileCity.propTypes = {
  user: PropTypes.object,
  setUser: PropTypes.func
}


export default MyProfileCity;
