import React from 'react';
import { useState } from 'react';
import { Button } from 'react-bootstrap';
import { updateUserInfo } from '../../Requests/Auth';
import image from './update-icon.jpg';
import PropTypes from 'prop-types';

const MyProfileCountry = ({ user, setUser }) => {
  const [updateCountry, setUpdateCountry] = useState(false);
  const [country, setCountry] = useState(user.country);

  const update = (country) => {
    setUpdateCountry(false);
    if (user.country !== country) {
      updateUserInfo({ ...user, country }, user.id).then((data) => {
        setUser(data);
        const token = data.token;
        localStorage.setItem('token', token);
      });
    }
  };

  return (
    <>
      {!updateCountry ? (
        <>
          {country}{' '}
          <button
            className='profile-update-btn'
            onClick={() => setUpdateCountry(true)}
          >
            <img src={image} alt='update=icon' width='20rem' />
          </button>
        </>
      ) : (
        <>
          <select value={country} onChange={(e) => setCountry(e.target.value)}>
            <option value='Bulgaria'>Bulgaria</option>
            <option value='Spain'>Spain</option>
          </select>
          <Button onClick={(e) => update(country)}>Done</Button>
        </>
      )}
    </>
  );
};

MyProfileCountry.propTypes = {
  user: PropTypes.object,
  setUser: PropTypes.func
}



export default MyProfileCountry;
