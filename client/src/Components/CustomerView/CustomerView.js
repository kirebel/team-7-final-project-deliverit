import AllParcelsView from '../AllParcelsView/AllParcelsView';
import './CustomerView.css';
import Logout from '../Logout/Logout';
import ParcelStatus from '../DropdownLists/ParcelStatus.js';
import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Header from '../Main/Header.jsx';
import AuthContext from '../../providers/AuthContext';
import { getParcels } from '../../Requests/Parcels-requests';
import { useContext, useEffect } from 'react';

const CustomerView = () => {
  const [status, setStatus] = useState('show all');
  const history = useHistory();

  const [parcels, setParcels] = useState([]);
  const { user } = useContext(AuthContext);

  const searchTerms = `user_id=${user.id ? user.id : user.userId}`;

  useEffect(() => {
    getParcels(searchTerms).then((data) => setParcels(data));
  }, [searchTerms, user]);

  const getStatus = (value) => {
    if (value) {
      setStatus(value);
    }
  };

  return (
    <div className='wrapper-customerview'>
      <div className='customer-header'>
        <Header />
      </div>
      <div className='customer-body-wrapper'>
        <div className='customer-menu'>
          <div>
            <Button
              size='lg'
              className='button-locations'
              onClick={() => history.push('/maps')}
            >
              Locations
            </Button>
          </div>
          <div>
            <Button
              size='lg'
              className={'buttons-customer'}
              onClick={() => history.push('/customer-support')}
            >
              Support
            </Button>
          </div>
          <div className='button-my-profile'>
            <Button size='lg' onClick={() => history.push('/my-profile')}>
              My Profile
            </Button>
          </div>
          <div>
            <Logout classname={'buttons-customer'} />
          </div>
        </div>

        {parcels.length > 0 ? (
          <>
            {' '}
            <div className='customer-parcels'>
              <div className='customer-parcels-table'>
                <AllParcelsView status={status} parcels={parcels} />
              </div>
              <div className='customer-parcels-filter'>
                <div className='filter-menu'>
                  <ParcelStatus status={getStatus} />
                </div>
              </div>
            </div>{' '}
          </>
        ) : (
          <div className='no-results'>You have no parcels yet.</div>
        )}
      </div>
    </div>
  );
};

export default CustomerView;
