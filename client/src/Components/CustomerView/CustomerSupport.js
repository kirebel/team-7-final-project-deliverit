import React, { useContext, useEffect, useRef, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import io from 'socket.io-client';
import AuthContext from '../../providers/AuthContext';
import Logout from '../Logout/Logout';
import Header from '../Main/Header';
import ParcelsSearch from '../ParcelsSearch/ParcelsSearch';
import ShipmentSearch from '../ShipmentSearch/ShipmentSearch';
import './CustomerSupport.css';

const CustomerSupport = () => {
  const [singleMessage, setSingleMessage] = useState({
    message: '',
    username: '',
  });
  const [chat, setChat] = useState([]);

  const [showParcelsButtonState, setShowParcelsButtonState] = useState(true);
  const [showShipmentsButtonState, setShowShipmentsButtonState] =
    useState(false);
  const [showParcelsButtonColor, setShowParcelsButtonColor] =
    useState('primary');
  const [showShipmentsButtonColor, setShowShipmentsButtonColor] =
    useState('light');

  const { user } = useContext(AuthContext);

  const history = useHistory();

  const socketRef = useRef();

  useEffect(() => {
    socketRef.current = io.connect('http://localhost:4000');

    if (user) {
      socketRef.current.emit('setUserInfo', {
        username: user.username,
        socket_id: socketRef.current.id,
      });
    }
    socketRef.current.on('message', ({ username, message }) => {
      setChat([...chat, { username, message }]);
    });
    return () => socketRef.current.disconnect();
  }, [chat, user]);

  const updateMessageInfo = (prop, value) => {
    singleMessage[prop] = value;
    setSingleMessage({ ...singleMessage });
  };

  const onMessageSubmit = () => {
    const { username, message } = singleMessage;
    socketRef.current.emit('message', {
      username,
      message,
    });
    setSingleMessage({ message: '', username });
  };

  const addSpecificMessage = (message) => {
    setSingleMessage({ message, username: user.username });
  };

  const handleShipmentsButton = () => {
    setShowShipmentsButtonState(true);
    setShowParcelsButtonState(false);
    setShowShipmentsButtonColor('primary');
    setShowParcelsButtonColor('light');
  };

  const handleParcelsButton = () => {
    setShowShipmentsButtonState(false);
    setShowParcelsButtonState(true);
    setShowParcelsButtonColor('primary');
    setShowShipmentsButtonColor('light');
  };

  return (
    <div>
      <div className='support-header'>
        <Header />
      </div>

      <div className='customer-support-view'>
        <div className='support-chat'>
          <div>
            <div className='chat-title'>Customer Support Chat</div>

            <div className='chat'>
              {chat.map(({ username, message }, index) => (
                <div key={index}>
                  <div className='single-message'>
                    {username}: <span className='message-text'>{message}</span>
                  </div>
                </div>
              ))}
            </div>
          </div>

          <input
            className='chat-input'
            value={
              singleMessage.message !== 'undefined'
                ? singleMessage.message
                : null
            }
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                updateMessageInfo('username', user.username);
                onMessageSubmit();
              }
            }}
            onChange={(e) => updateMessageInfo('message', e.target.value)}
          ></input>

          <Button className='send-message-button' onClick={onMessageSubmit}>
            Send message
          </Button>
        </div>
        {user.role !== 'support' ? (
          <div className='frequently-asked-questions'>
            <div className='questions-header'>Frequently asked questions</div>
            <div
              className='frequently-questions'
              onClick={() => addSpecificMessage('When my order will arrive?')}
            >
              -&nbsp;When my order will arrive?
            </div>
            <div
              className='frequently-questions'
              onClick={() =>
                addSpecificMessage('Are you working with other countries?')
              }
            >
              -&nbsp;Are you working with other countries?
            </div>
            <div
              className='frequently-questions'
              onClick={() =>
                addSpecificMessage('What form of payment do you accept?')
              }
            >
              -&nbsp;What form of payment do you accept?
            </div>
            <div
              className='frequently-questions'
              onClick={() =>
                addSpecificMessage('How the home delivery process works?')
              }
            >
              -&nbsp;How the home delivery process works?
            </div>
            <hr />
            <div className='back-button'>
              <Button size='lg' onClick={() => history.push('/customer-view')}>
                Back
              </Button>
            </div>
          </div>
        ) : (
          <div className='wrapper-support-options'>
            {showParcelsButtonState ? (
              <div className='parcels'>
                <ParcelsSearch />
              </div>
            ) : null}
            {showShipmentsButtonState ? (
              <div className='parcels'>
                <ShipmentSearch />
              </div>
            ) : null}
          </div>
        )}
        {user.role === 'support' ? (
          <div className='support-options'>
            <Button
              variant={showParcelsButtonColor}
              onClick={handleParcelsButton}
            >
              Parcels
            </Button>
            <Button
              variant={showShipmentsButtonColor}
              onClick={handleShipmentsButton}
            >
              Shipments
            </Button>
            <br />
            <Logout />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default CustomerSupport;
