import React from 'react';
import { Button } from 'react-bootstrap';
import { checkUsername, updateUserInfo } from '../../Requests/Auth';
import image from './update-icon.jpg';
import PropTypes from 'prop-types';

const MyProfileRowInfo = ({
  userInfo,
  setUserInfo,
  rowName,
  user,
  setUser,
}) => {
  const validators = {
    username: (value) => value.length > 2 && value.length < 20,
    email: (value) => /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(value),
    first_name: (value) => value.length > 2 && value.length < 20,
    last_name: (value) => value.length > 2 && value.length < 20,
    street: (value) => value.length > 2 && value.length < 60,
  };

  const checkUsernameExists = async (username, value) => {
    const updatedInput = userInfo[username];
    updatedInput.value = value;

    const checkUsernameExists = await checkUsername({
      username: value,
    });

    if (Object.keys(checkUsernameExists)[0] === 'exist') {
      updatedInput.valid = false;
      updatedInput.error = 'Username already exists!';
    } else {
      updatedInput.valid = validators[username](value);
      updatedInput.error = 'Username name must be between 2 and 20 characters!';
    }

    setUserInfo({ ...userInfo, [username]: updatedInput });
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    if (name === 'username') {
      checkUsernameExists(name, value);
    } else {
      const updatedInput = userInfo[name];
      updatedInput.value = value;
      updatedInput.valid = validators[name](value);

      setUserInfo({ ...userInfo, [name]: updatedInput });
    }
  };

  const showUpdate = (name) => {
    const updatedData = {
      ...userInfo[name],
      [name]: (userInfo[name].update = true),
    };

    setUserInfo({
      ...userInfo,
      updatedData,
    });
  };

  const closeUpdate = (name, value) => {
    const updatedData = {
      ...userInfo[name],
      [name]: (userInfo[name].update = false),
    };

    setUserInfo({
      ...userInfo,
      updatedData,
    });

    if (user[name] !== value) {
      if (userInfo[rowName].valid) {
        updateUserInfo({ ...user, [name]: userInfo[name].value }, user.id).then(
          (data) => {
            setUser(data);
            const token = data.token;
            localStorage.setItem('token', token);
          }
        );
      }
    }
  };

  return (
    <>
      {userInfo[rowName].update ? (
        <>
          <input
            type='text'
            name={rowName}
            value={userInfo[rowName].value}
            onChange={(e) => handleInputChange(e)}
          />

          {!userInfo[rowName].valid ? (
            <div>{userInfo[rowName].error}</div>
          ) : (
            <></>
          )}
          <Button
            onClick={() => closeUpdate(rowName, userInfo[rowName].value)}
            disabled={!userInfo[rowName].valid}
          >
            Done
          </Button>
        </>
      ) : (
        <>
          {userInfo[rowName].value}
          <button
            className='profile-update-btn'
            onClick={() => showUpdate(rowName)}
          >
            <img src={image} alt='update=icon' width='20rem' />
          </button>
        </>
      )}
    </>
  );
};

MyProfileRowInfo.propTypes = {
  userInfo: PropTypes.object,
  setUserInfo: PropTypes.func,
  rowName: PropTypes.string,
  user: PropTypes.object,
  setUser: PropTypes.func,
}

export default MyProfileRowInfo;
