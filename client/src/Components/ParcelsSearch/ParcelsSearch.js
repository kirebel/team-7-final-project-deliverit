import React, { useContext, useEffect, useState } from 'react';
import { getParcels } from '../../Requests/Parcels-requests';
import './ParcelsSearch.css';
import CategoriesDropdownList from '../DropdownLists/CategoryDropdownList';
import WarehousesDropdown from '../DropdownLists/WarehousesDropdownList';
import WeightFilterRange from '../WeightFilterRange/WeightFilterRange';
import { Button } from 'react-bootstrap';
import AuthContext from '../../providers/AuthContext';
import PropTypes from 'prop-types';

const ParcelsSearch = ({
  dispatchActionsState,
  setShipmentForUpdate,
  dispatchSmallButtonsState,
}) => {
  const [parcels, setParcels] = useState([]);

  const [customerUsername, setCustomerUsername] = useState('');
  const [category, setCategory] = useState('');
  const [weight, setWeight] = useState('');
  const [warehouse, setWarehouse] = useState('');
  const [sortArrivalDate, setSortArrivalDate] = useState('');
  const [sortWeight, setSortWeight] = useState('');
  const [minWeight, setMinWeight] = useState('');
  const [maxWeight, setMaxWeight] = useState('');

  const [categoryCheckBoxStatus, setCategoryCheckBoxStatus] = useState(false);
  const [warehouseCheckBoxStatus, setWarehouseCheckBoxStatus] = useState(false);
  const [weightCheckBoxStatus, setWeightCheckBoxStatus] = useState(false);
  const [
    sortArrivalDateAscCheckBoxStatus,
    setSortArrivalDateAscCheckBoxStatus,
  ] = useState(false);

  const [
    sortArrivalDateDescCheckBoxStatus,
    setSortArrivalDateDescCheckBoxStatus,
  ] = useState(false);

  const [sortWeightAscCheckBoxStatus, setSortWeightAscCheckBoxStatus] =
    useState(false);

  const [sortWeightDescCheckBoxStatus, setSortWeightDescCheckBoxStatus] =
    useState(false);

  const [dropdownValues, setDropdownValues] = useState({
    categoryDropdown: '',
    warehouseDropdown: '',
  });

  const updateDropdown = (data) => {
    setDropdownValues({ ...dropdownValues, ...data });
  };

  const { user } = useContext(AuthContext);

  const wholeSort = [sortArrivalDate, sortWeight];

  const sortTerm =
    wholeSort[0] !== '' && wholeSort[1] !== ''
      ? `sort=${wholeSort.join(',')}`
      : wholeSort[0].length !== 0
      ? `sort=${wholeSort[0]}`
      : `sort=${wholeSort[1]}`;

  const searchTerms =
    category + customerUsername + warehouse + weight + sortTerm;

  useEffect(() => {
    getParcels(searchTerms).then((data) => {
      setParcels(data);
    });
  }, [searchTerms]);

  const handleUsernameInput = (e) => {
    if (e.target.value.length === 0) {
      setCustomerUsername('');
    }
    setCustomerUsername('user=' + e.target.value + '&');
  };

  const handleCategoryCheckBox = () => {
    if (categoryCheckBoxStatus) {
      updateDropdown({ categoryDropdown: '' });
    }
    setCategoryCheckBoxStatus(!categoryCheckBoxStatus);
    setCategory('');
  };

  const handleWarehouseCheckBox = () => {
    if (warehouseCheckBoxStatus) {
      updateDropdown({ warehouseDropdown: '' });
    }
    setWarehouseCheckBoxStatus(!warehouseCheckBoxStatus);
    setWarehouse('');
  };

  const handleWeightCheckBox = () => {
    setWeightCheckBoxStatus(!weightCheckBoxStatus);
    if (weightCheckBoxStatus) {
      setWeight('');
      setMinWeight('');
      setMaxWeight('');
    }
  };

  const handleSortArrivalTimeAscCheckBox = () => {
    setSortArrivalDateAscCheckBoxStatus(!sortArrivalDateAscCheckBoxStatus);
    if (!sortArrivalDateAscCheckBoxStatus) {
      setSortArrivalDate('arrival_time:asc');
    } else {
      setSortArrivalDate('');
    }
  };

  const handleSortArrivalDateDescCheckBox = () => {
    setSortArrivalDateDescCheckBoxStatus(!sortArrivalDateDescCheckBoxStatus);
    if (!sortArrivalDateDescCheckBoxStatus) {
      setSortArrivalDate('arrival_time:desc');
    } else {
      setSortArrivalDate('');
    }
  };

  const handleSortWeightAscCheckBox = () => {
    setSortWeightAscCheckBoxStatus(!sortWeightAscCheckBoxStatus);
    if (!sortWeightAscCheckBoxStatus && sortArrivalDate) {
      setSortWeight('weight:asc');
    } else if (!sortWeightAscCheckBoxStatus) {
      setSortWeight('weight:asc');
    } else {
      setSortWeight('');
    }
  };

  const handleSortWeightDescCheckBox = () => {
    setSortWeightDescCheckBoxStatus(!sortWeightDescCheckBoxStatus);
    if (!sortWeightDescCheckBoxStatus && sortArrivalDate) {
      setSortWeight('weight:desc');
    } else if (!sortWeightDescCheckBoxStatus) {
      setSortWeight('weight:desc');
    } else {
      setSortWeight('');
    }
  };

  return (
    <div className='search-params-and-results'>
      <div className='search'>
        <div>
          <div className='username-search'>
            <div className='search-engine-div'>Search engine</div>
            <input
              maxLength={15}
              placeholder={`customer's username`}
              onChange={(e) => handleUsernameInput(e)}
            ></input>
          </div>
        </div>
        <div className='vertical-line-filter'></div>
        <div className='filtering'>
          <span>Filter by:</span>
          <div>
            <input
              type='checkbox'
              defaultChecked={categoryCheckBoxStatus}
              onChange={handleCategoryCheckBox}
            />
            <span>Category</span>
            <CategoriesDropdownList
              setCategory={setCategory}
              checkBoxStatus={categoryCheckBoxStatus}
              setCategoryCheckBoxStatus={setCategoryCheckBoxStatus}
              dropdownValues={dropdownValues}
              setDropdownValues={setDropdownValues}
            />
          </div>
          <div>
            <input
              type='checkbox'
              defaultChecked={warehouseCheckBoxStatus}
              onChange={handleWarehouseCheckBox}
            />
            <span>Warehouse</span>
            <WarehousesDropdown
              setWarehouse={setWarehouse}
              checkBoxStatus={warehouseCheckBoxStatus}
              dropdownValues={dropdownValues}
              setDropdownValues={setDropdownValues}
            />
          </div>
          <div>
            <input
              type='checkbox'
              defaultChecked={weightCheckBoxStatus}
              onChange={handleWeightCheckBox}
            />
            <span>Weight</span>
            <WeightFilterRange
              setWeight={setWeight}
              minWeight={minWeight}
              setMinWeight={setMinWeight}
              maxWeight={maxWeight}
              setMaxWeight={setMaxWeight}
              checkBoxStatus={weightCheckBoxStatus}
            />
          </div>
        </div>
        <div className='vertical-line-sort'></div>
        <div className='filtering'>
          <span>Sort by:</span>
          <div>
            <input
              type='checkbox'
              onChange={handleSortArrivalTimeAscCheckBox}
            />
            <span>arrival date, asc</span>
          </div>
          <div>
            <input
              type='checkbox'
              onChange={handleSortArrivalDateDescCheckBox}
            />
            <span>arrival date, desc</span>
          </div>
          <div>
            <input type='checkbox' onChange={handleSortWeightAscCheckBox} />
            <span>weight, asc</span>
          </div>
          <div>
            <input type='checkbox' onChange={handleSortWeightDescCheckBox} />
            <span>weight, desc</span>
          </div>
        </div>
      </div>

      <div className='results'>
        <table className='table-search'>
          <tbody>
            <tr>
              <th>#</th>
              <th>customer</th>
              <th>category</th>
              <th>weight</th>
              <th>arrival time</th>
              <th>shipment #</th>
              <th>to warehouse #</th>
              <th>status</th>
              {user.role !== 'support' ? <th></th> : null}
            </tr>

            {parcels.map((parcel) => (
              <tr key={parcel.id}>
                <td>{parcel.id}</td>
                <td>{parcel.user}</td>
                <td>{parcel.category}</td>
                <td>{parcel.weight} kg</td>
                <th>
                  {parcel.arrival_time
                    ? new Date(parcel.arrival_time).toLocaleString()
                    : null}
                </th>
                <th>{parcel.shipment_id}</th>
                <th>{parcel.warehouse_id}</th>
                <th>{!parcel.status ? 'preparing' : parcel.status}</th>
                {user.role !== 'support' ? (
                  <th>
                    {parcel.shipment_id ? (
                      <Button
                        onClick={() => {
                          dispatchSmallButtonsState({
                            type: 'show-shipments-options',
                          });
                          setShipmentForUpdate(parcel.shipment_id);
                          dispatchActionsState({
                            type: 'shipment-create-form',
                          });
                        }}
                      >
                        Shipment
                      </Button>
                    ) : null}
                  </th>
                ) : null}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

ParcelsSearch.propTypes = {
  dispatchActionsState: PropTypes.func,
  setShipmentForUpdate: PropTypes.func,
};

export default ParcelsSearch;
