import React, { useContext, useReducer } from 'react';
import AuthContext from '../../providers/AuthContext';
import './EmployeeMenu.css';
import PropTypes from 'prop-types';

const EmployeeMenu = ({
  dispatchActionsState,
  smallButtonsState,
  dispatchState,
  state,
}) => {
  const { setUser } = useContext(AuthContext);
  const triggerLogout = () => {
    setUser(null);

    localStorage.removeItem('token');
  };

  return (
    <div className='parent'>
      <div className='buttonContainer'>
        <div
          onClick={() => {
            dispatchState({
              type: 'show-customers-options',
              payload: true,
            });
          }}
          className='menu-button'
        >
          Customers
        </div>
        {state.showCustomersOptions ? (
          <div
            className={
              smallButtonsState.showCustomerSearch
                ? 'options-button-clicked'
                : 'options-button'
            }
            onClick={() => dispatchActionsState({ type: 'customer-search' })}
          >
            search
          </div>
        ) : null}
      </div>
      <div className='buttonContainer'>
        <div
          className='menu-button'
          onClick={() => {
            dispatchState({ type: 'show-warehouses-options' });
          }}
        >
          Warehouses
        </div>
        {state.showWarehousesOptions ? (
          <>
            <div
              className={
                smallButtonsState.showWarehouseCreateForm
                  ? 'options-button-clicked'
                  : 'options-button'
              }
              onClick={() => {
                dispatchActionsState({ type: 'warehouse-create-form' });
              }}
            >
              create/modify
            </div>
            <div
              className={
                smallButtonsState.showWarehouseSearch
                  ? 'options-button-clicked'
                  : 'options-button'
              }
              onClick={() => {
                dispatchActionsState({ type: 'warehouse-search' });
              }}
            >
              search
            </div>
          </>
        ) : null}
      </div>
      <div className='buttonContainer'>
        <div
          className='menu-button'
          onClick={() => {
            dispatchState({ type: 'show-shipments-options', payload: true });
          }}
        >
          Shipments
        </div>
        {state.showShipmentsOptions ? (
          <>
            <div
              className={
                smallButtonsState.showShipmentCreateForm
                  ? 'options-button-clicked'
                  : 'options-button'
              }
              onClick={() =>
                dispatchActionsState({ type: 'shipment-create-form' })
              }
            >
              create/modify
            </div>
            <div
              className={
                smallButtonsState.showShipmentSearch
                  ? 'options-button-clicked'
                  : 'options-button'
              }
              onClick={() => dispatchActionsState({ type: 'shipments-search' })}
            >
              search
            </div>
          </>
        ) : null}
      </div>
      <div className='buttonContainer'>
        <div
          className='menu-button'
          onClick={() => {
            dispatchState({ type: 'show-parcels-options' });
          }}
        >
          Parcels
        </div>
        {state.showParcelsOptions ? (
          <>
            <div
              className={
                smallButtonsState.showParcelsCreateForm
                  ? 'options-button-clicked'
                  : 'options-button'
              }
              onClick={() =>
                dispatchActionsState({ type: 'parcels-create-form' })
              }
            >
              create
            </div>
            <div
              className={
                smallButtonsState.showParcelsSearch
                  ? 'options-button-clicked'
                  : 'options-button'
              }
              onClick={() => dispatchActionsState({ type: 'parcels-search' })}
            >
              search
            </div>
          </>
        ) : null}
      </div>

      <div className='button-logout' onClick={() => triggerLogout()}>
        Logout
      </div>
    </div>
  );
};

EmployeeMenu.propTypes = {
  dispatchActionsState: PropTypes.func,
};

export default EmployeeMenu;
