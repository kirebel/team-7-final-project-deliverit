import React from 'react';
import './WeightFilterRange.css';
import PropTypes from 'prop-types';

const WeightFilterRange = ({
  setWeight,
  checkBoxStatus,
  minWeight,
  setMinWeight,
  maxWeight,
  setMaxWeight,
}) => {
  const handleWeightFromInput = (e) => {
    if (checkBoxStatus) {
      setWeight(`weight=${e.target.value}_1000&`);
      setMinWeight(e.target.value);
    }
  };

  const handleWeightToInput = (e) => {
    if (checkBoxStatus) {
      setMaxWeight(e.target.value);
    }
    setWeight(`weight=${minWeight}_${e.target.value}&`);
  };

  return (
    <>
      &nbsp;from&nbsp;
      <input
        className='weight'
        value={minWeight}
        onChange={(e) => handleWeightFromInput(e)}
      ></input>
      &nbsp;to&nbsp;
      <input
        className='weight'
        value={maxWeight}
        onChange={(e) => handleWeightToInput(e)}
      ></input>
      &nbsp;kg
    </>
  );
};

WeightFilterRange.propTypes = {
  setWeight: PropTypes.func,
  checkBoxStatus: PropTypes.bool,
  minWeight: PropTypes.string,
  setMinWeight: PropTypes.func,
  maxWeight: PropTypes.string,
  setMaxWeight: PropTypes.func,
};

export default WeightFilterRange;
