import React, { useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import ParcelsInSingleShipment from '../ParcelsInSingleShipment/ParcelsInSingleShipment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import dateFormatter from '../../Common/date-formatter';
import './ShipmentUpdate.css';
import AuthContext from '../../providers/AuthContext';
import PropTypes from 'prop-types';

const ShipmentUpdateForm = ({
  shipmentForUpdate,
  shipments,
  shipmentParcels,
  setShipmentUpdateInfo,
  updateShipment,
  updateShipmentInfo,
  deleteShipment,
  setShipmentParcels,
  dispatchActionsState,
  setShipmentForUpdate,
  setShipments,
  dispatchSmallButtonsState,
}) => {
  const [departureDate, setDepartureDate] = useState('');
  const [arrivalDate, setArrivalDate] = useState('');

  const { user } = useContext(AuthContext);

  const handleDepartureTimeDatePicker = (date, el) => {
    if (!date) {
      date = new Date();
    }
    setDepartureDate(date);
    setShipmentUpdateInfo(
      'arrival_time',
      dateFormatter(
        el.arrival_time
          ? new Date(el.arrival_time).toLocaleDateString('fr-CA')
          : null
      )
    );
    setShipmentUpdateInfo(
      'departure_time',
      dateFormatter(date.toLocaleDateString())
    );
  };

  const handleArrivalTimeDatePicker = (date, el) => {
    if (!date) {
      date = new Date();
    }
    setArrivalDate(date);
    setShipmentUpdateInfo(
      'departure_time',
      dateFormatter(
        el.departure_time
          ? new Date(el.departure_time).toLocaleDateString()
          : null
      )
    );
    setShipmentUpdateInfo(
      'arrival_time',
      dateFormatter(date.toLocaleDateString())
    );
  };

  const handleUpdateButton = (el) => {
    setDepartureDate('');
    setArrivalDate('');
    setShipmentUpdateInfo('origin_warehouse_id', el.origin_warehouse_id);
    setShipmentUpdateInfo(
      'destination_warehouse_id',
      el.destination_warehouse_id
    );
    setShipmentUpdateInfo(
      'shipment_status',
      !el.arrival_time ? (el.departure_time ? 3 : 2) : 1
    );
    updateShipment(updateShipmentInfo, el.shipment_id);
  };

  return (
    <div>
      <table>
        <tbody>
          <tr>
            <th colSpan='9' className='form-titles'>
              Details Shipment Form
            </th>
          </tr>
          <tr>
            <th colSpan='9'>
              Shipment:{' '}
              <select
                value={shipmentForUpdate}
                onChange={(e) => {
                  setShipmentForUpdate(e.target.value);
                }}
              >
                <option value='default'>Choose here</option>
                {shipments
                  .sort((a, b) => a.shipment_id - b.shipment_id)
                  .filter((el) => el.isDeleted === 0)
                  .map((element) => (
                    <option
                      key={element.shipment_id}
                      value={element.shipment_id}
                    >
                      #{element.shipment_id}&nbsp;from:&nbsp;
                      {element.from_city_name},{element.from_country_name}&nbsp;
                      to:&nbsp;
                      {element.to_city_name},{element.to_country_name}&nbsp;
                      {element.shipment_departure_time
                        ? new Date(element.shipment_departure_time)
                            .toLocaleString()
                            .slice(0, 8)
                        : null}
                      , &nbsp;
                      {10 - element.parcels_count === 0
                        ? 'full'
                        : `parcels to full:${10 - element.parcels_count}`}
                    </option>
                  ))}
              </select>
            </th>
          </tr>
          <tr>
            <th className='small-columns'>shipment #</th>
            <th className='from-to-status'>from</th>
            <th className='from-to-status'>to</th>
            <th className='small-columns'>destination warehouse #</th>
            <th className='time-picker'>departure time</th>
            <th className='time-picker'>arrival time</th>
            <th className='small-columns'>parcels count</th>
            <th className='from-to-status'>status</th>
            <th className='buttons'></th>
          </tr>

          {shipments
            .sort((a, b) => a.shipment_id - b.shipment_id)
            .filter((element) => element.isDeleted === 0)
            .map((el) =>
              el.shipment_id === +shipmentForUpdate ? (
                <tr key={el.shipment_id}>
                  <td>{el.shipment_id}</td>
                  <td>
                    {el.from_city_name},
                    <br />
                    {el.from_country_name}
                  </td>
                  <td>
                    {el.to_city_name},
                    <br />
                    {el.to_country_name}
                  </td>
                  <td>{el.destination_warehouse_id}</td>
                  <td>
                    {el.departure_time ? (
                      new Date(el.departure_time).toLocaleDateString()
                    ) : (
                      <DatePicker
                        placeholderText={new Date().toLocaleDateString()}
                        selected={departureDate}
                        className='customDatePickerWidth'
                        onChange={(date) =>
                          handleDepartureTimeDatePicker(date, el)
                        }
                      />
                    )}
                  </td>
                  <td>
                    {el.arrival_time ? (
                      new Date(el.arrival_time).toLocaleDateString()
                    ) : el.shipment_status !== 1 ? (
                      <DatePicker
                        placeholderText={new Date().toLocaleDateString()}
                        selected={arrivalDate}
                        className='customDatePickerWidth'
                        onChange={(date) =>
                          handleArrivalTimeDatePicker(date, el)
                        }
                      />
                    ) : null}
                  </td>
                  <td>{el.parcels_count}</td>
                  <td>
                    {el.shipment_status === 1
                      ? 'preparing'
                      : el.shipment_status === 2
                      ? 'on the way'
                      : 'completed'}
                  </td>
                  <td>
                    <Button
                      disabled={
                        (el.arrival_time && el.departure_time) ||
                        (user.city !== el.to_city_name &&
                          user.city !== el.from_city_name) ||
                        !departureDate
                          ? !arrivalDate
                            ? true
                            : false
                          : false
                      }
                      onClick={() => handleUpdateButton(el)}
                    >
                      Update
                    </Button>
                    <br />
                    <Button
                      disabled={el.shipment_status !== 1}
                      style={{ marginTop: '2px' }}
                      onClick={() => deleteShipment(el.shipment_id)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ) : null
            )}
        </tbody>
      </table>
      <ParcelsInSingleShipment
        dispatchSmallButtonsState={dispatchSmallButtonsState}
        setShipments={setShipments}
        shipmentParcels={shipmentParcels}
        shipmentForUpdate={shipmentForUpdate}
        setShipmentParcels={setShipmentParcels}
        dispatchActionsState={dispatchActionsState}
        shipments={shipments}
      />
    </div>
  );
};

ShipmentUpdateForm.propTypes = {
  shipmentForUpdate: PropTypes.number,
  shipments: PropTypes.array,
  shipmentParcels: PropTypes.array,
  setShipmentUpdateInfo: PropTypes.func,
  updateShipment: PropTypes.func,
  updateShipmentInfo: PropTypes.object,
  deleteShipment: PropTypes.func,
  setShipmentParcels: PropTypes.func,
  dispatchActionsState: PropTypes.func,
  setShipmentForUpdate: PropTypes.func,
  setShipments: PropTypes.func,
};

export default ShipmentUpdateForm;
