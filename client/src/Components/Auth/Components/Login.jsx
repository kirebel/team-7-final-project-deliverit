import AuthContext from '../../../providers/AuthContext';
import { useContext, useState } from 'react';
import { getUserFromToken } from '../../../Common/token';
import { logInRequest } from '../../../Requests/Auth';
import { Button } from 'react-bootstrap';
import '../CSS/Auth.css';
import Modal from 'react-bootstrap/Modal';
import React from 'react';
import { useHistory } from 'react-router-dom';

const Login = () => {
  const history = useHistory();

  const [authForm, setAuthForm] = useState({
    username: {
      name: 'username',
      placeholder: 'username',
      value: '',
      type: 'text',
    },
    password: {
      name: 'password',
      placeholder: 'password',
      value: '',
      type: 'password',
    },
  });

  const { setUser } = useContext(AuthContext);
  const [show, setShow] = useState(false);
  const [invalidLogin, setInvalidLogin] = useState(false);


  const handleClose = () => {
    authForm.username.value = '';
    authForm.password.value = '';
    setInvalidLogin(false);
    setShow(false);
  };
  const handleShow = () => setShow(true);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    const updatedControl = { ...authForm[name], value };
    const updatedForm = { ...authForm, [name]: updatedControl };
    setAuthForm(updatedForm);
  };

  const login = (ev) => {
    ev.preventDefault();

    const body = {
      username: authForm.username.value,
      password: authForm.password.value,
    };

 logInRequest(body.username, body.password).then(({ token }) => {
      if (token) {
        handleClose();
        localStorage.setItem('token', token);
        setUser(getUserFromToken(token));
        const userr = getUserFromToken(token);
       
        if (userr.role === 'employee') {
          history.push('/eview');
        } else if (userr.role === 'customer') {
          history.push('/customer-view');
        } else {
          history.push('/customer-support');
        }

       
      } else {
        setInvalidLogin(true);
      }
    });
  };

  const handleKeypress = (e) => {
    if (e.charCode === 13) {
      login(e);
    }
  };

  const formElements = Object.keys(authForm)
    .map((name) => ({ name, config: authForm[name] }))
    .map(({ name, config }) => {
      return (
        <input
          type={config.type}
          key={name}
          name={name}
          placeholder={config.placeholder}
          value={config.value}
          onChange={handleInputChange}
          onKeyPress={(e) => handleKeypress(e)}
        />
      );
    });

  return (
    <>
      <Button variant='primary' size='lg' onClick={handleShow}>
        Login
      </Button>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop='static'
        keyboard={false}
        animation={false}
      >
        <Modal.Header>
          <Modal.Title>Login Form</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className='login-input-wrapper'>{formElements}</div>
          {invalidLogin ? (
            <div className={'invalid-credentials'}>Invalid credentials!</div>
          ) : (
            ''
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' onClick={login}>
            Login
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Login;
