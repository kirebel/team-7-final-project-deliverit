import Login from './Login';
import Register from './Register';
import React from 'react';

const Auth = () => {
  return (
    <>
      <Login />
      <Register />
    </>
  );
};

export default Auth;
