import React from 'react';
import { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../CSS/Auth.css';
import { Button } from 'react-bootstrap';
import { registerRequest } from '../../../Requests/Auth';
import { bgCities } from '../../../Common/bg-cities';
import { espCities } from '../../../Common/esp-cities';
import { createCity } from '../../../Requests/City';
import PropTypes from 'prop-types';

const validators = {
  first_name: (value) => value.length > 2 && value.length < 20,
  last_name: (value) => value.length > 2 && value.length < 20,
  email: (value) => /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(value),
  street: (value) => value.length > 2 && value.length < 60,
};
const RegisterPartTwo = ({ userDetails, isRegistered }) => {
  const { username, password } = userDetails;

  const [authForm, setAuthForm] = useState({
    first_name: {
      name: 'first_name',
      placeholder: 'First Name',
      value: '',
      type: 'text',
      valid: true,
      isTouched: false,
    },
    last_name: {
      name: 'last_name:',
      placeholder: 'Last Name',
      value: '',
      type: 'text',
      valid: true,
      isTouched: false,
    },
    email: {
      name: 'email',
      placeholder: 'Email',
      value: '',
      type: 'text',
      valid: true,
      isTouched: false,
    },
    street: {
      name: 'street',
      placeholder: 'Street',
      value: '',
      type: 'text',
      valid: true,
      isTouched: false,
    },
  });

  const [formValid, setFormValid] = useState(false);
  const [show, setShow] = useState(true);
  const [country, setCountry] = useState('Bulgaria');
  const [city, setCity] = useState('Aheloy');

  const handleClose = () => {
    authForm.first_name.value = '';
    authForm.last_name.value = '';
    authForm.email.value = '';
    authForm.street.value = '';
    authForm.first_name.isTouched = false;
    authForm.last_name.isTouched = false;
    authForm.email.isTouched = false;
    authForm.street.isTouched = false;
    isRegistered(true);
    setShow(false);
  };
  const handleShow = () => setShow(true);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    const updatedInput = authForm[name];
    updatedInput.value = value;
    updatedInput.valid = validators[name](value);
    updatedInput.isTouched = true;

    setAuthForm({ ...authForm, [name]: updatedInput });

    const formValid = Object.values(authForm).every(
      (element) => element.valid && element.isTouched
    );
    setFormValid(formValid);
  };

  const register = (ev) => {
    ev.preventDefault();

    if (!formValid) {
      return;
    }
    const registerDetails = {
      username: username,
      password,
      role: 'customer',
      first_name: authForm.first_name.value,
      last_name: authForm.last_name.value,
      email: authForm.email.value,
      street: authForm.street.value,
      country,
      city,
    };

    createCity(city, country);
    registerRequest(registerDetails);
    isRegistered(true);
    alert('You have registered successfully!')
    handleClose();
  };

  const formElements = Object.keys(authForm)
    .map((name) => ({ name, config: authForm[name] }))
    .map(({ name, config }) => {
      return (
        <div key={name}>
          <input
            type={config.type}
            name={name}
            placeholder={config.placeholder}
            value={config.value}
            onChange={handleInputChange}
            className={config.valid ? '' : 'not-valid-input'}
          />
          {config.name === 'first_name' && !config.valid ? (
            <div>First name must be between 2 and 20 characters!</div>
          ) : (
            <></>
          )}

          {name === 'last_name' && !config.valid ? (
            <div>Last name must be between 2 and 20 characters!</div>
          ) : (
            <></>
          )}

          {config.name === 'email' && !config.valid ? (
            <div>Not a valid email!</div>
          ) : (
            <></>
          )}

          {config.name === 'street' && !config.valid ? (
            <div>Street must be between 2 and 60 characters</div>
          ) : (
            <></>
          )}
        </div>
      );
    });

  return (
    <>
      <Button variant='danger' onClick={handleShow}>
        Register
      </Button>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop='static'
        keyboard={false}
        animation={false}
      >
        <Modal.Header>
          <Modal.Title>Register Form</Modal.Title>
        </Modal.Header>
        <Modal.Body className='registerBody'>
          <form onSubmit={register}>
            <h3>Please provide the additional info</h3>
            {formElements}
          </form>
          <select onClick={(e) => setCountry(e.target.value)}>
            <option value='Bulgaria'>Bulgaria</option>
            <option value='Spain'>Spain</option>
          </select>

          <select onClick={(e) => setCity(e.target.value)}>
            {(country === 'Bulgaria' ? bgCities : espCities).map((city) => (
              <option key={city} value={city}>
                {city}
              </option>
            ))}
          </select>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' disabled={!formValid} onClick={register}>
            Register
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

RegisterPartTwo.propTypes = {
  userDetails: PropTypes.object,
  isRegistered: PropTypes.func,
};

export default RegisterPartTwo;
