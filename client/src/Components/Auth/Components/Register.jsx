import React from 'react';
import { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../CSS/Auth.css';
import { Button } from 'react-bootstrap';
import { checkUsername } from '../../../Requests/Auth';
import RegisterPartTwo from './RegisterPartTwo';

const Register = () => {
  const validators = {
    username: (value) => value.length > 2 && value.length < 20,
    password: (value) =>
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
        value
      ),
    confirmPassword: (value) => value === authForm.password.value,
  };

  const [continueForm, setContinueForm] = useState(false);
  const [authForm, setAuthForm] = useState({
    username: {
      name: 'username',
      placeholder: 'username',
      value: '',
      type: 'text',
      valid: true,
      isTouched: false,
    },
    password: {
      name: 'password',
      placeholder: 'password',
      value: '',
      type: 'password',
      valid: true,
      isTouched: false,
    },
    confirmPassword: {
      name: 'confirm-password',
      placeholder: 'confirm password',
      value: '',
      type: 'password',
      valid: true,
      isTouched: false,
    },
  });
  const [show, setShow] = useState(false);
  const [formValid, setFormValid] = useState(false);
  const [validUsername, setValidUsername] = useState(true);

  const handleClose = () => {
    authForm.username.value = '';
    authForm.password.value = '';
    authForm.confirmPassword.value = '';
    authForm.username.isTouched = false;
    authForm.password.isTouched = false;
    authForm.confirmPassword.isTouched = false;
    authForm.username.valid = true;
    authForm.password.valid = true;
    authForm.confirmPassword.valid = true;
    setValidUsername(true);
    setFormValid(false);
    setShow(false);
  };
  const handleShow = () => setShow(true);

  const handleInputChange = async (event) => {
    const { name, value } = event.target;

    const updatedInput = authForm[name];
    updatedInput.value = value;
    updatedInput.valid = validators[name](value);
    updatedInput.isTouched = true;

    const checkUsernameExists = await checkUsername({
      username: authForm.username.value,
    });

    if (Object.keys(checkUsernameExists)[0] === 'exist') {
      setValidUsername(false);
      updatedInput.valid = false;
    } else {
      setValidUsername(true);
    }

    setAuthForm({ ...authForm, [name]: updatedInput });

    const formValid = Object.values(authForm).every(
      (element) => element.valid && element.isTouched
    );
    setFormValid(formValid);
  };

  const register = (ev) => {
    if (!formValid) {
      return;
    }
    ev.preventDefault();
    setContinueForm(true);
  };

  const formElements = Object.keys(authForm)
    .map((name) => ({ name, config: authForm[name] }))
    .map(({ name, config }) => {
      return (
        <div key={name}>
          <input
            type={config.type}
            name={name}
            placeholder={config.placeholder}
            value={config.value}
            onChange={handleInputChange}
            className={config.valid ? '' : 'not-valid-input'}
          />

          {config.name === 'password' && !config.valid ? (
            <div>
              Password must be minimum eight characters, at least one letter,
              one number and one special character!
            </div>
          ) : (
            <></>
          )}

          {config.name === 'confirm-password' && !config.valid ? (
            <div>Passwords does not match!</div>
          ) : (
            <></>
          )}
          {config.name === 'username' && !config.valid && validUsername ? (
            <div>Username must be between 2 and 20 characters!</div>
          ) : (
            <></>
          )}
          {config.name === 'username' && !validUsername ? (
            <div>Username already exists!</div>
          ) : (
            <></>
          )}
        </div>
      );
    });

  const registered = (value) => {
    if (value) {
      setContinueForm(false);
      handleClose();
    }
  };

  return (
    <>
      {!continueForm ? (
        <>
          <Button variant='primary' size='lg' onClick={handleShow}>
            Register
          </Button>
          <Modal
            show={show}
            onHide={handleClose}
            backdrop='static'
            keyboard={false}
            animation={false}
          >
            <Modal.Header>
              <Modal.Title>Register Form</Modal.Title>
            </Modal.Header>
            <Modal.Body className='register-body-wrapper'>
              <form onSubmit={register}>{formElements}</form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant='secondary' onClick={handleClose}>
                Close
              </Button>
              <Button
                variant='primary'
                onClick={register}
                disabled={!formValid}
              >
                Register
              </Button>
            </Modal.Footer>
          </Modal>
        </>
      ) : (
        <RegisterPartTwo
          userDetails={{
            username: authForm.username.value,
            password: authForm.password.value,
          }}
          isRegistered={registered}
        />
      )}
    </>
  );
};

export default Register;
