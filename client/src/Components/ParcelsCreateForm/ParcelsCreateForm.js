import React, { useState, useEffect } from 'react';
import './ParcelsCreateForm.css';
import { createParcel } from '../../Requests/Parcels-requests';
import ShipmentsDropdown from '../DropdownLists/ShipmentsDropdownList';
import WaitingParcels from '../WaitingParcels/WaitingParcels';
import { Button } from 'react-bootstrap';
import { getCategories } from '../../Requests/Categories-requests';
import Orders from '../Orders/Orders';
import { getParcels } from '../../Requests/Parcels-requests';
import UsersDropdown from '../DropdownLists/UsersDropdown';
import { getOrders, updateOrderStatus } from '../../Requests/Orders-request';

const ParcelsCreateForm = () => {
  const [homeDeliveryCheckBoxState, setHomeDeliveryCheckBoxState] =
    useState(false);

  const [categories, setCategories] = useState([]);

  const [createParcelDropdownValues, setCreateParcelDropdownValues] = useState({
    weightInput: '',
    categoryDropdown: '',
    customerDropdown: '',
    shipmentsDropdown: '',
  });

  const [waitingParcels, setWaitingParcels] = useState([]);
  const [orders, setOrders] = useState([]);

  const [waitingParcelCheckBoxState, setWaitingParcelCheckBoxState] =
    useState(false);

  const [attachParcelToShipment, setAttachParcelToShipment] = useState({
    shipment_id: '',
    warehouse_id: '',
  });

  const [createParcelInfo, setCreateParcelInfo] = useState({
    home_delivery: 1,
    user_id: '',
    warehouse_id: '',
    shipment_id: '',
    category_id: '',
    weight: '',
    customer_city: '',
    customer_country: '',
  });

  const [orderId, setOrderId] = useState('');

  useEffect(() => {
    getCategories().then((data) => setCategories(data));
    getParcels('').then((data) => setWaitingParcels(data));
    getOrders().then((data) => setOrders(data));
  }, []);

  const updateDropdown = (data) => {
    setCreateParcelDropdownValues({ ...createParcelDropdownValues, ...data });
  };

  const updateInfo = (prop, value) => {
    createParcelInfo[prop] = value;
    setCreateParcelInfo({ ...createParcelInfo });
  };

  const createNewParcel = () => {
    createParcel(createParcelInfo).then((data) => {
      if (data.shipment_id === null) {
        setWaitingParcels([
          ...waitingParcels,
          { ...data, ...createParcelInfo },
        ]);
      }
    });
  };

  const orderToParcel = (id) => {
    updateOrderStatus(id).then((response) => {
      setOrders([
        ...orders.map((order) =>
          order.id === id ? { ...order, ...response } : order
        ),
      ]);
    });
  };

  const handleDoneButton = () => {
    createNewParcel(createParcelInfo);
    orderToParcel(+orderId);
    setWaitingParcelCheckBoxState(false);
    setOrderId('');
    updateDropdown({
      weightInput: '',
      categoryDropdown: '',
      shipmentsDropdown: '',
      customerDropdown: '',
    });
    setCreateParcelInfo({
      home_delivery: 1,
      user_id: '',
      warehouse_id: '',
      shipment_id: '',
      category_id: '',
      weight: '',
      customer_city: '',
      customer_country: '',
    });
    alert('Parcel successfully created!');
  };

  const handleWeightInput = (e) => {
    updateDropdown({ weightInput: e.target.value });
    updateInfo('weight', +e.target.value);
  };

  const handleCategoryDropdown = (e) => {
    updateInfo('category_id', +e.target.value);
    updateDropdown({ categoryDropdown: e.target.value });
  };

  const [shipmentIdState, setShipmentIdState] = useState([]);

  const handleWaitingParcelsCheckBox = () => {
    if (!waitingParcelCheckBoxState) {
      updateInfo('shipment_id', null);
      updateInfo('warehouse_id', null);
    } else {
      updateInfo('shipment_id', shipmentIdState);
    }
    setWaitingParcelCheckBoxState(!waitingParcelCheckBoxState);
  };

  const handleHomeDeliveryInputOnChange = () => {
    if (!homeDeliveryCheckBoxState) {
      updateInfo('home_delivery', 2);
    } else {
      updateInfo('home_delivery', 1);
    }
  };

  return (
    <div className='main-div'>
      <Orders orders={orders} />
      <table>
        <tbody>
          <tr>
            <th colSpan='7' className='form-titles'>
              Create Parcel Form
            </th>
          </tr>
          <tr>
            <th>order #</th>
            <th>weight</th>
            <th>customer</th>
            <th>category</th>
            <th className='shipment-dropdown-column'>shipment </th>
            <th>home delivery</th>
            <th></th>
          </tr>
          <tr>
            <td>
              <input
                value={orderId}
                className='createParcelsInput'
                onChange={(e) => {
                  setOrderId(e.target.value);
                }}
              ></input>
            </td>
            <td>
              <input
                value={createParcelDropdownValues.weightInput}
                className='createParcelsInput'
                onChange={(e) => handleWeightInput(e)}
              ></input>
            </td>
            <td>
              <UsersDropdown
                updateInfo={updateInfo}
                createParcelDropdownValues={createParcelDropdownValues}
                updateDropdown={updateDropdown}
              />
            </td>
            <td>
              <select
                value={createParcelDropdownValues.categoryDropdown}
                onChange={(e) => handleCategoryDropdown(e)}
              >
                <option value='default'>Choose here</option>
                {categories.map((element) => (
                  <option key={element.id} value={element.id}>
                    {element.name}
                  </option>
                ))}
              </select>
            </td>
            <th>
              <ShipmentsDropdown
                setShipmentIdState={setShipmentIdState}
                updateInfo={updateInfo}
                waitingParcelCheckBoxState={waitingParcelCheckBoxState}
                createParcelDropdownValues={createParcelDropdownValues}
                updateDropdown={updateDropdown}
              />
              <br />
              <input
                style={{ marginLeft: '5px' }}
                type='checkbox'
                checked={waitingParcelCheckBoxState}
                onChange={handleWaitingParcelsCheckBox}
              ></input>
              waiting
            </th>
            <th>
              <input
                className='createParcelsInput'
                type='checkbox'
                onClick={() =>
                  setHomeDeliveryCheckBoxState(!homeDeliveryCheckBoxState)
                }
                onChange={handleHomeDeliveryInputOnChange}
              ></input>
            </th>
            <th>
              <Button
                disabled={
                  Object.values(createParcelInfo).some(
                    (element) => element === ''
                  ) ||
                  isNaN(createParcelInfo.user_id) ||
                  isNaN(createParcelInfo.category_id) ||
                  isNaN(createParcelInfo.shipment_id) ||
                  isNaN(createParcelInfo.weight) ||
                  (!waitingParcelCheckBoxState &&
                    !createParcelInfo.shipment_id) ||
                  !orderId ||
                  isNaN(orderId) ||
                  createParcelInfo.weight <= 0
                }
                variant='primary'
                onClick={handleDoneButton}
              >
                Done
              </Button>
            </th>
          </tr>
        </tbody>
      </table>
      <hr className='hr'></hr>
      <WaitingParcels
        waitingParcels={waitingParcels}
        updateInfo={updateInfo}
        attachParcelToShipment={attachParcelToShipment}
        setAttachParcelToShipment={setAttachParcelToShipment}
        setWaitingParcels={setWaitingParcels}
      />
    </div>
  );
};

export default ParcelsCreateForm;
