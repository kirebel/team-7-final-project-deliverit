import React, { useReducer, useState } from 'react';
import EmployeeMenu from '../EmployeeMenu/EmployeeMenu';
import ParcelsCreateForm from '../ParcelsCreateForm/ParcelsCreateForm';
import ParcelsSearch from '../ParcelsSearch/ParcelsSearch';
import WarehousesCreate from '../Warehouses/WarehousesCreate';
import ShipmentsCreateAndUpdateForm from '../ShipmentsCreateAndUpdateForm/UpdateForm/ShipmentsCreateAndUpdateView';
import ShipmentSearch from '../ShipmentSearch/ShipmentSearch';
import UsersSearch from '../UsersSearch/UsersSearch';
import WarehousesView from '../Warehouses/WarehousesView';
import Header from '../Main/Header';
import './EmployeeView.css';

const actionsReducer = (state, action) => {
  switch (action.type) {
    case 'parcels-search': {
      return {
        ...state,
        showParcelsSearch: true,
        showParcelsCreateForm: false,
        showWarehouseSearch: false,
        showWarehouseCreateForm: false,
        showShipmentSearch: false,
        showShipmentCreateForm: false,
        showCustomerSearch: false,
      };
    }
    case 'parcels-create-form': {
      return {
        ...state,
        showParcelsSearch: false,
        showParcelsCreateForm: true,
        showWarehouseSearch: false,
        showWarehouseCreateForm: false,
        showShipmentSearch: false,
        showShipmentCreateForm: false,
        showCustomerSearch: false,
      };
    }
    case 'warehouse-search': {
      return {
        ...state,
        showParcelsSearch: false,
        showParcelsCreateForm: false,
        showWarehouseSearch: true,
        showWarehouseCreateForm: false,
        showShipmentSearch: false,
        showShipmentCreateForm: false,
        showCustomerSearch: false,
      };
    }
    case 'warehouse-create-form': {
      return {
        ...state,
        showParcelsSearch: false,
        showParcelsCreateForm: false,
        showWarehouseSearch: false,
        showWarehouseCreateForm: true,
        showShipmentSearch: false,
        showShipmentCreateForm: false,
        showCustomerSearch: false,
      };
    }

    case 'shipment-create-form': {
      return {
        ...state,
        showParcelsSearch: false,
        showParcelsCreateForm: false,
        showWarehouseSearch: false,
        showWarehouseCreateForm: false,
        showShipmentSearch: false,
        showShipmentCreateForm: true,
        showCustomerSearch: false,
      };
    }
    case 'shipments-search': {
      return {
        ...state,
        showParcelsSearch: false,
        showParcelsCreateForm: false,
        showWarehouseSearch: false,
        showWarehouseCreateForm: false,
        showShipmentSearch: true,
        showShipmentCreateForm: false,
        showCustomerSearch: false,
      };
    }

    case 'customer-search': {
      return {
        ...state,
        showParcelsSearch: false,
        showParcelsCreateForm: false,
        showWarehouseSearch: false,
        showWarehouseCreateForm: false,
        showShipmentSearch: false,
        showShipmentCreateForm: false,
        showCustomerSearch: true,
      };
    }
    default:
      return state;
  }
};

const menuReducer = (state, action) => {
  switch (action.type) {
    case 'show-customers-options': {
      return {
        ...state,
        showCustomersOptions: true,
        showWarehousesOptions: false,
        showShipmentsOptions: false,
        showParcelsOptions: false,
      };
    }
    case 'show-warehouses-options': {
      return {
        ...state,
        showWarehousesOptions: true,
        showCustomersOptions: false,
        showShipmentsOptions: false,
        showParcelsOptions: false,
      };
    }
    case 'show-shipments-options': {
      return {
        ...state,
        showShipmentsOptions: true,
        showCustomersOptions: false,
        showWarehousesOptions: false,
        showParcelsOptions: false,
      };
    }
    case 'show-parcels-options': {
      return {
        ...state,
        showParcelsOptions: true,
        showCustomersOptions: false,
        showShipmentsOptions: false,
        showWarehousesOptions: false,
      };
    }
    default:
      return state;
  }
};

const EmployeeView = () => {
  const [bigButtonsState, dispatchBigButtonsState] = useReducer(
    actionsReducer,
    {
      showParcelsCreateForm: true,
    }
  );
  const [smallButtonsState, dispatchSmallButtonsStateState] = useReducer(
    menuReducer,
    {
      showParcelsOptions: true,
    }
  );

  const [shipmentForUpdate, setShipmentForUpdate] = useState(0);
  const [warehouseForUpdate, setWarehouseForUpdate] = useState('');

  return (
    <div>
      <div className='header-eview'>
        <Header></Header>
      </div>
      <div className='employee-view'>
        <div className='menu-wrapper'>
          <EmployeeMenu
            dispatchActionsState={dispatchBigButtonsState}
            smallButtonsState={bigButtonsState}
            state={smallButtonsState}
            dispatchState={dispatchSmallButtonsStateState}
          />
        </div>
        {bigButtonsState.showParcelsSearch ? (
          <ParcelsSearch
            dispatchActionsState={dispatchBigButtonsState}
            dispatchSmallButtonsState={dispatchSmallButtonsStateState}
            setShipmentForUpdate={setShipmentForUpdate}
          />
        ) : null}
        {bigButtonsState.showParcelsCreateForm ? <ParcelsCreateForm /> : null}
        {bigButtonsState.showShipmentSearch ? (
          <ShipmentSearch
            setShipmentForUpdate={setShipmentForUpdate}
            dispatchActionsState={dispatchBigButtonsState}
          />
        ) : null}
        {bigButtonsState.showShipmentCreateForm ? (
          <ShipmentsCreateAndUpdateForm
            dispatchSmallButtonsState={dispatchSmallButtonsStateState}
            shipmentForUpdate={shipmentForUpdate}
            setShipmentForUpdate={setShipmentForUpdate}
            dispatchActionsState={dispatchBigButtonsState}
          />
        ) : null}
        {bigButtonsState.showCustomerSearch ? (
          <UsersSearch
            setShipmentForUpdate={setShipmentForUpdate}
            dispatchActionsState={dispatchBigButtonsState}
            dispatchSmallButtonsState={dispatchSmallButtonsStateState}
          />
        ) : null}
        {bigButtonsState.showWarehouseSearch ? (
          <WarehousesView
            dispatchActionsState={dispatchBigButtonsState}
            setWarehouseForUpdate={setWarehouseForUpdate}
          />
        ) : null}
        {bigButtonsState.showWarehouseCreateForm ? (
          <WarehousesCreate
            setWarehouseForUpdate={setWarehouseForUpdate}
            warehouseForUpdate={warehouseForUpdate}
          />
        ) : null}
      </div>
    </div>
  );
};

export default EmployeeView;
