import React from 'react';
import { Button } from 'react-bootstrap';
import { dropParcelFromShipment } from '../../Requests/Parcels-requests';
import './ParcelsInSingleShipment.css';
import PropTypes from 'prop-types';

const ParcelsInSingleShipment = ({
  shipmentParcels,
  shipmentForUpdate,
  setShipmentParcels,
  dispatchActionsState,
  shipments,
  setShipments,
  dispatchSmallButtonsState,
}) => {
  const dropParcels = (parcelId) => {
    setShipmentParcels([
      ...shipmentParcels.map((element) =>
        element.id === parcelId
          ? dropParcelFromShipment(
              {
                ...element,
                shipment_id: null,
                warehouse_id: null,
              },
              element.id
            )
          : element
      ),
    ]);
    setShipments([
      ...shipments.map((element) =>
        element.shipment_id === +shipmentForUpdate
          ? { ...element, parcels_count: element.parcels_count - 1 }
          : element
      ),
    ]);
  };

  const attachParcelsButtonDisableHelpConstant = shipments.find(
    (element) => element.shipment_id === +shipmentForUpdate
  );

  return (
    <div className='parcels-in-single-shipment'>
      <table className='shipment-parcels-waiting'>
        <tbody>
          <tr>
            <th colSpan='8' className='form-subtitles'>
              Shipment Parcels
            </th>
          </tr>
          <tr>
            <th className='number-columns'>#</th>
            <th className='string-columns'>customer</th>
            <th className='string-columns'>category</th>
            <th className='number-columns'>weight</th>
            <th className='string-columns'>arrival time</th>
            <th className='number-columns'>shipment #</th>
            <th className='number-columns'>to warehouse #</th>
            <th className='number-columns'></th>
          </tr>

          {shipmentParcels
            .filter(
              (element) =>
                element.shipment_id === +shipmentForUpdate &&
                element.shipment_id !== null
            )
            .map((parcel) => (
              <tr key={parcel.id}>
                <td>{parcel.id}</td>
                <td>{parcel.user}</td>
                <td>{parcel.category}</td>
                <td>{parcel.weight} kg</td>
                <th>
                  {parcel.arrival_time
                    ? new Date(parcel.arrival_time).toLocaleDateString()
                    : null}
                </th>
                <th>{parcel.shipment_id}</th>
                <th>{parcel.warehouse_id}</th>
                <th>
                  <Button
                    disabled={
                      shipments.find(
                        (el) => el.shipment_id === parcel.shipment_id
                      )
                        ? shipments.find(
                            (el) => el.shipment_id === parcel.shipment_id
                          ).shipment_status !== 1
                          ? true
                          : false
                        : false
                    }
                    onClick={() => dropParcels(parcel.id)}
                  >
                    Drop
                  </Button>
                </th>
              </tr>
            ))}
          <tr>
            <th colSpan='8'>
              <Button
                disabled={
                  attachParcelsButtonDisableHelpConstant
                    ? attachParcelsButtonDisableHelpConstant.parcels_count ===
                        10 ||
                      attachParcelsButtonDisableHelpConstant.shipment_status !==
                        1
                      ? true
                      : false
                    : false
                }
                onClick={() => {
                  dispatchActionsState({ type: 'parcels-create-form' });
                  dispatchSmallButtonsState({ type: 'show-parcels-options' });
                }}
              >
                Attach parcels to shipment
              </Button>
            </th>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

ParcelsInSingleShipment.propTypes = {
  shipmentParcels: PropTypes.array,
  shipmentForUpdate: PropTypes.number,
  setShipmentParcels: PropTypes.func,
  dispatchActionsState: PropTypes.func,
  shipments: PropTypes.array,
  setShipments: PropTypes.func,
};

export default ParcelsInSingleShipment;
