import React from 'react';
import './AllParcelsView.css';
import PropTypes from 'prop-types';

const AllParcelsView = ({ status, parcels }) => {

  return (
    <>
    
      {parcels
        .filter((parcel) => {
          if (status !== 'show all') {
            if (parcel.status === null) {
              parcel.status = 'preparing';
            }
            return parcel.status.includes(status);
          }
          return parcel;
        })
        .map((parcel) => (
          <div className='parcel' key={parcel.id}>
            <div className='parcel-info'>Parcel #: {parcel.id}</div>
            <div className='parcel-info'>
              Status: {parcel.status ? parcel.status : 'preparing'}
            </div>
            <div className='parcel-info'>
              Sent Date:{' '}
              {parcel.sent_date ? (
                new Date(parcel.sent_date).toLocaleString().split(',')[0]
              ) : (
                <></>
              )}
            </div>
            <div className='parcel-info'>
              Arrival Date:{' '}
              {parcel.arrival_time ? (
                new Date(parcel.arrival_time).toLocaleString().split(',')[0]
              ) : (
                <></>
              )}
            </div>
          </div>
        ))}
    </>
  );
};

AllParcelsView.propTypes ={
  status: PropTypes.string,
  parcels: PropTypes.array
}

export default AllParcelsView;
