import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { getParcels } from '../../Requests/Parcels-requests';
import './CustomerIncomingParcels.css';
import PropTypes from 'prop-types';

const CustomerIncomingParcels = ({
  customerUsernameForQueryIncomingParcels,
  setShipmentForUpdate,
  dispatchActionsState,
  windowHeight,
  dispatchSmallButtonsState,
}) => {
  const [incomingParcels, setIncomingParcels] = useState([]);

  useEffect(() => {
    getParcels('').then((data) => setIncomingParcels(data));
  }, []);

  const handleSeeShipmentButton = (parcel) => {
    dispatchActionsState({
      type: 'shipment-create-form',
    });
    setShipmentForUpdate(parcel.shipment_id);
    dispatchSmallButtonsState({ type: 'show-shipments-options' });
  };

  return (
    <div
      className='incoming-parcels-results'
      style={
        incomingParcels.filter(
          (element) => element.user === customerUsernameForQueryIncomingParcels
        ).length >= 7
          ? { overflowY: 'auto', marginTop: `${windowHeight - 100}px` }
          : { marginTop: `${windowHeight - 100}px` }
      }
    >
      <table>
        <tbody>
          <tr>
            <th>parcel #</th>
            <th>shipment #</th>
            <th>category</th>
            <th>status</th>
            <th className='shipment-button'></th>
          </tr>

          {incomingParcels
            .sort((a, b) => a.id - b.id)
            .filter(
              (element) =>
                element.status !== 'completed' &&
                element.user === customerUsernameForQueryIncomingParcels
            )
            .map((parcel) => {
              if (parcel) {
                return (
                  <tr key={parcel.id}>
                    <td>{parcel.id}</td>
                    <td>{parcel.shipment_id}</td>
                    <td>{parcel.category}</td>
                    <td>
                      {parcel.status || parcel.status === 'preparing'
                        ? parcel.status
                        : 'preparing'}
                    </td>
                    <td>
                      {parcel.shipment_id ? (
                        <Button onClick={() => handleSeeShipmentButton(parcel)}>
                          See shipment
                        </Button>
                      ) : null}
                    </td>
                  </tr>
                );
              }
              return null;
            })}
        </tbody>
      </table>
    </div>
  );
};

CustomerIncomingParcels.propTypes = {
  customerUsernameForQueryIncomingParcels: PropTypes.string,
  setShipmentForUpdate: PropTypes.func,
  dispatchActionsState: PropTypes.func,
  windowHeight: PropTypes.number,
};

export default CustomerIncomingParcels;
