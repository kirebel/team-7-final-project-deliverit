import React, { useState } from 'react';
import { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { bgCities } from '../../Common/bg-cities';
import { espCities } from '../../Common/esp-cities';
import { createCity } from '../../Requests/City';
import {
  createWarehouse,
  getWarehouses,
} from '../../Requests/Warehouses-requests';
import WarehousesUpdate from './WarehousesUpdate';
import PropTypes from 'prop-types';

const validators = {
  street: (value) => value.length > 2 && value.length < 60,
};
const WarehousesCreate = ({ warehouseForUpdate, setWarehouseForUpdate }) => {
  const [street, setStreet] = useState({
    name: 'street',
    placeholder: 'Street',
    value: '',
    type: 'text',
    valid: true,
    isTouched: false,
  });

  const [country, setCountry] = useState('Bulgaria');
  const [city, setCity] = useState('Aheloy');
  const [isValid, setIsValid] = useState(true);
  const [alreadyExists, setAlreadyExists] = useState(false);
  const [warehouses, setWarehouses] = useState([]);
  const [cityCreated, setCityCreated] = useState(false);

  useEffect(() => {
    getWarehouses().then((data) => setWarehouses(data));
  }, []);

  const validateStreet = (event) => {
    const value = event.target.value;
    const updatedStreet = street;
    updatedStreet.value = value;
    updatedStreet.valid = validators['street'](value);
    updatedStreet.isTouched = true;
    setStreet({ ...street, updatedStreet });
    setAlreadyExists(false);
    if (street.valid === true) {
      setIsValid(false);
    } else {
      setIsValid(true);
    }
  };

  const create = () => {
    createCity(city, country);
    setCityCreated(true);
    
  };

const confirmWarehouseCreate = ()=>{
  createWarehouse({ street: street.value, country, city }).then((data) => {
    if (data.error) {
      setAlreadyExists(true);
      setIsValid(true);
      return;
    }
    setWarehouses([...warehouses, data[0]]);
    setAlreadyExists(false);
    alert('You have created warehouse successfully!');
    setCityCreated(false);
  });
}

  return (
    <>
      <div className='warehouse-create-table'>
        <table className='table'>
          <tbody>
            <tr>
              <th colSpan='4' className='form-titles'>
                Create form
              </th>
            </tr>
            <tr className='create-form-details'>
              <th>Street</th>
              <th>Country</th>
              <th>City:</th>
              <th></th>
            </tr>

            <tr className='create-form-details'>
              <td>
                <input
                  size='40'
                  type='text'
                  name='street'
                  placeholder='Street'
                  onChange={validateStreet}
                />

                {!street.valid && street.isTouched ? (
                  <div>First name must be between 2 and 20 characters!</div>
                ) : (
                  <></>
                )}
              </td>
              <td>
                {' '}
                <select
                  onClick={(e) => {
                    setAlreadyExists(false);
                    setIsValid(false);
                    setCountry(e.target.value);
                  }}
                >
                  <option value='Bulgaria'>Bulgaria</option>
                  <option value='Spain'>Spain</option>
                </select>
              </td>
              <td>
                <select
                  onClick={(e) => {
                    setAlreadyExists(false);
                    setIsValid(false);
                    setCity(e.target.value);
                  }}
                >
                  {(country === 'Bulgaria' ? bgCities : espCities).map(
                    (city) => (
                      <option key={city} value={city}>
                        {city}
                      </option>
                    )
                  )}
                </select>
              </td>
              <td>
                {!cityCreated?
                <Button onClick={create} disabled={isValid}>
                  Create
                </Button>
                : <Button variant='success' onClick={confirmWarehouseCreate}>Confirm</Button>}
                {alreadyExists ? (
                  <div>Warehouse with the given adress already exists!</div>
                ) : (
                  <></>
                )}{' '}
              </td>
            </tr>
          </tbody>
        </table>

        <div>
          <table className='table'>
            <tbody>
              <tr>
                <th colSpan='4' className='form-titles'>
                  Update Warehouse
                </th>
              </tr>
              <tr>
                {warehouses.length !== 0 ? (
                  <th colSpan='4'>
                    <select
                      value={warehouseForUpdate.warehouse_id}
                      onChange={(e) => {
                        setWarehouseForUpdate(
                          warehouses.filter((w) => {
                            return w.warehouse_id === +e.target.value;
                          })[0]
                        );
                      }}
                    >
                      {warehouses
                        .filter((warehouse) => warehouse.isDeleted === 0)
                        .sort((a, b) => a.warehouse_id - b.warehouse_id)
                        .map((warehouse) => {
                          return (
                            <option
                              key={warehouse.warehouse_id}
                              value={warehouse.warehouse_id}
                            >
                              Warehouse ID: {warehouse.warehouse_id} / Street "
                              {warehouse.warehouse_street}" / City:{' '}
                              {warehouse.warehouse_city} / Country:{' '}
                              {warehouse.warehouse_country}
                            </option>
                          );
                        })}
                    </select>
                  </th>
                ) : (
                  <></>
                )}
              </tr>

              <tr>
                <th>Street </th>
                <th>Country</th>
                <th>City:</th>
                <th></th>
              </tr>
              {warehouseForUpdate ? (
                <WarehousesUpdate
                  warehouses={warehouses}
                  setWarehouses={setWarehouses}
                  warehouseForUpdate={warehouseForUpdate}
                ></WarehousesUpdate>
              ) : (
                <></>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

WarehousesCreate.propTypes = {
  warehouseForUpdate: PropTypes.string, 
  setWarehouseForUpdate: PropTypes.func
}

export default WarehousesCreate;
