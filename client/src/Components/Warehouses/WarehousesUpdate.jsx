import { Button } from 'react-bootstrap';
import React from 'react';
import { useState } from 'react';
import { bgCities } from '../../Common/bg-cities';
import { espCities } from '../../Common/esp-cities';
import { useEffect } from 'react';
import {
  deleteWarehouse,
  updateWarehouse,
} from '../../Requests/Warehouses-requests';
import { createCity } from '../../Requests/City';
import PropTypes from 'prop-types';

const WarehousesUpdate = ({
  warehouses,
  setWarehouses,
  warehouseForUpdate,
}) => {
  const [street, setStreet] = useState('Default');
  const [city, setCity] = useState('Default');
  const [country, setCountry] = useState('Default');
  const [showUpdate, setShowUpdate] = useState(true);
  
  useEffect(() => {
    setStreet(warehouseForUpdate.warehouse_street);
    setCity(warehouseForUpdate.warehouse_city);
    setCountry(warehouseForUpdate.warehouse_country);
    setShowUpdate(true);
  }, [warehouseForUpdate]);

  const deleteWrh = (warehouseId) => {
    deleteWarehouse(warehouseId).then((response) => {
      if (response.message) {
        setWarehouses([
          ...warehouses.map((warehouse) =>
            warehouse.warehouse_id === warehouseId
              ? { ...warehouse, isDeleted: 1 }
              : warehouse
          ),
        ]);
        setShowUpdate(false);
        setStreet('');
        alert('Warehouse was successfully deleted!');
      }
    });
  };

  const updateWrh = (warehouseId) => {
    createCity(city, country);
    updateWarehouse({ street, city, country }, warehouseId).then((data) => {
      alert('Warehouse was updated successfully!');
      setWarehouses([
        ...warehouses.map((warehouse) =>
          warehouse.warehouse_id === warehouseId
            ? {
                ...warehouse,
                warehouse_id: warehouseId,
                warehouse_street: street,
                warehouse_city: city,
                warehouse_country: country,
              }
            : warehouse
        ),
      ]);
    });
  };
  return (
    <>
      {showUpdate ? (
        <tr>
          <td>
            <input
              size='40'
              type='text'
              name='street'
              value={street}
              onChange={(e) => {
                setStreet(e.target.value);
              }}
            />
          </td>

          <td>
            <select
              value={country}
              onChange={(e) => {
                setCountry(e.target.value);
              }}
            >
              <option value='Bulgaria'>Bulgaria</option>
              <option value='Spain'>Spain</option>
            </select>
          </td>
          <td>
            <select
              value={city}
              onChange={(e) => {
                setCity(e.target.value);
              }}
            >
              {(country === 'Bulgaria' ? bgCities : espCities).map((city) => (
                <option key={city} value={city}>
                  {city}
                </option>
              ))}
            </select>
          </td>
          <td>
            <Button onClick={() => updateWrh(warehouseForUpdate.warehouse_id)}>
              Update
            </Button>
            <Button style={{marginLeft:'3px'}} onClick={() => deleteWrh(warehouseForUpdate.warehouse_id)}>
              Delete
            </Button>
          </td>
        </tr>
      ) : (
        <></>
      )}
    </>
  );
};

WarehousesUpdate.propTypes = {
  warehouses: PropTypes.array,
  setWarehouses: PropTypes.func,
  warehouseForUpdate: PropTypes.string
}

export default WarehousesUpdate;
