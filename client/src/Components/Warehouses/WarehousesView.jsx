import { Button } from 'react-bootstrap';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import {
  getWarehouses,
} from '../../Requests/Warehouses-requests';
import './Warehouses.css';
import PropTypes from 'prop-types';

const WarehousesView = ({ dispatchActionsState, setWarehouseForUpdate }) => {
  const [warehouses, setWarehouses] = useState([]);

  useEffect(() => {
    getWarehouses().then((data) => setWarehouses(data));
  }, []);

  return (
    <div className='warehouse-wrapper'  >
      <table>
          <tbody>
            <tr>
              <th>Warehouse ID:</th>
              <th>Street:</th>
              <th>City:</th>
              <th>Country:</th>
              <th></th>
            </tr>

            {warehouses
              .sort((a, b) => a.warehouse_id - b.warehouse_id)
              .map((warehouse) => (
                <tr key={warehouse.warehouse_id}>
                  <td>{warehouse.warehouse_id}</td>
                  <td>"{warehouse.warehouse_street}"</td>
                  <td>{warehouse.warehouse_city}</td>
                  <td>{warehouse.warehouse_country} </td>
                  <td>
                    <Button
                      onClick={() => {
                        setWarehouseForUpdate(warehouse);
                        dispatchActionsState({ type: 'warehouse-create-form' });
                      }}
                    >
                      Modify
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
      </table>
    </div>
  );
};

WarehousesView.propTypes = {
  dispatchActionsState:PropTypes.func,
  setWarehouseForUpdate: PropTypes.func, 
}

export default WarehousesView;
