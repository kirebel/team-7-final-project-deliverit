import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { getUsers } from '../../Requests/Users-requests';
import Login from '../Auth/Components/Login';
import Register from '../Auth/Components/Register';
import Maps from '../MapsView/Maps';
import Header from './Header';
import './Main.css';

const Main = () => {
  const [people, setPeople] = useState(0);

  useEffect(() => {
    getUsers('').then((data) => setPeople(data.length));
  }, []);

  return (
    <div className='main-wrapper'>
      <div className='header-main-wrapper'>
        <Header />
        <div className='main-buttons'>
          <div className='login-button'>
            {' '}
            <Login />
          </div>
          <div>
            <Register />
          </div>
        </div>
      </div>
      <div className='body-main-wrapper'>
        <div className='main-info'>
          <div>
            Number of people trusting in us: <b>{people}</b>
          </div>
          <br />
          <div>
            Number of countries we work with: <b>2</b>
          </div>
          <br />
          <div>
            <span className='about-us'>
              You grow your business <br />
              We'll take care of the logistics
            </span>
            <br />
            As the world of commerce is changing, it's time to change your
            business with it. Move your logistics service from offline to
            online. With DeliverIT, booking, tracking and managing your
            shipments is easy.
          </div>
        </div>
        <div className='main-google'>
          <Maps width='50' height='60' />
        </div>
      </div>
    </div>
  );
};

export default Main;
