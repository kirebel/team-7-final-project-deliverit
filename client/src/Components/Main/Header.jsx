import React from 'react';

const Header = () => {
  return (
    <div className='main-logo'>
      <div className='logo-delivery'>Deliver Like A Flash &#128498;</div>
      <div className='logo-text'>
        <div>Your Parcels</div>
        <div className='logo'></div>
        <div>Our Warehouses</div>
      </div>
    </div>
  );
};

export default Header;
