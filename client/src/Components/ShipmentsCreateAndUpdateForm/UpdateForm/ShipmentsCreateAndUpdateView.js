import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import {
  createShipment,
  deleteShipmentInTheDatabase,
  getShipments,
  updateShipmentInTheDatabase,
} from '../../../Requests/Shipments-requests';
import { getWarehouses } from '../../../Requests/Warehouses-requests';
import {
  dropParcelFromShipment,
  getParcels,
} from '../../../Requests/Parcels-requests';
import ShipmentUpdateForm from '../../ShipmentUpdate/ShipmentUpdate';
import '../ShipmentsCreateAndUpdateView.css';
import PropTypes from 'prop-types';

const ShipmentsCreateAndUpdateForm = ({
  shipmentForUpdate,
  setShipmentForUpdate,
  dispatchActionsState,
  dispatchSmallButtonsState,
}) => {
  const [warehouses, setWarehouses] = useState([]);
  const [shipments, setShipments] = useState([]);
  const [shipmentParcels, setShipmentParcels] = useState([]);

  const [successfulCreatedShipment, setSuccessfulCreatedShipment] =
    useState(false);

  const [createShipmentInfo, setCreateShipmentInfo] = useState({
    shipment_status: 1,
    origin_warehouse_id: '',
    destination_warehouse_id: '',
  });

  const [updateShipmentInfo, setUpdateShipmentInfo] = useState({
    departure_time: '',
    shipment_status: '',
    origin_warehouse_id: '',
    destination_warehouse_id: '',
    arrival_time: '',
  });

  useEffect(() => {
    getParcels('').then((data) => setShipmentParcels(data));
    getWarehouses().then((data) => setWarehouses(data));
    getShipments().then((data) => setShipments(data));
  }, []);

  const setShipmentUpdateInfo = (prop, value) => {
    updateShipmentInfo[prop] = value;
    setUpdateShipmentInfo({ ...updateShipmentInfo });
  };

  const updateCreateInfo = (prop, value) => {
    createShipmentInfo[prop] = value;
    setCreateShipmentInfo({ ...createShipmentInfo });
  };

  const deleteShipment = (shipmentId) => {
    deleteShipmentInTheDatabase(shipmentId).then((response) => {
      setShipments([
        ...shipments.map((element) =>
          element.shipment_id === shipmentId
            ? { ...element, isDeleted: 1 }
            : element
        ),
      ]);

      setShipmentParcels([
        ...shipmentParcels.map((element) =>
          element.shipment_id === shipmentId
            ? dropParcelFromShipment(
                {
                  ...element,
                  shipment_id: null,
                  warehouse_id: null,
                },
                element.id
              )
            : element
        ),
      ]);
    });
  };

  const updateShipment = (updateData, shipmentId) => {
    updateShipmentInTheDatabase(updateData, shipmentId).then((response) => {
      setShipments([
        ...shipments.map((shipment) =>
          shipment.shipment_id === shipmentId
            ? { ...shipment, ...updateData }
            : shipment
        ),
      ]);
    });
  };

  const createNewShipment = (shipmentParameters) => {
    createShipment(shipmentParameters).then((data) => {
      setShipments([...shipments, data[0]]);
    });
  };

  const handleFromWarehouseDropdown = (e) => {
    if (e.target.value !== 'default') {
      updateCreateInfo('origin_warehouse_id', +e.target.value);
    } else {
      updateCreateInfo('origin_warehouse_id', '');
    }
    setSuccessfulCreatedShipment(false);
  };

  const handleToWareHouseDropdown = (e) => {
    if (e.target.value !== 'default') {
      updateCreateInfo('destination_warehouse_id', +e.target.value);
    } else {
      updateCreateInfo('destination_warehouse_id', '');
    }
  };

  return (
    <div className='crete-and-update-form'>
      <div className='create-form'>
        <table>
          <tbody>
            <tr>
              <th colSpan='3' className='form-titles'>
                Create Shipment Form
              </th>
            </tr>
            <tr>
              <th>from warehouse</th>
              <th>to warehouse</th>
              <th></th>
            </tr>
            <tr>
              <td>
                <select
                  onClick={() => setSuccessfulCreatedShipment(false)}
                  onChange={(e) => handleFromWarehouseDropdown(e)}
                >
                  <option value='default'>Choose here</option>
                  {warehouses.map((element) => (
                    <option
                      key={element.warehouse_id}
                      value={element.warehouse_id}
                    >
                      #{element.warehouse_id}&nbsp;{element.warehouse_city},
                      {element.warehouse_country}
                    </option>
                  ))}
                </select>
              </td>
              <td>
                <select
                  onClick={() => setSuccessfulCreatedShipment(false)}
                  onChange={(e) => handleToWareHouseDropdown(e)}
                >
                  <option value='default'>Choose here</option>
                  {warehouses.map((element) => (
                    <option
                      key={element.warehouse_id}
                      value={element.warehouse_id}
                    >
                      #{element.warehouse_id},{element.warehouse_city},
                      {element.warehouse_country}
                    </option>
                  ))}
                </select>
              </td>
              <td>
                <Button
                  onClick={() => {
                    setSuccessfulCreatedShipment(true);
                    createNewShipment(createShipmentInfo);
                  }}
                  disabled={Object.values(createShipmentInfo).some((el) => !el)}
                >
                  Create
                </Button>
              </td>
            </tr>
            {successfulCreatedShipment ? (
              <tr>
                <th colSpan='3'>Shipment created successfully.</th>
              </tr>
            ) : null}
          </tbody>
        </table>
      </div>
      <hr className='hr'></hr>

      <div className='update-form-border'>
        <ShipmentUpdateForm
          dispatchSmallButtonsState={dispatchSmallButtonsState}
          shipments={shipments}
          setShipments={setShipments}
          shipmentForUpdate={shipmentForUpdate}
          shipmentParcels={shipmentParcels}
          setShipmentUpdateInfo={setShipmentUpdateInfo}
          updateShipment={updateShipment}
          updateShipmentInfo={updateShipmentInfo}
          deleteShipment={deleteShipment}
          setShipmentParcels={setShipmentParcels}
          dispatchActionsState={dispatchActionsState}
          setShipmentForUpdate={setShipmentForUpdate}
        />
      </div>
    </div>
  );
};

ShipmentsCreateAndUpdateForm.propTypes = {
  shipmentForUpdate: PropTypes.number,
  setShipmentForUpdate: PropTypes.func,
  dispatchActionsState: PropTypes.func,
};

export default ShipmentsCreateAndUpdateForm;
