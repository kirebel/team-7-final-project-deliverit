import { Button } from 'react-bootstrap';
import React  from 'react';
import Maps from './Maps';
import { useHistory } from 'react-router';

const MapsView = () => {
  const history = useHistory();

  return (
    <div>
      <div className='map-view-header'>
        <div className='button-map-view'>
          {' '}
          <Button
            className='button-locations'
            onClick={() => history.push('/customer-view')}
          >
            Back
          </Button>
        </div>
        <div className='text-map-view'>
          Location of our warehouses on the map
        </div>
      </div>
      <div className='map-view-page'>
        <Maps width='80' height='80' />
      </div>
    </div>
  );
};

export default MapsView;
