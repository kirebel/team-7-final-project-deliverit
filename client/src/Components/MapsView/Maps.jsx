import React from 'react';
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api';
import './MapsView.css';
import compass from './compass.png';
import { useEffect } from 'react';
import { getWarehouses } from '../../Requests/Warehouses-requests';
import { citiesCordinates } from './Cities-cordinates/cities-cordinates';
import { useRef } from 'react';
import { useState } from 'react';
import { useCallback } from 'react';
import PropTypes from 'prop-types'

const libraries = ['places'];

const center = {
  lat: 42.6977,
  lng: 23.3219,
};

const Locate = ({ panTo }) => {
  return (
    <button
      className='locate'
      onClick={() => {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            panTo({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            });
          },
          () => null
        );
      }}
    >
      <img src={compass} alt='compass' />
    </button>
  );
};

Locate.propTypes = {
  panTo: PropTypes.func,
}


const Maps = ({ width, height }) => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyB3EiR__vi5LvmXyTQfgT1IJj_gw1UFVMM',
    libraries,
  });

  const mapContainerStyle = {
    width: `${width}vw`,
    height: `${height}vh`,
  };

  const [markers, setMarkers] = useState([]);

  const mapRef = useRef();
  const onMapLoad = useCallback((map) => {
    mapRef.current = map;
  }, []);

  const panTo = useCallback(({ lat, lng }) => {
    mapRef.current.panTo({ lat, lng });
    mapRef.current.setZoom(12);
  }, []);

  useEffect(() => {
    getWarehouses().then((data) =>
      data.map((warehouse) => {
        citiesCordinates.find((el) => {
          if (warehouse.warehouse_city === el.city) {
            setMarkers((markers) => [...markers, { lat: el.lat, lng: el.lng }]);
          }
          return null;
        });
        return null;
      })
    );
  }, []);

  if (loadError) return 'Error';
  if (!isLoaded) return 'Loading...';

  return (
    <div className='map-wrapper'>
      <div className='locate-test'>
        {' '}
        <Locate panTo={panTo} />
      </div>
      <div className='map-outset'>
        {' '}
        <GoogleMap
          mapContainerStyle={mapContainerStyle}
          zoom={8}
          center={center}
          onLoad={onMapLoad}
        >
          {markers.map((marker) => (
            <Marker
              key={`${marker.lat}-${marker.lng}`}
              position={{ lat: +marker.lat, lng: +marker.lng }}
            />
          ))}
        </GoogleMap>
      </div>
    </div>
  );
};

Maps.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
}

export default Maps;
