import React from 'react';
import './Orders.css';
import PropTypes from 'prop-types';

const Orders = ({ orders }) => {
  return (
    <>
      <div className='orders-table'>
        <table>
          <tbody>
            <tr>
              <th colSpan='3' className='form-titles'>
                Orders
              </th>
            </tr>
            <tr>
              <th className='order-number'>order #</th>
              <th className='customer-number'>customer #</th>
              <th className='orders-description'>description</th>
            </tr>

            {orders
              .filter((element) => element.isDone === 0)
              .map((order) => (
                <tr key={order.id}>
                  <td>{order.id}</td>
                  <td>{order.user_id}</td>
                  <td>{order.description}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
      <hr className='hr'></hr>
    </>
  );
};

Orders.propTypes = {
  orders: PropTypes.array,
};

export default Orders;
