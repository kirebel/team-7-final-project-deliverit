import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for getting all orders
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API
 */

export const getOrders = () => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/orders`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for updating order status
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} orderId - the id of the order
 * @return {object} => a response from the API
 */

export const updateOrderStatus = (orderId) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/orders/${orderId}`, request).then((response) =>
    response.json()
  );
};
