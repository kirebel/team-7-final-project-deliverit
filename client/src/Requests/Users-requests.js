import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for getting users
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} searchTerms - the search terms of the search(query params)
 * @return {object} => a response from the API
 */

export const getUsers = (searchTerms) => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/users?${searchTerms}`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for getting an user by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId - the id of the user
 * @return {object} => a response from the API
 */

export const getUserById = (userId) => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/users/${userId}`, request).then((response) =>
    response.json()
  );
};
