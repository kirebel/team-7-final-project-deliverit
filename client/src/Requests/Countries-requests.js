import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for creating
 * a new city
 * @author Kiril Belchinski<kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API
 */

export const getCountries = () => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/countries`, request).then((response) =>
    response.json()
  );
};
