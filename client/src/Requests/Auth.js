import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:3001) for getting
 * login user
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} username - the username of the user
 * @param {string} password - the password of the user
 * @return {object} => a response from the API
 */

export const logInRequest = (username, password) =>
  fetch(`${MAIN_URL}/authentication/signin`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username,
      password,
    }),
  }).then((r) => r.json());

/**
 * Makes request to the API (localhost:5555) for register
 * additional info for user
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} registerDetails - the additional info of the user
 * @return {object} => a response from the API
 */

export const registerRequest = (registerDetails) =>
  fetch(`${MAIN_URL}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(registerDetails),
  }).then((r) => r.json());

/**
 * Makes request to the API (localhost:5555) for updating
 * user information
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} userDetails - the details of the user
 * @param {number} userId - the id of the user
 * @return {object} => a response from the API
 */

export const updateUserInfo = (userDetails, userId) => {
  return fetch(`${MAIN_URL}/users/${userId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(userDetails),
  }).then((r) => r.json());
};

/**
 * Makes request to the API (localhost:5555) for checking
 * if username exist
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} username - the username that is checked
 * @return {object} => a response from the API
 */

export const checkUsername = async ({ username }) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ username }),
  };

  return await fetch(`${MAIN_URL}/users/username`, request).then((response) =>
    response.json()
  );
};
