import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for getting all warehouses
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {object} => a response from the API
 */

export const getWarehouses = () => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/warehouses`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for creating a warehouse
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} data - the information about the warehouse
 * @return {object} => a response from the API
 */

export const createWarehouse = (data) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(`${MAIN_URL}/warehouses`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for deleting a warehouse by id
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} warehouseId - the id of the warehouse
 * @return {object} => a response from the API
 */

export const deleteWarehouse = (warehouseId) =>
  fetch(`${MAIN_URL}/warehouses/${warehouseId}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  }).then((r) => r.json());

/**
 * Makes request to the API (localhost:5555) for updating a warehouse by id
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} data - the information about the warehouse
 * @param {number} warehouseId - the id of the warehouse
 * @return {object} => a response from the API
 */

export const updateWarehouse = (data, warehouseId) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(`${MAIN_URL}/warehouses/${warehouseId}`, request).then(
    (response) => response.json()
  );
};
