import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for getting shipments
 * @author Ivaylo Garnev <ivailo.garnev.a29@learn.telerikacademy.com>
 * @param {searchTerms} searchTerms - the search terms (query params)
 * @return {object} => a response from the API
 */

export const getShipments = (searchTerms) => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/shipments?${searchTerms}`, request).then(
    (response) => response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for creating  a shipment
 * @author Ivaylo Garnev <ivailo.garnev.a29@learn.telerikacademy.com>
 * @param {object} data - the data about the shipment
 * @return {object} => a response from the API
 */

export const createShipment = (data) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };
  return fetch(`${MAIN_URL}/shipments`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for updating a shipment
 * @author Ivaylo Garnev <ivailo.garnev.a29@learn.telerikacademy.com>
 * @param {object} data - the information about the shipment
 * @param {number} shipmentId - the id of the shipment
 * @return {object} => a response from the API
 */

export const updateShipmentInTheDatabase = (data, shipmentId) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(`${MAIN_URL}/shipments/${shipmentId}`, request).then(
    (response) => response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for deleting a shipment
 * @author Ivaylo Garnev <ivailo.garnev.a29@learn.telerikacademy.com>
 * @param {number} shipmentId - the id of the shipment
 * @return {object} => a response from the API
 */

export const deleteShipmentInTheDatabase = (shipmentId) => {
  const request = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/shipments/${shipmentId}`, request).then(
    (response) => response.json()
  );
};
