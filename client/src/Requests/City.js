import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for creating
 * a new city
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} city - the city name
 * @param {string} country - the country name
 * @return {object} => a response from the API
 */

export const createCity = (city, country) => {
  fetch(`${MAIN_URL}/cities`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },

    body: JSON.stringify({
      city,
      country,
    }),
  }).then((r) => r.json());
};
