import MAIN_URL from '../Common/MainUrl';
import { getToken } from '../Common/token';

/**
 * Makes request to the API (localhost:5555) for getting all parcels
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} searchTerms - the search terms(query params)
 * @return {object} => a response from the API
 */

export const getParcels = (searchTerms) => {
  const request = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };
  return fetch(`${MAIN_URL}/parcels?${searchTerms}`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for creating a parcel
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} data - the information about the parcel
 * @return {object} => a response from the API
 */

export const createParcel = (data) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(`${MAIN_URL}/parcels`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for updating a parcel(attaching it to shipment)
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} data - the information about the parcel
 * @param {number} parcelId - the id of the parcel
 * @return {object} => a response from the API
 */

export const addParcelToShipment = (data, parcelId) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(`${MAIN_URL}/parcels/${parcelId}`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for deleting a parcel
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} parcelId - the id of the parcel
 * @return {object} => a response from the API
 */

export const deleteParcel = (parcelId) => {
  const request = {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
  };

  return fetch(`${MAIN_URL}/parcels/${parcelId}`, request).then((response) =>
    response.json()
  );
};

/**
 * Makes request to the API (localhost:5555) for updating a parcel(drop parcel from shipment)
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} data - the information about the parcel
 * @param {number} parcelId - the id of the parcel
 * @return {object} => a response from the API
 */

export const dropParcelFromShipment = (data, parcelId) => {
  const request = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${getToken()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(`${MAIN_URL}/parcels/${parcelId}`, request).then((response) =>
    response.json()
  );
};
