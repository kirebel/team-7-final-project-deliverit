import serviceErrors from '../common/service-errors.js';
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE } from '../common/constants.js';

/**
 * Active sql request function from usersData module for
 * searching user about specific criteria
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} usersData- the module that includes all sql requests
 * @return {closure} - function that accepts an object of query parameters
 * and their values then getUserTableColumns function is called and
 * if there is no error in the next few lines(generating Sql script for searching)
 * searchBy function is called(results from this function are returned).
 * If there is an error - error is returned.
 */

const getUsers = (usersData) => {
  return async (queryObject) => {
    const userTableColumns = await usersData.getUserTableColumns();

    let searchSql = '';

    for (const [key, value] of Object.entries(queryObject)) {
      if (!userTableColumns.includes(key)) {
        return {
          error: serviceErrors.BAD_REQUEST,
          users: null,
        };
      }
      if ((key === 'country_id' || key === 'city_id') && isNaN(+value)) {
        return {
          error: serviceErrors.BAD_REQUEST,
          users: null,
        };
      }
      searchSql += `AND u.${key} LIKE '%${value}%'`;
    }

    const result = await usersData.searchBy(searchSql);

    return {
      error: null,
      users: result,
    };
  };
};

/**
 * Active sql request function from usersData module for
 * signup a user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} usersData- the module that includes all sql requests
 * @return {closure} - function that calls the getUserInfoWithPassword
 * from the usersData module for checking if the user exist and
 * if the database includes users credentials
 */

const signInUser = (usersData) => {
  return async (username, password) => {
    const user = await usersData.getUserInfoWithPassword(username);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_SIGHIN,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

const checkIfUsernameExists = (usersData) => {
  return async (username) => {
    return await usersData.checkForDuplicateUsername('username', username);
  };
};
/**
 * Active sql request function from usersData module for
 * creating a new user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} usersData- the module that includes all sql requests
 * @return {closure} - function that calls checkForDuplicateUsername
 * function for checking if the username exist and then
 * if is not, create function is called to create
 * the new user
 */

const createUser = (usersData) => {
  return async (userInfo) => {
    const {
      username,
      password,
      first_name,
      last_name,
      email,
      street,
      country,
      city,
    } = userInfo;

    const existingUser = await usersData.checkForDuplicateUsername(
      'username',
      username
    );

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.create(
      username,
      passwordHash,
      DEFAULT_USER_ROLE,
      first_name,
      last_name,
      email,
      street,
      country,
      city
    );

    return {
      error: null,
      user: user,
    };
  };
};

/**
 * Active sql request function from usersData module for
 * getting user information searching the user by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} usersData- the module that includes all sql requests
 * @return {closure} - function that calls the getBy function from
 * the usersData module. If the users exist his/her info is
 * returned, if is not - error is returned
 */

const getUserById = (usersData) => {
  return async (userId) => {
    const user = await usersData.getBy('id', userId);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

/**
 * Active sql request function from usersData module for
 * updating user's information
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} usersData- the module that includes all sql requests
 * @return {closure} - function that calls the getBy function from
 * the usersData module.If the users exist,update
 * function is called for updating the information
 */

const updateUser = (usersData) => {
  return async (pathId, updateData, userId) => {
    const user = await usersData.getBy('id', pathId);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    if (pathId !== userId) {
      return {
        error: serviceErrors.UNALLOWED_ACTION,
        user: null,
      };
    }

    // if (
    //   updateData.username &&
    //   !!(await usersData.getBy('username', updateData.username))
    // ) {
    //   return {
    //     error: serviceErrors.DUPLICATE_RECORD,
    //     user: null,
    //   };
    // }

    const updatedUser = { ...user, ...updateData };

    const updatingUser = await usersData.update(updatedUser, +pathId);

    return {
      error: null,
      user: { ...updatedUser, token: updatingUser.token, id: +userId },
    };
  };
};

/**
 * Active sql request function from usersData module for
 * deleting an user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} usersData- the module that includes all sql requests
 * @return {closure} - function that calls the getBy function from
 * the usersData module.If the users exist, removeUser function
 * is called for delete the user
 */

const deleteUser = (usersData) => {
  return async (id) => {
    const userToDelete = await usersData.getBy('id', id);

    if (!userToDelete) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    const _ = await usersData.removeUser(userToDelete.id);

    return { error: null, user: userToDelete };
  };
};

export default {
  createUser,
  getUserById,
  signInUser,
  updateUser,
  deleteUser,
  getUsers,
  checkIfUsernameExists,
};
