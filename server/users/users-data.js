import createToken from '../authentication/create-token.js';
import { IS_DELETED_FALSE, IS_DELETED_TRUE } from '../common/constants.js';
import pool from '../pool.js';

/**
 * Makes sql request to the database for
 * searching users about specific criteria dynamically
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} searchSql- the Sql script generated
 * dynamically(the searching criteria)
 * @return {array} =>an array that includes all
 * users that meets the searching criteria
 */

const searchBy = async (searchSql) => {
  const sql = `
  SELECT u.id, u.username, u.role, u.first_name,u.last_name,u.email,u.street,
  (SELECT name FROM countries WHERE id = u.country_id) as country,
  (SELECT name FROM cities WHERE id = u.city_id) as city
  FROM user as u
  WHERE isDeleted = ?
      ${searchSql}
  `;

  return await pool.query(sql, [IS_DELETED_FALSE]);
};

/**
 * Makes sql request to the database for creating
 * a new user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} username - the username of the new user
 * @param {string} password - the encrypted password of the new user
 * @param {string} role - the role of the new user('customer' by default)
 * @param {string} first_name - the first name of the user
 * @param {string} last_name - the last name of the user
 * @param {string} email - the email of the user
 * @param {string} street - the street of the user
 * @param {string} country - the country of the user
 * @param {string} city - the city of the user
 * @return {object} => an object with the information
 * of the created user
 */

const create = async (
  username,
  password,
  role,
  first_name,
  last_name,
  email,
  street,
  country,
  city
) => {
  const sql = `
    INSERT INTO 
    user (username,password,role,first_name,last_name,email,street,country_id,city_id)
    VALUES (?,?,?,?,?,?,?,(SELECT id FROM countries where name = ?),(SELECT id from cities where name = ?))`;

  const result = await pool.query(sql, [
    username,
    password,
    role,
    first_name,
    last_name,
    email,
    street,
    country,
    city,
  ]);

  return {
    id: result.insertId,
    username: username,
    role: role,
    first_name: first_name,
    last_name: last_name,
    email: email,
    street: street,
    country: country,
    city: city,
  };
};

/**
 * Makes sql request to the database for checking
 * if a username already exist
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} column - the name of the column
 * in the selected table
 * @param {string} value - the username which
 * the new user tries to register with
 * @return {object} => an object with the
 * username if exist
 */

const checkForDuplicateUsername = async (column, value) => {
  const sql = `
    SELECT username
    FROM user
    WHERE ${column}=?`;

  const result = await pool.query(sql, [value]);

  return result[0];
};

/**
 * Makes sql request to the database for getting
 * an information of an user by username(returns password too, needed for signin)
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} username - the username of the user which information is needed
 * @return {object} => an object with the information
 * about the user
 */

const getUserInfoWithPassword = async (username) => {
  const sql = `
  SELECT u.id, u.username, u.password, u.role, u.first_name,
  u.last_name, u.email,u.street, u.country_id, u.city_id,u.isDeleted,
  ci.name as city_name,co.name as country_name
  FROM user as u
  JOIN cities as ci on u.city_id = ci.id
  JOIN countries as co on co.id = u.country_id
  WHERE u.username  = ?`;

  const result = await pool.query(sql, [username]);

  return result[0];
};

/**
 * Makes sql request to the database for getting
 * an information of an user by specific parameter(id or username)
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} column - the name of the column
 * in the selected table
 * @param {string(username) or number(id)} value - the username
 * or the id of the user which information is needed
 * @return {object} => an object with the information
 * about the user
 */

const getBy = async (column, value) => {
  const sql = `
  SELECT u.id,u.username,u.role,u.first_name,u.last_name,u.street,u.email,
  (SELECT name FROM countries where id = u.country_id) as country,
  (SELECT name from cities where id = u.city_id) as city
  FROM user as u
  WHERE u.${column} =?
  AND u.isDeleted = ?`;

  const result = await pool.query(sql, [value, IS_DELETED_FALSE]);

  return result[0];
};

/**
 * Makes sql request to the database for
 * deleting an user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} userId- the id of the user
 * which has to be deleted
 * @return {object} => an object with the information
 * about request made(affected rows, insert id ect.)
 */

const removeUser = async (userId) => {
  const sql = `
  UPDATE user
  SET isDeleted = ?
  WHERE id = ?`;

  const result = await pool.query(sql, [IS_DELETED_TRUE, userId]);

  return result;
};

/**
 * Makes sql request to the database for
 * updating user's information
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} updateData- the update data
 * @param {number} userId- the id of the user
 * @return {object} => an object with the updated information
 */

const update = async (updateData, userId) => {
  const { username, first_name, last_name, country, city, email, street } =
    updateData;

  const sql = `
    UPDATE user
    SET username= ?,
    first_name = ?,
    last_name =?,
    country_id =  (SELECT id FROM countries where name = ?),
    city_id = (SELECT id from cities where name = ?),
    email = ?,
    street = ?
    WHERE id = ?
    AND isDeleted = ?`;

  const result = await pool.query(sql, [
    username,
    first_name,
    last_name,
    country,
    city,
    email,
    street,
    userId,
    IS_DELETED_FALSE,
  ]);

  const info = {
    username,
    first_name,
    last_name,
    country,
    city,
    email,
    street,
    userId,
    IS_DELETED_FALSE,
    id: userId,
    role: 'customer',
  };

  const newToken = createToken(info);

  const final_result = {
    ...info,
    token: newToken,
  };

  return final_result;
};

/**
 * Makes sql request to the database for
 * getting all column names of user table
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {array} => an array with all the names
 * of the user table
 */

const getUserTableColumns = async () => {
  const sql = `SELECT*
  FROM user 
 `;

  const result = await pool.query(sql);
  return Object.keys(result[0]);
};

export default {
  create,
  checkForDuplicateUsername,
  getBy,
  getUserInfoWithPassword,
  removeUser,
  update,
  searchBy,
  getUserTableColumns,
};
