import express from 'express';

import usersService from './users-service.js';
import usersData from './users-data.js';
import serviceErrors from '../common/service-errors.js';

import { createValidator } from '../middleware/create.validator.js';
import { createUserSchema } from '../validators/create-user.schema.js';
import { authMiddleware } from '../authentication/auth-middleware.js';
import { updateUserInfoSchema } from '../validators/update-user.schema.js';

const usersController = express.Router();

usersController

  .get('/', async (req, res) => {
    const queryObject = req.query;

    const { error, users } = await usersService.getUsers(usersData)(
      queryObject
    );

    if (error === serviceErrors.BAD_REQUEST) {
      res.status(400).json({
        error: 'Bad request.',
        detail: {
          query_parameters:
            'Wrong query parameters. Check server documentation for more information',
        },
      });
    } else {
      res.status(200).json(users);
    }
  })

  .post('/username', async (req, res) => {
    const { username } = req.body;

    const checkUsername = await usersService.checkIfUsernameExists(usersData)(
      username
    );

    if (checkUsername) {
      return res.status(200).json({ exist: 'Username exists!' });
    }
    return res.status(200).json({ not_exists: 'Username does not exists!' });
  })

  .post('/', createValidator(createUserSchema), async (req, res) => {
    const createData = req.body;

    const { error, user } = await usersService.createUser(usersData)(
      createData
    );

    if (error === serviceErrors.DUPLICATE_RECORD) {
      res.status(409).json({
        error: 'Conflict',
        detail: {
          username: 'Already existing username',
        },
      });
    } else {
      res.status(201).json(user);
    }
  })

  .get('/:id', async (req, res) => {
    const userId = +req.params.id;

    if (isNaN(userId)) {
      res.status(400).json({
        error: 'Bad request.',
        detail: {
          resource: 'No resource found.',
        },
      });
    }

    const { error, user } = await usersService.getUserById(usersData)(userId);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).json({
        error: 'User not found.',
        detail: {
          id: 'User with this id does not exist!',
        },
      });
    }
    res.status(200).json(user);
  })

  .put(
    '/:id',
    createValidator(updateUserInfoSchema),
    authMiddleware,
    async (req, res) => {
      const pathId = +req.params.id;
      const userId = +req.user.id;

      const updateInfo = req.body;

      const { error, user } = await usersService.updateUser(usersData)(
        pathId,
        updateInfo,
        userId
      );

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).json({
          error: 'User not found.',
          detail: {
            id: 'User with this id does not exist!',
          },
        });
      } else if (error === serviceErrors.UNALLOWED_ACTION) {
        res.status(405).json({
          error: 'Unallowed action',
          detail: {
            userId: 'User can edit only his own information',
          },
        });
      } else if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).json({
          error: 'Conflict',
          detail: {
            username: 'Already existing username',
          },
        });
      } else {
        res.status(200).json(user);
      }
    }
  )

  .delete('/:id', authMiddleware, async (req, res) => {
    const userId = +req.params.id;

    const { error, user } = await usersService.deleteUser(usersData)(userId);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).json({
        error: 'User not found.',
        detail: {
          id: 'User with this id does not exist!',
        },
      });
    } else {
      res.status(200).json(user);
    }
  });

export default usersController;
