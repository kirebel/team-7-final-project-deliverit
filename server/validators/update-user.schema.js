export const updateUserInfoSchema = {
  username: (value) => {
    if (!value) {
      return { error: 'Username is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return { error: 'Username should be a string in range [3..25]' };
    }

    return null;
  },

  country: (value) => {
    if (!value) {
      return { error: 'Country is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return { error: 'Country should be a string in range [3..25]' };
    }

    return null;
  },
  city: (value) => {
    if (!value) {
      return { error: 'City is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return { error: 'City should be a string in range [3..25]' };
    }

    return null;
  },
  street: (value) => {
    if (!value) {
      return { error: 'Street is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 50) {
      return { error: 'Street should be a string in range [3..50]' };
    }

    return null;
  },
  first_name: (value) => {
    if (!value) {
      return { error: 'First name is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return { error: 'First name should be a string in range [3..25]' };
    }

    return null;
  },
  last_name: (value) => {
    if (!value) {
      return { error: 'Last name is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 25) {
      return { error: 'Last name should be a string in range [3..25]' };
    }

    return null;
  },
  email: (value) => {
    if (!value) {
      return { error: 'Email is required' };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 50) {
      return { error: 'Email should be a string in range [3..50]' };
    }

    return null;
  },
};
