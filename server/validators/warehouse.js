export const warehouseSchema = {
  street: (value) => {
    if (!value) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Street name is required!',
        },
      };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 60) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Street name should be a string in range [3..60]!',
        },
      };
    }

    return null;
  },
  country: (value) => {
    if (!value) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Country name is required!',
        },
      };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 60) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Country name should be a string in range [3..60]!',
        },
      };
    }

    return null;
  },
  city: (value) => {
    if (!value) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'City name is required!',
        },
      };
    }

    if (typeof value !== 'string' || value.length < 3 || value.length > 60) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'City name should be a string in range [3..60]!',
        },
      };
    }

    return null;
  },
};
