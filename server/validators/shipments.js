import {
  STATUS_COMPLETED,
  STATUS_ON_THE_WAY,
  STATUS_PREPARING,
} from '../common/constants.js';
import { dateFormatCheck } from '../common/date-format-checker.js';

export const shipmentSchema = {
  departure_time: () => {
    return null;
  },
  arrival_time: () => {
    return null;
  },
  shipment_status: (value) => {
    if (!value) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Shipments status is required!',
        },
      };
    }

    if (
      ![STATUS_PREPARING, STATUS_COMPLETED, STATUS_ON_THE_WAY].includes(value)
    ) {
      return {
        error: 'Invalid payload.',
        detail: {
          street:
            'Shipment status must be a number! (preparing - 1, on the way - 2, completed - 3)',
        },
      };
    }

    return null;
  },
  origin_warehouse_id: (value) => {
    if (!value) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Origin warehouse is required!',
        },
      };
    }

    if (typeof value !== 'number') {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Origin warehouse id must be a number!',
        },
      };
    }

    return null;
  },
  destination_warehouse_id: (value) => {
    if (!value) {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Destination warehouse id is required!',
        },
      };
    }

    if (typeof value !== 'number') {
      return {
        error: 'Invalid payload.',
        detail: {
          street: 'Destination warehouse id must be a number!',
        },
      };
    }

    return null;
  },
};
