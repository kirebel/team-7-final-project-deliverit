import { HOME_DELIVERY, NOT_HOME_DELIVERY } from '../common/constants.js';

export const createParcelSchema = {
  weight: (value) => {
    if (!value) {
      return { error: 'Weight is required' };
    }

    if (typeof value !== 'number') {
      return { error: 'Weight should be a number' };
    }

    return null;
  },

  home_delivery: (value) => {
    if (!value) {
      return { error: 'Home delivery is check  required.' };
    }

    if (![NOT_HOME_DELIVERY, HOME_DELIVERY].includes(value)) {
      return {
        error:
          'Home delivery should be a number (1 - home delivery,  2 - not home delivery).',
      };
    }

    return null;
  },
  user_id: (value) => {
    if (!value) {
      return { error: 'User id is required.' };
    }

    if (typeof value !== 'number' || value < 1) {
      return { error: 'User id should be a number greater than 0.' };
    }

    return null;
  },
  shipment_id: (value) => {
    if (typeof value !== 'number' && value !== null) {
      return { error: 'Shipment id is required.' };
    }

    if (value !== null && typeof value !== 'number') {
      return { error: 'Shipment id must be a number greater than 0.' };
    }

    return null;
  },
  warehouse_id: (value) => {
    if (typeof value !== 'number' && value !== null) {
      return { error: 'Warehouse id is required.' };
    }

    if (value !== null && typeof value !== 'number') {
      return { error: 'Warehouse id should be a number in range [3..7]' };
    }

    return null;
  },
  category_id: (value) => {
    if (!value) {
      return { error: 'Category id is required.' };
    }

    if (typeof value !== 'number' || value < 1 || value > 6) {
      return { error: 'Category id should be a number in range [1..6]' };
    }

    return null;
  },
};
