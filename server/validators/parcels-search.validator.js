export const parcelsQueryParamsValidator = (queryObject) => {
  // eslint-disable-next-line complexity
  return Object.entries(queryObject).reduce((acc, el) => {
    let isValid = true;

    if (el[0] === 'user_id' && isNaN(+el[1])) {
      isValid = false;
    }
    if (el[0] === 'category_id' && isNaN(+el[1]) && !el[1]) {
      isValid = false;
    }
    if (el[0] === 'warehouse_id' && isNaN(+el[1]) && !el[1]) {
      isValid = false;
    }
    if (el[0] === 'sort') {
      const [sortParam1, sortParam2] = el[1].split(',');
      if (
        sortParam1 &&
        sortParam1 !== 'arrival_time:desc' &&
        sortParam1 !== 'arrival_time:asc' &&
        sortParam1 !== 'weight:asc' &&
        sortParam1 !== 'weight:desc'
      ) {
        isValid = false;
      } else if (
        sortParam2 &&
        sortParam2 !== 'arrival_time:desc' &&
        sortParam2 !== 'arrival_time:asc' &&
        sortParam2 !== 'weight:asc' &&
        sortParam2 !== 'weight:desc'
      ) {
        isValid = false;
      }
    }
    return acc && isValid;
  }, true);
};
