export const cityValueToId = (city) =>
  `(SELECT cities.id FROM cities WHERE cities.name = '${city}')`;

export const countryValueToId = (country) =>
  `(SELECT c.id FROM countries as c WHERE c.name = '${country}')`;

export const getWarehouseFullInfo = `
SELECT w.id as warehouse_id, w.street as warehouse_street,
cities.name as warehouse_city, countries.name as warehouse_country, w.isDeleted as isDeleted

FROM warehouses as w
JOIN countries ON w.country_id = countries.id 
JOIN cities ON  w.city_id = cities.id`;
