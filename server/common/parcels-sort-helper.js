export const parcelsSortDataProcessing = (data) => {
  if (data) {
    return data.split('').reduce((acc, el) => {
      return el === ':' ? (acc += ' ') : (acc += el);
    }, ' ORDER BY ');
  }
  return '';
};
