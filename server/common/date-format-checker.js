export const dateFormatCheck = (date) => {
  if (!date) {
    return null;
  }
  const splitDate = date.split('-');
  if (splitDate[0].length !== 4) {
    return true;
  }
  return false;
};
