CREATE DATABASE  IF NOT EXISTS `deliverit_database` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `deliverit_database`;
-- MariaDB dump 10.19  Distrib 10.5.10-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: deliverit_database
-- ------------------------------------------------------
-- Server version	10.5.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Electronics'),(2,'Medical'),(3,'Cosmetics'),(4,'Sport'),(5,'Clothing'),(6,'Other');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Cities_Countries1_idx` (`country_id`),
  CONSTRAINT `fk_Cities_Countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Sofia',1),(12,'Sapareva Banya',1),(13,'Ahtopol',1),(14,'Burgas',1),(15,'Pleven',1),(16,'Ruse',1),(17,'Vratsa',1),(18,'Plovdiv',1),(19,'Barcelona',3),(20,'Madrid',3),(21,'Sevilla',3),(22,'Valencia',3),(23,'Bilbao',3),(24,'Veliko Tarnovo',1),(25,'Silistra',1),(26,'Apriltsi',1),(27,'Balchik',1);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Bulgaria'),(3,'Spain');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(465) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isDone` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_orders_user1_idx` (`user_id`),
  CONSTRAINT `fk_orders_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2260 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (2256,'Laptop, weight:3kg,delivery:address',6,0),(2257,'Football ball, goalkeeper gloves, weight:1.2kg,delivery:warehouse',7,0),(2258,'New Avon collection, weight:3kg, delivery:warehouse',103,0),(2259,'Fishing lures,weight:1.9kg,delivery:home',99,0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcels`
--

DROP TABLE IF EXISTS `parcels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parcels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` float NOT NULL,
  `home_delivery` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_Parcels_User1_idx` (`user_id`),
  KEY `fk_Parcels_Shipments1_idx` (`shipment_id`),
  KEY `fk_Parcels_Warehouses1_idx` (`warehouse_id`),
  KEY `fk_Parcels_Categories1_idx` (`category_id`),
  CONSTRAINT `fk_Parcels_Categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Parcels_Shipments1` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Parcels_User1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Parcels_Warehouses1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32735 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcels`
--

LOCK TABLES `parcels` WRITE;
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
INSERT INTO `parcels` VALUES (32707,1.2,2,97,4474,26,1,0),(32708,2,1,97,4474,26,1,0),(32709,5,2,97,4474,26,2,0),(32710,4,1,97,4475,19,6,0),(32711,20,1,97,4475,19,5,0),(32712,1,2,97,4475,19,1,0),(32713,5,1,97,4475,19,5,0),(32714,2,1,96,4475,19,4,0),(32715,5,1,96,4475,19,3,0),(32716,2,2,97,4475,19,2,0),(32717,2,1,95,4475,19,4,0),(32718,5,1,97,4475,19,3,0),(32719,7,1,95,NULL,NULL,2,1),(32720,6,2,7,4475,19,4,0),(32721,6,2,6,4476,21,2,0),(32722,6,1,97,4476,21,2,0),(32723,1,1,98,4477,25,4,0),(32724,8,1,98,4486,11,3,0),(32725,6,1,99,4477,25,3,0),(32726,2,1,99,4477,25,6,0),(32727,6,2,100,4477,25,1,0),(32728,1,1,97,4477,25,2,0),(32729,6,2,101,NULL,NULL,3,0),(32730,2,1,102,NULL,NULL,5,0),(32731,22,1,101,4477,25,1,0),(32732,5,2,102,NULL,NULL,6,0),(32733,55,1,103,NULL,NULL,3,0),(32734,3,2,6,NULL,NULL,1,0);
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipments`
--

DROP TABLE IF EXISTS `shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arrival_time` timestamp NULL DEFAULT NULL,
  `departure_time` datetime DEFAULT NULL,
  `shipment_status` int(11) NOT NULL,
  `origin_warehouse_id` int(11) NOT NULL,
  `destination_warehouse_id` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_Shipments_ShipmentStatus1_idx` (`shipment_status`),
  KEY `fk_Shipments_Warehouses1_idx` (`origin_warehouse_id`),
  KEY `fk_Shipments_Warehouses2_idx` (`destination_warehouse_id`),
  CONSTRAINT `fk_Shipments_ShipmentStatus1` FOREIGN KEY (`shipment_status`) REFERENCES `shipmentstatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Shipments_Warehouses1` FOREIGN KEY (`origin_warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Shipments_Warehouses2` FOREIGN KEY (`destination_warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4492 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipments`
--

LOCK TABLES `shipments` WRITE;
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` VALUES (4472,NULL,NULL,1,5,16,1),(4473,NULL,NULL,1,5,11,1),(4474,NULL,'2021-08-31 00:00:00',2,5,26,0),(4475,'2021-08-29 21:00:00','2021-08-31 00:00:00',3,5,19,0),(4476,NULL,NULL,1,5,21,0),(4477,NULL,NULL,1,5,25,0),(4478,NULL,NULL,1,5,24,0),(4479,NULL,NULL,1,13,24,0),(4480,NULL,NULL,1,15,24,0),(4481,NULL,NULL,1,16,10,0),(4482,NULL,NULL,1,5,15,0),(4483,NULL,NULL,1,5,24,0),(4484,NULL,NULL,1,5,19,0),(4485,NULL,NULL,1,5,16,0),(4486,NULL,NULL,1,5,11,0),(4487,NULL,NULL,1,5,14,0),(4488,NULL,NULL,1,5,27,0),(4489,NULL,NULL,1,5,16,0),(4490,NULL,NULL,1,5,10,0),(4491,NULL,NULL,1,5,10,0);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipmentstatus`
--

DROP TABLE IF EXISTS `shipmentstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipmentstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipmentstatus`
--

LOCK TABLES `shipmentstatus` WRITE;
/*!40000 ALTER TABLE `shipmentstatus` DISABLE KEYS */;
INSERT INTO `shipmentstatus` VALUES (1,'preparing'),(2,'on the way'),(3,'completed');
/*!40000 ALTER TABLE `shipmentstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(70) NOT NULL,
  `street` varchar(60) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_User_Countries1_idx` (`country_id`),
  KEY `fk_User_Cities1_idx` (`city_id`),
  CONSTRAINT `fk_User_Cities1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_Countries1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (6,'destroyer','$2b$10$HwyVs.OHngTt3O1GZowOYeQfGm4jd/YGr/NTdC39OnXzBM6efnl4O','customer','Kiril','Belchinski','kikobel_82@abv.bg','Mariq Luziza',1,1,0),(7,'asencho','$2b$10$eYazGXEpacsakwzBj3iVzes9suyySOxP3f1KJAIMJ6XprCgXB2XK.','employee','Kiril','Belchinski','dfsfdsdf','Mariq Luziza',1,1,0),(95,'support','$2b$10$zYCOTbolhb7NhLB6C2d8NuaBcl1fudjlnXZrSIu.IyQ6FRnio3MVq','support','Kiril','Belchinski','email@abv.bg','Germanea',1,12,0),(96,'employee1','$2b$10$MOPZ/w9OThWh.gukPQ57VOJzXz4oLsTEWk758emwGMuR..ttLioHa','employee','Ivan','Atanasov','VanioA@gmail.com','Panichishte',1,1,0),(97,'customer1','$2b$10$xI9kG2F68XWjQi8DUmTVoe/E74L6j95V6n1YwqAezNil15jurD.hm','customer','Customer','Customer','customer@abv.bg','customer',1,13,0),(98,'ivan23','$2b$10$78atV03Juf5Xf/G8QThYCOjv2HsbyubZ2KnhAQPYjLzloeuz5/wPK','customer','Ivan','Ivanov','ivanov@abv.bg','Крайречна',1,26,0),(99,'gameChanger','$2b$10$wgMDwsH.Y7Fn3nuoGjWlt.IMHoe0m5GoIxk9g.L6asyrOeaqHB07m','customer','Asen','Asenov','asenov@abv.bg','Крайречна',1,27,0),(100,'morata22','$2b$10$ebmQDS99KjQwbsvKbaI37e7PdSgfgQyaucrpAmUkdd.SeKmuJFc4C','customer','Alvaro','Morata','morata@gmail.com','Villarias Kalea',3,23,0),(101,'fernando11','$2b$10$7rle3vSoomq/V3MEWpi2Ue5wwfhZzpR5iVxkiqGi773hWXQHAvyLe','customer','Fernando','Torres','torres@gmail.com','Nafarroa Kalea',3,19,0),(102,'sergio22','$2b$10$cSl1B6ZWbUY75WpzH510AeeiVlO33z04/4yNnTX82/Ng5C4RHXuwi','customer','Sergio','Rammos','rammos@abv.bg','Bailen Kalea',3,20,0),(103,'penelope2','$2b$10$zoqGAkZMljQnZdQHsKB71OHcIg7KIK5SgHHFPhIOEbuohE0aD58Ei','customer','Penelope','Cruz','penelope@gmail.com','Areatzako Zubia',3,20,0),(104,'kirebel','$2b$10$hzd50/HBiyjRM4LOdLIjQO.wgLtpuJJQoiprFARksarm5x6v2wgzy','customer','Kiril','Belchinski','k@abv.bg','Germanea',1,12,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouses`
--

DROP TABLE IF EXISTS `warehouses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(60) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_Warehouses_Countries1_idx` (`country_id`),
  KEY `fk_Warehouses_Cities1_idx` (`city_id`),
  CONSTRAINT `fk_Warehouses_Cities1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouses`
--

LOCK TABLES `warehouses` WRITE;
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT INTO `warehouses` VALUES (4,'Цар Самуил. 22',1,1,1),(5,'Искър',1,1,0),(6,'Позитано',1,1,1),(7,'Солунска',1,1,1),(8,'Иван Богоров',1,14,1),(9,'Г. Баев',1,14,1),(10,'Васил Левски',1,14,0),(11,'Тодор Каблешков',1,15,0),(12,'Тунджа',1,15,1),(13,'Иван Вазов',1,16,0),(14,'Св. Климент Охридски',1,17,0),(15,'бул. Европа',1,18,0),(16,'Avenida Diagonal',3,19,0),(17,'Passage de Gracia',3,19,1),(18,'C. de la Cava Baja',3,20,1),(19,'C. de Lope de Vega',3,20,0),(20,'C. de las Huertas',3,20,1),(21,'C. Martin de Gainza',3,21,0),(22,'C. Anden',3,21,1),(23,'C. Calisto',3,21,1),(24,'C. Calisto',3,22,0),(25,'Av.de Andaluciq',3,23,0),(26,'Патриарх Евтимий',1,24,0),(27,'Оборище',1,25,0),(28,'germaneq',1,12,0);
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'deliverit_database'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-29 11:50:38
