import express from 'express';
import countriesService from './countries-service.js';

const countriesController = express.Router();
import { authMiddleware } from '../authentication/auth-middleware.js';

countriesController.get('/', authMiddleware, async (req, res) => {
  const countries = await countriesService.getCountries();

  if (countries) {
    return res.status(200).send(countries);
  }

  return res
    .status(200)
    .send({ message: 'No countries found in the database!' });
});

export default countriesController;
