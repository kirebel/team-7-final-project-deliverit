import pool from '../pool.js';

/**
 * Makes sql request to the database for getting all the countries in it
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {object} => an object with information
 * of the country (country_id and country_name)
 */

const getCountries = async () => {
  const sql = `
    SELECT countries.id as country_id, countries.name as country_name
    from countries
    `;
  return await pool.query(sql);
};

export default {
  getCountries,
};
