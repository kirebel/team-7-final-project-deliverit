import countriesData from './countries-data.js';

/**
 * Activate sql request function from countriesData module
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {closure} - function that calls getCountries function
 * from the countriesData module
 */

const getCountries = async () => {
  return await countriesData.getCountries();
};

export default {
  getCountries,
};
