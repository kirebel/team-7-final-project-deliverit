/**
 * Activate sql request function from categoriesData module
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {closure} - function that calls getAllCategories function
 * from the categoriesData module
 */

const getCategories = (categoriesData) => {
  return async () => {
    const result = await categoriesData.getAllCategories();
    return result;
  };
};

export default {
  getCategories,
};
