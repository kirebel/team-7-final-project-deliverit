import express from 'express';
import categoriesData from './categories-data.js';
import categoriesService from './categories-service.js';
import { authMiddleware } from '../authentication/auth-middleware.js';

const categoriesController = express.Router();

categoriesController.get('/', authMiddleware, async (req, res) => {
  const categories = await categoriesService.getCategories(categoriesData)();

  res.status(200).json(categories);
});

export default categoriesController;
