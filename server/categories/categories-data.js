import pool from '../pool.js';

/**
 * Makes sql request to the database for getting all existing categories
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {array} => an array with the categories info
 */

const getAllCategories = async () => {
  const sql = `SELECT *
  FROM categories`;

  const result = await pool.query(sql);
  return result;
};

export default {
  getAllCategories,
};
