import express from 'express';

const citiesController = express.Router();
import { authMiddleware } from '../authentication/auth-middleware.js';
import citiesService from './cities-service.js';

citiesController
  .get('/', authMiddleware, async (req, res) => {
    const cities = await citiesService.getCities();

    if (cities) {
      return res.status(200).send(cities);
    }

    return res
      .status(200)
      .send({ message: 'No cities found in the database!' });
  })

  .post('/', async (req, res) => {
    const { city, country } = req.body;

    const createCity = await citiesService.createCity(city, country);

    return res.status(200).send(createCity);
  });

export default citiesController;
