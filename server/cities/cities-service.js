import citiesData from './cities-data.js';

/**
 * Activate sql request function from citiesData module
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {closure} - function that calls getCities function
 * from the citiesData module
 */

const getCities = async () => {
  return await citiesData.getCities();
};

/**
 * Activate sql request function from citiesData module and checking
 * if the city already exists
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} city The city name
 * @param {string} country The country name
 * @return {closure} - function that calls createCities function
 * from the citiesData module
 */

const createCity = async (city, country) => {
  const checkIfCityExists = Object.values(await citiesData.getCity(city))[0];
  if (checkIfCityExists.city_id) {
    return checkIfCityExists;
  }
  return await citiesData.createCities(city, country);
};

export default {
  getCities,
  createCity,
};
