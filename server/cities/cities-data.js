import pool from '../pool.js';

/**
 * Makes sql request to the database for getting all the cities in it
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {object} => an object with information
 * of the cities (city_id, city_name, city_country)
 */

const getCities = async () => {
  const sql = `
    SELECT cities.id as city_id, cities.name as city_name, countries.name as country_name
    FROM cities
    JOIN countries ON cities.country_id = countries.id 
    `;

  return await pool.query(sql);
};

/**
 * Makes sql request to the database for getting specific city by its name
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} city The city name
 * @return {object} => an object with information
 * of the city (city_id, city_name, city_country)
 */
const getCity = async (city) => {
  const sql = `
    SELECT cities.id as city_id, cities.name as city_name, countries.name as country_name
    FROM cities
    JOIN countries ON cities.country_id = countries.id 
    WHERE cities.name = ?
    `;

  return await pool.query(sql, [city]);
};

/**
 * Makes sql request to the database for creating new city
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} city The city name
 * @param {string} country The country name
 * @return {object} => an object with information
 * of the newly created city (city_id, city_name, city_country)
 */

const createCities = async (city, country) => {
  const sqlCountryId = `
  SELECT countries.id as country_id
  FROM countries
  WHERE countries.name = ?`;

  const getCountryId = await pool.query(sqlCountryId, [country]);

  const sqlCreateCity = `
  INSERT INTO cities(name, country_id)
  VALUES (?,? )
  `;

  const createCity = await pool.query(sqlCreateCity, [
    city,
    getCountryId[0].country_id,
  ]);

  return {
    city_id: createCity.insertId,
    city_name: city,
    country_name: country,
  };
};
export default {
  getCities,
  createCities,
  getCity,
};
