import pool from '../pool.js';

/**
 * Makes sql request to the database for getting all orders
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {array} => an array with all the orders
 */

const getAllOrders = async () => {
  const sql = `SELECT *
  FROM orders
  WHERE isDone =0`;

  const result = await pool.query(sql);
  return result;
};

/**
 * Makes sql request to the database for
 * updating order's isDone
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id- the id of the order
 * @return {object} => an object with the updated information
 */

const update = async (id) => {
  const sql = `UPDATE
    orders
    SET isDone =1
    WHERE id = ?`;

  const result = await pool.query(sql, [id]);

  return result;
};

/**
 * Makes sql request to the database for getting
 * an information of an order by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id - the id of the order
 * @return {object} => an object with the information
 * about the order
 */

const getById = async (id) => {
  const sql = `
 SELECT *
 FROM orders 
 WHERE id = ?
 AND isDone = 0
 `;
  const result = await pool.query(sql, [id]);

  return result[0];
};

export default {
  getAllOrders,
  update,
  getById,
};
