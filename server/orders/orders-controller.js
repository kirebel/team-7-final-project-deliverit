import express from 'express';
import ordersData from './orders-data.js';
import ordersService from './orders-service.js';
import serviceErrors from '../common/service-errors.js';
import { authMiddleware } from '../authentication/auth-middleware.js';

const ordersController = express.Router();

ordersController
  .get('/', authMiddleware, async (req, res) => {
    const orders = await ordersService.getOrders(ordersData)();

    res.status(200).json(orders);
  })

  .put('/:id', async (req, res) => {
    const id = +req.params.id;

    const { error, order } = await ordersService.updateOrder(ordersData)(id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).json({
        error: 'User not found.',
        detail: {
          id: 'User with this id does not exist!',
        },
      });
    } else {
      res.status(200).json(order);
    }
  });

export default ordersController;
