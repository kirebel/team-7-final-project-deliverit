import serviceErrors from '../common/service-errors.js';

/**
 * Activate sql request function from ordersData module
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {closure} - function that calls getAllOrders function
 * from the ordersData module
 */

const getOrders = (ordersData) => {
  return async () => {
    const result = await ordersData.getAllOrders();
    return result;
  };
};

/**
 * Active sql request function from ordersData module for
 * updating  order's information
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} ordersData- the module that includes all sql requests
 * @return {closure} - function that calls the getById function from
 * the ordersData module.If the order exist,update
 * function is called for updating the information
 */

const updateOrder = (ordersData) => {
  return async (id) => {
    const order = await ordersData.getById(id);

    if (!order) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        order: null,
      };
    }

    const updatedOrder = { ...order, isDone: 1 };

    const _ = await ordersData.update(id);

    return { error: null, order: updatedOrder };
  };
};

export default {
  getOrders,
  updateOrder,
};
