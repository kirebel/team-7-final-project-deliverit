import serviceErrors from '../common/service-errors.js';
import { parcelsQueryParamsValidator } from '../validators/parcels-search.validator.js';
import { parcelsSortDataProcessing } from '../common/parcels-sort-helper.js';
/**
 * Active sql request function from parcelsData module for
 * searching a parcel about specific criteria
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} parcelsData- the module that includes all sql requests
 * @return {closure} - function that accepts an object of query parameters
 * and their values then getParcelsTableColumnsAndArrivalTime function is called and
 * if there is no error in the next few lines(generating Sql script for searching)
 * searchBy function is called(results from this function are returned).
 * If there is an error - error is returned.
 */

const getParcels = (parcelsData) => {
  return async (queryObject) => {
    const searchTableColumns =
      await parcelsData.getParcelsTableColumnsAndArrivalTime();
    if (
      !Object.keys(queryObject).every(
        (element) =>
          searchTableColumns.includes(element) ||
          element === 'sort' ||
          element === 'user'
      ) ||
      !parcelsQueryParamsValidator(queryObject)
    ) {
      return {
        error: serviceErrors.BAD_REQUEST,
        parcels: null,
      };
    }

    const searchSql = Object.entries(queryObject).reduce((acc, el) => {
      if (el[0] === 'sort') {
        acc += '';
      } else {
        if (el[0] === 'weight') {
          const [minPrice, maxPrice] = el[1].split('_');
          if (!minPrice || !maxPrice || isNaN(maxPrice) || isNaN(minPrice)) {
            acc += ` AND  p.${el[0]} BETWEEN 0 AND 1000 `;
          } else {
            acc += ` AND  p.${el[0]} BETWEEN ${minPrice} AND ${maxPrice} `;
          }
        } else if (el[0] === 'user') {
          acc += ` AND u.username LIKE '%${el[1]}%'`;
        } else {
          acc += ` AND p.${el[0]}=${+el[1]}`;
        }
      }

      return acc;
    }, '');

    const result = await parcelsData.searchBy(
      searchSql + parcelsSortDataProcessing(queryObject.sort)
    );

    return {
      error: null,
      parcels: result,
    };
  };
};

/**
 * Active sql request function from parcelsData module for
 * creating a new parcel
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} parcelsData- the module that includes all sql requests
 * @return {closure} - function that calls create function
 * of the parcelsData module and creates new parcel
 */

const createParcel = (parcelsData) => {
  return async (parcelData) => {
    const parcel = await parcelsData.create(parcelData);
    return parcel;
  };
};

/**
 * Active sql request function from parcelsData module for
 * getting parcel information searching for the parcel by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} parcelsData- the module that includes all sql requests
 * @return {closure} - function that calls the getById function from
 * the parcelsData module. If the parcel exists its info is
 * returned, if it is not - error is returned
 */

const getParcelById = (parcelsData) => {
  return async (id) => {
    const parcel = await parcelsData.getById(id);

    if (!parcel) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        parcel: null,
      };
    }
    return { parcel: parcel, error: null };
  };
};

/**
 * Active sql request function from parcelsData module for
 * updating parcels information
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} parcelsData- the module that includes all sql requests
 * @return {closure} - function that calls the getById function from
 * the parcelsData module.If the parcel exist,update
 * function is called for updating the information
 */

const updateParcel = (parcelsData) => {
  return async (updateData, parcelId) => {
    const parcel = await parcelsData.getById(parcelId);

    if (!parcel) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        parcel: null,
      };
    }

    const updatedParcel = { ...parcel, ...updateData };

    const _ = await parcelsData.update(updatedParcel, parcelId);

    return { error: null, parcel: updatedParcel };
  };
};

/**
 * Active sql request function from parcelsData module for
 * deleting a parcel
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {module} parcelsData- the module that includes all sql requests
 * @return {closure} - function that calls the getById function from
 * the parcelsData module.If the parcel exist, remove function
 * is called for deleting the parcel
 */

const deleteParcel = (parcelsData) => {
  return async (id) => {
    const parcelToDelete = await parcelsData.getById(id);

    if (!parcelToDelete) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        parcel: null,
      };
    }

    const _ = await parcelsData.remove(parcelToDelete.id);

    return { error: null, parcel: parcelToDelete };
  };
};

export default {
  createParcel,
  getParcelById,
  deleteParcel,
  updateParcel,
  getParcels,
};
