import { IS_DELETED_FALSE, IS_DELETED_TRUE } from '../common/constants.js';
import pool from '../pool.js';

/**
 * Makes sql request to the database for
 * searching parcels about specific criteria dynamically
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} searchSql- the Sql script generated
 * dynamically(the searching criteria)
 * @return {array} =>an array that includes all
 * parcels that meets the searching criteria
 */

const searchBy = async (searchSql) => {
  const sql = `
    SELECT p.id, p.weight,p.category_id,p.user_id,p.home_delivery,(SELECT username FROM user WHERE id = p.user_id) as user,p.shipment_id,p.warehouse_id,
    (SELECT name FROM categories WHERE id =p.category_id) as category, p.isDeleted,
    (SELECT arrival_time from shipments WHERE id = p.shipment_id) as arrival_time,
    (SELECT departure_time from shipments WHERE id = p.shipment_id) as sent_date,
    (SELECT name from cities WHERE id = u.city_id) as customer_city,
    (SELECT name from countries WHERE id = u.country_id) as customer_country,
    (SELECT name from shipmentstatus WHERE id= (SELECT shipment_status FROM shipments WHERE id = p.shipment_id) ) as status
    FROM parcels as p
    JOIN user as u on u.id = p.user_id
    WHERE p.isDeleted = ?
${searchSql}
`;

  return await pool.query(sql, [IS_DELETED_FALSE]);
};
/**
 * Makes sql request to the database for
 * getting all column names of user table
 * + arrival_time column name from shipments
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {array} => an array with all the names
 * of the user table
 */

const getParcelsTableColumnsAndArrivalTime = async () => {
  const sql = `SELECT p.id, p.weight,p.home_delivery,p.user_id,p.shipment_id,p.warehouse_id,
    p.category_id, p.isDeleted, s.arrival_time
    FROM parcels as p
    JOIN shipments as s on s.id = p.shipment_id
   `;

  const result = await pool.query(sql);

  return result
    ? Object.keys(result[0])
    : [
        'weight',
        'home_delivery',
        'user_id',
        'shipment_id',
        'warehouse_id',
        'category_id',
        'isDeleted',
        'arrival_time',
      ];
};

/**
 * Makes sql request to the database for creating
 * a new user
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} data - the data of the parcel created
 * @return {object} => an object with the information
 * of the created parcel
 */

const create = async (data) => {
  const {
    weight,
    home_delivery,
    user_id,
    shipment_id,
    warehouse_id,
    category_id,
  } = data;

  const sql = `INSERT INTO 
parcels (weight,home_delivery,user_id,shipment_id,warehouse_id,category_id)
VALUES (?,?,?,?,?,?)`;

  const createdParcel = await pool.query(sql, [
    weight,
    home_delivery,
    user_id,
    shipment_id,
    warehouse_id,
    category_id,
  ]);

  const returnSql = `SELECT p.id,p.weight,p.home_delivery,
  (SELECT username from user WHERE id= p.user_id ) as user,
  p.shipment_id,
  p.warehouse_id,
  p.category_id,
  p.user_id,
  (SELECT name FROM categories where id = p.category_id) as category
   FROM parcels as p WHERE p.id = ?`;

  const result = await pool.query(returnSql, [createdParcel.insertId]);
  return result[0];
};
/**
 * Makes sql request to the database for getting
 * an information of a parcel by id
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id - the id of the parcel
 * or the id of the user which information is needed
 * @return {object} => an object with the information
 * about the parcel
 */

const getById = async (id) => {
  const sql = `SELECT *
    FROM parcels
    WHERE id = ?
    AND isDeleted = ?`;

  const result = await pool.query(sql, [id, IS_DELETED_FALSE]);

  return result[0];
};

/**
 * Makes sql request to the database for
 * deleting a parcel
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} id- the id of the parcel
 * which has to be deleted
 * @return {object} => an object with the information
 * about request made(affected rows, insert id ect.)
 */

const remove = async (id) => {
  const sql = `UPDATE
    parcels
    SET isDeleted = ?
    WHERE id=?`;

  const result = await pool.query(sql, [IS_DELETED_TRUE, id]);

  return result;
};

/**
 * Makes sql request to the database for
 * updating parcel's information
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} updateData- the update data of the parcel
 * @param {object} parcelId- the id of the parcel
 * @return {object} => an object with the information
 * about the updated parcel
 */

const update = async (updateData, parcelId) => {
  const {
    weight,
    home_delivery,
    user_id,
    shipment_id,
    warehouse_id,
    category_id,
  } = updateData;

  const sql = `
      UPDATE parcels
      SET weight=?,
      home_delivery = ?,
      user_id =?,
      shipment_id =  ?,
      warehouse_id = ?,
      category_id = ?
      WHERE id =?
      AND isDeleted= ?
      `;

  const result = await pool.query(sql, [
    weight,
    home_delivery,
    user_id,
    shipment_id,
    warehouse_id,
    category_id,
    parcelId,
    IS_DELETED_FALSE,
  ]);

  return result;
};

export default {
  create,
  getById,
  remove,
  update,
  searchBy,
  getParcelsTableColumnsAndArrivalTime,
};
