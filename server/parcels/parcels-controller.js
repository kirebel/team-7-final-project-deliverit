import express from 'express';

import parcelsData from './parcels-data.js';
import serviceErrors from '../common/service-errors.js';
import parcelsService from './parcels-service.js';

import {
  authMiddleware,
  roleMiddleware,
} from '../authentication/auth-middleware.js';
import { createParcelSchema } from '../validators/create-parcel.schema.js';
import { createValidator } from '../middleware/create.validator.js';

const parcelsController = express.Router();

parcelsController
  .get('/', authMiddleware, async (req, res) => {
    const queryObject = req.query;

    const { error, parcels } = await parcelsService.getParcels(parcelsData)(
      queryObject
    );

    if (error === serviceErrors.BAD_REQUEST) {
      res.status(400).json({
        error: 'Bad request.',
        detail: {
          query_parameters:
            'Wrong query parameters or their values. Check server documentation for more information',
        },
      });
    } else {
      res.status(200).json(parcels);
    }
  })

  .post(
    '/',
    authMiddleware,
    roleMiddleware('employee'),
    createValidator(createParcelSchema),
    async (req, res) => {
      const parcelData = req.body;

      const parcel = await parcelsService.createParcel(parcelsData)(parcelData);

      res.status(200).json(parcel);
    }
  )
  .get('/:id', authMiddleware, async (req, res) => {
    const parcelId = +req.params.id;

    if (isNaN(parcelId)) {
      res.status(400).json({
        error: 'Bad request.',
        detail: {
          resource: 'No resource found.',
        },
      });
    }

    const { error, parcel } = await parcelsService.getParcelById(parcelsData)(
      parcelId
    );

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).json({
        error: 'Parcel not found.',
        detail: {
          id: 'Parcel with this id does not exist!',
        },
      });
    } else {
      res.status(200).json(parcel);
    }
  })

  .delete(
    '/:id',
    authMiddleware,
    roleMiddleware('employee'),
    async (req, res) => {
      const parcelId = +req.params.id;

      const { error, parcel } = await parcelsService.deleteParcel(parcelsData)(
        parcelId
      );

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).json({
          error: 'Parcel not found.',
          detail: {
            id: 'Parcel with this id does not exist!',
          },
        });
      } else {
        res.status(200).json(parcel);
      }
    }
  )
  //32542

  .put(
    '/:id',
    authMiddleware,
    roleMiddleware('employee'),
    createValidator(createParcelSchema),
    async (req, res) => {
      const parcelId = +req.params.id;

      const updateInfo = req.body;

      const { error, parcel } = await parcelsService.updateParcel(parcelsData)(
        updateInfo,
        parcelId
      );

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).json({
          error: 'Parcel not found.',
          detail: {
            id: 'Parcel with this id does not exist!',
          },
        });
      } else {
        res.status(200).json(parcel);
      }
    }
  );
export default parcelsController;
