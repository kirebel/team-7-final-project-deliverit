import { IS_DELETED_FALSE, IS_DELETED_TRUE } from '../common/constants.js';
import pool from '../pool.js';

/**
 * Makes sql request to get Shipment info by given parameter
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} column - the parameter name
 * @param {string} value - value of the parameter
 * @return {object} => an object with the full info of the shipment
 */

const getShipmentsBy = async (column, value) => {
  const sql = `
  SELECT s.id as shipment_id, s.isDeleted,s.arrival_time as arrival_time,
  s.departure_time  as departure_time, 
  s.shipment_status as status_code,
  shipmentstatus.id as shipment_status,
  s.origin_warehouse_id, s.destination_warehouse_id,
  (SELECT name from cities where id = (Select city_id from warehouses WHERE id = s.destination_warehouse_id ) ) as to_city_name,
  (SELECT name from countries where id = (Select country_id from warehouses WHERE id = s.destination_warehouse_id ) ) as to_country_name,
  (SELECT count(id) FROM parcels WHERE shipment_id = s.id AND isDeleted=0) as parcels_count,
  (SELECT name from cities where id = (Select city_id from warehouses WHERE id = s.origin_warehouse_id ) ) as from_city_name,
  (SELECT name from countries where id = (Select country_id from warehouses WHERE id = s.origin_warehouse_id ) ) as from_country_name

 
  FROM shipments as s
  JOIN shipmentstatus ON s.shipment_status = shipmentstatus.id
  WHERE s.isDeleted = ? AND s.${column} ${value}
      `;

  return await pool.query(sql, [IS_DELETED_FALSE]);
};

/**
 * Makes sql request to filter all shipments for specific user
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} userId - the column name
 * @return {object} => an object with all the shipments where the user is included
 */

const getShipmentsByUser = async (username) => {
  const sql = `
  SELECT DISTINCT  u.username as user_username,
  s.id as shipment_id, s.arrival_time as shipment_arrival_time,
  s.departure_time  as shipment_departure_time, 
  shipmentstatus.id as shipment_status,
  s.origin_warehouse_id, s.destination_warehouse_id,
  (SELECT name from cities where id = (Select city_id from warehouses WHERE id = s.destination_warehouse_id ) ) as to_city_name,
  (SELECT name from countries where id = (Select country_id from warehouses WHERE id = s.destination_warehouse_id ) ) as to_country_name,
  (SELECT count(id) FROM parcels WHERE shipment_id = s.id) as parcels_count,
  (SELECT name from cities where id = (Select city_id from warehouses WHERE id = s.origin_warehouse_id ) ) as from_city_name,
  (SELECT name from countries where id = (Select country_id from warehouses WHERE id = s.origin_warehouse_id ) ) as from_country_name
  FROM shipments as s
  JOIN parcels as p on p.shipment_id = s.id
  JOIN user as u on u.id = p.user_id
  JOIN shipmentstatus ON s.shipment_status = shipmentstatus.id
  WHERE s.isDeleted = ? AND u.username  LIKE '%${username}%'`;

  return await pool.query(sql, [IS_DELETED_FALSE, username]);
};

/**
 * Makes sql request to create a new shipment
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} shipmentData - full info of the new shipment
 * (departure_time,shipment_status,origin_warehouse_id,destination_warehouse_id)
 * @return {object} => an object with info of the newly created shipment
 */

const createShipments = async (shipmentData) => {
  const { shipment_status, origin_warehouse_id, destination_warehouse_id } =
    shipmentData;
  const sql = `
  INSERT INTO shipments( shipment_status,
    origin_warehouse_id, destination_warehouse_id)
  VALUES ( ?, ?, ?)`;

  const creatingShipment = await pool.query(sql, [
    shipment_status,
    origin_warehouse_id,
    destination_warehouse_id,
  ]);

  const createdShipment = getShipmentsBy('id', `=${creatingShipment.insertId}`);
  return createdShipment;
};

/**
 * Makes sql request to update a new shipment
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} shipmentData - full info of the new shipment
 * (departure_time,shipment_status,origin_warehouse_id,destination_warehouse_id)
 * @param {number} shipmentId - ID of the shipment to update
 * @return {object} => an object with info of the updated shipment
 */
const updateShipment = async (shipmentData, shipmentId) => {
  const {
    departure_time,
    shipment_status,
    origin_warehouse_id,
    destination_warehouse_id,
    arrival_time,
  } = shipmentData;

  const sql = `
    UPDATE shipments 
    SET 
        departure_time = ?,
        shipment_status = ?,
        origin_warehouse_id = ?,
        destination_warehouse_id = ?,   
        arrival_time = ?
    WHERE
       id = ?
      `;
  const updatingShipment = await pool.query(sql, [
    departure_time,
    shipment_status,
    origin_warehouse_id,
    destination_warehouse_id,
    arrival_time,
    shipmentId,
  ]);

  const updatedShipment = await getShipmentsBy('id', `= ${shipmentId}`);

  return updatedShipment;
};

/**
 * Makes sql request to delete a new shipment
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} shipmentId - ID of the shipment to delete
 * @return {object} => an object with info of the deleted shipment
 */
const deleteShipment = async (shipmentId) => {
  const sql = `
    UPDATE shipments 
    SET 
      isDeleted = ?
    WHERE id = ?
    `;

  const deletedShipment = await getShipmentsBy('id', `= ${shipmentId}`);
  const deletingShipment = await pool.query(sql, [IS_DELETED_TRUE, shipmentId]);

  return deletedShipment;
};
export default {
  getShipmentsBy,
  createShipments,
  updateShipment,
  deleteShipment,
  getShipmentsByUser,
};
