import shipmentsData from './shipments-data.js';

/**
 * Activating sql request functions from shipmentData module depending on
 * the provided query parameter
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} queryObject - the query object
 * @return {closure} - function that calls getShipmentsBy function
 * from the shipmentData module
 */
const getShipmentsBy = async (queryObject) => {
  if (Object.keys(queryObject).length !== 0) {
    if (
      Object.keys(queryObject)[0] === 'destination_warehouse_id' &&
      isNaN(+Object.values(queryObject)[0])
    ) {
      return {
        error: 'Query parameter is not valid!',
      };
    }

    if (queryObject.destination_warehouse_id) {
      const shipments = await shipmentsData.getShipmentsBy(
        'destination_warehouse_id',
        `= ${queryObject.destination_warehouse_id}`
      );

      return shipments;
    } else if (queryObject.user) {
      const shipments = await shipmentsData.getShipmentsByUser(
        queryObject.user
      );

      return shipments;
    }
  }
  const shipments = await shipmentsData.getShipmentsBy('id', '');
  return shipments;
};

/**
 * Activating sql request function to create new shipment
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} shipmentData - the needed data to create a shipment
 * @return {closure} - function that calls createShipments function
 * from the shipmentData module
 */
const createShipment = async (shipmentData) => {
  return await shipmentsData.createShipments(shipmentData);
};

/**
 * Checking if the provided shipmentId is correct and
 * activating sql request function to update shipment
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} shipmentData - the needed data to create a shipment
 * @param {number} shipmentId - ID of the shipment to update
 * @return {closure} - function that calls updateShipment function
 * from the shipmentData module
 */

const updateShipment = async (shipmentData, shipmentId) => {
  const checkIfValidId = await shipmentsData.getShipmentsBy(
    'id',
    `= ${shipmentId}`
  );
  if (!checkIfValidId[0]) {
    return {
      error: 'Shipment with the given id does not exists!',
      value: null,
    };
  }

  const updatedShipment = await shipmentsData.updateShipment(
    shipmentData,
    shipmentId
  );
  return { error: null, value: updatedShipment };
};

/**
 * Checking if the provided shipmentId is correct and
 * activating sql request function to delete shipment
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} shipmentId - ID of the shipment to delete
 * @return {closure} - function that calls deleteShipment function
 * from the shipmentData module
 */

const deleteShipment = async (shipmentId) => {
  const checkIfValidId = await shipmentsData.getShipmentsBy(
    'id',
    `= ${shipmentId}`
  );

  if (!checkIfValidId[0]) {
    return {
      error: 'Shipment with the given id does not exists!',
      value: null,
    };
  }

  const deletedShipment = await shipmentsData.deleteShipment(shipmentId);
  return { error: null, value: deletedShipment };
};

export default {
  getShipmentsBy,
  createShipment,
  updateShipment,
  deleteShipment,
};
