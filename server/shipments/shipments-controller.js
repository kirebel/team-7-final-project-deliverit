import express, { query } from 'express';
import {
  authMiddleware,
  roleMiddleware,
} from '../authentication/auth-middleware.js';
import { createValidator } from '../middleware/create.validator.js';
import { shipmentSchema } from '../validators/shipments.js';
import shipmentsService from './shipments-service.js';

const shipmentsController = express.Router();

shipmentsController
  .get('/', authMiddleware, async (req, res) => {
    const queryObject = req.query;
    const shipments = await shipmentsService.getShipmentsBy(queryObject);

    if (shipments.error) {
      res.status(400).send(shipments);
    }
    res.status(200).send(shipments);
  })

  .post(
    '/',
    authMiddleware,
    createValidator(shipmentSchema),

    async (req, res) => {
      const shipmentData = req.body;
      const createdShipment = await shipmentsService.createShipment(
        shipmentData
      );
      return res.status(201).json(createdShipment);
    }
  )

  .put(
    '/:shipmentId',
    authMiddleware,
    createValidator(shipmentSchema),
    roleMiddleware('employee'),
    async (req, res) => {
      const { shipmentId } = req.params;
      const shipmentData = req.body;

      const updatedShipment = await shipmentsService.updateShipment(
        shipmentData,
        +shipmentId
      );
      if (updatedShipment.error) {
        return res.status(400).json({ error: updatedShipment.error });
      }

      return res.status(201).json(updatedShipment.value);
    }
  )
  .delete(
    '/:shipmentId',
    authMiddleware,
    roleMiddleware('employee'),
    async (req, res) => {
      const { shipmentId } = req.params;
      const deletedShipment = await shipmentsService.deleteShipment(shipmentId);

      if (deletedShipment.error) {
        return res.status(400).json({ error: deletedShipment.error });
      }
      return res.status(201).send(deletedShipment.value);
    }
  );

export default shipmentsController;
