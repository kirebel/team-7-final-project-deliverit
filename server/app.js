import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './Authentication/strategy.js';

import usersController from './users/users-controller.js';
import authController from './authentication/auth-controller.js';
import countriesController from './countries/countries-controller.js';
import citiesController from './cities/cities-controller.js';
import warehousesController from './warehouses/warehouse-controller.js';
import shipmentsController from './shipments/shipments-controller.js';
import parcelsController from './parcels/parcels-controller.js';

import errorMiddleware from './middleware/error-middleware.js';
import categoriesController from './categories/categories-controller.js';
import ordersController from './orders/orders-controller.js';

const app = express();

const config = dotenv.config().parsed;
const PORT = +config.PORT;

passport.use(jwtStrategy);
app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(passport.initialize());

app.use('/users', usersController);
app.use('/authentication', authController);
app.use('/countries', countriesController);
app.use('/cities', citiesController);
app.use('/warehouses', warehousesController);
app.use('/shipments', shipmentsController);
app.use('/parcels', parcelsController);
app.use('/categories', categoriesController);
app.use('/orders', ordersController);

app.all('*', (req, res) =>
  res.status(400).json({ message: 'Resource not found!' })
);

app.use(errorMiddleware);

app.listen(PORT, () => console.log(`App is listening ar port ${PORT}`));
