/**
 * A middleware that checks if the data that is getting
 * uploaded covers a specific criteria
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} req - the request parameter
 * @param {object} res - the response parameter
 * @param {function} next - function that activates the
 * next middleware if the criteria is passed
 */

export const createValidator = (schema) => {
  return (req, res, next) => {
    const body = req.body;
    const validations = Object.keys(schema);

    const fails = validations
      .map((v) => schema[v](body[v]))
      .filter((e) => e !== null);

    if (fails.length > 0) {
      res.status(400).send(fails);
    } else {
      next();
    }
  };
};
