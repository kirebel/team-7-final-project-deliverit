import jwt from 'jsonwebtoken';
import { TOKEN_LIFETIME } from '../common/constants.js';
import { PRIVATE_KEY } from '../config/config-constants.js';

const createToken = (payload) => {
  const token = jwt.sign(payload, PRIVATE_KEY, { expiresIn: TOKEN_LIFETIME });

  return token;
};

export default createToken;
