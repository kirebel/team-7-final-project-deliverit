import express from 'express';
import createToken from '../authentication/create-token.js';
import usersService from '../users/users-service.js';
import usersData from '../users/users-data.js';
import serviceErrors from '../common/service-errors.js';

const authController = express.Router();

authController.post('/signin', async (req, res) => {
  const { username, password } = req.body;
  const { error, user } = await usersService.signInUser(usersData)(
    username,
    password
  );

  if (error === serviceErrors.INVALID_SIGHIN) {
    res.status(400).json({
      error: 'Invalid username/password',
    });
  } else {
    const payload = {
      id: user.id,
      username: user.username,
      role: user.role,
      country: user.country_name,
      country_id: user.country_id,
      city_id: user.city_id,
      city: user.city_name,
      street: user.street,
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
    };

    const token = createToken(payload);

    res.status(200).json({
      token: token,
    });
  }
});

export default authController;
