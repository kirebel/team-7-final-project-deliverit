import warehousesData from './warehouses-data.js';

/**
 * Activating sql request function from warehouseData module
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {closure} - function that calls getWarehouses function
 * from the wareHouseData module
 */

const getWarehouses = async () => {
  return await warehousesData.getWarehouses();
};

/**
 * Activating sql request function from warehouseData module
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} street - the name of the street
 * @param {string} country - the name of the country
 * @param {string} city - the name of the city
 * @return {closure} - function that calls findWarehouse function
 * from the wareHouseData module
 */
const findWarehouse = async (street, country, city) => {
  return await warehousesData.findWarehouse(street, country, city);
};

/**
 * Activating sql request function from warehouseData module
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} street - the name of the street
 * @param {string} country - the name of the country
 * @param {string} city - the name of the city
 * @return {closure} - function that calls createWarehouse function
 * from the wareHouseData module
 */
const createWarehouse = async (street, country, city) => {
  return await warehousesData.createWarehouse(street, country, city);
};

/**
 * Checking if the provided warehouseId is valid and if it is valid
 * activating sql request function from warehouseData module else
 * providing an error
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} warehouseId - the id of the warehouse
 * @param {string} street - the name of the street
 * @param {string} country - the name of the country
 * @param {string} city - the name of the city
 * @return {closure} - function that calls updateWarehouse function
 * from the wareHouseData module
 */
const updateWarehouse = async (warehouseId, street, country, city) => {
  const checkIfValidId = await warehousesData.findWarehouseById(warehouseId);
  if (!checkIfValidId[0]) {
    return {
      error: 'Warehouse with the given id does not exists!',
      value: null,
    };
  }
  const updatedWarehouse = await warehousesData.updateWarehouse(
    warehouseId,
    street,
    country,
    city
  );
  return { error: null, value: updatedWarehouse };
};

/**
 * Checking if the provided warehouseId is valid and if it is valid
 * activating sql request function from warehouseData module else
 * providing an error
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} warehouseId - the id of the warehouse
 * @return {closure} - function that calls deleteWarehouse function
 * from the wareHouseData module
 */

const deleteWarehouse = async (warehouseId) => {
  const checkIfValidId = await warehousesData.findWarehouseById(warehouseId);

  if (!checkIfValidId[0]) {
    return {
      error: 'Warehouse with the given id does not exists!',
      value: null,
    };
  }

  const deletedWarehouse = await warehousesData.deleteWarehouse(warehouseId);
  return { error: null, value: deletedWarehouse };
};

export default {
  getWarehouses,
  createWarehouse,
  findWarehouse,
  updateWarehouse,
  deleteWarehouse,
};
