import { IS_DELETED_FALSE, IS_DELETED_TRUE } from '../common/constants.js';
import {
  cityValueToId,
  countryValueToId,
  getWarehouseFullInfo,
} from '../common/data-helper.js';
import pool from '../pool.js';

/**
 * Makes sql request to the database for getting all the warehouses in it
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {object} => an object with information
 * of the warehouse (warehouse_id, warehouse_street, warehouse_city, warehouse_country)
 */

const getWarehouses = async () => {
  const sql = `
    ${getWarehouseFullInfo}
    WHERE w.isDeleted = ?
    `;

  return await pool.query(sql, [IS_DELETED_FALSE]);
};

/**
 * Makes sql request to find if a warehouse exists
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} street - the name of the street
 * @param {string} country - the name of the country
 * @param {string} city - the name of the city
 * @return {object} => an object with the full info of the warehouse
 */

const findWarehouse = async (street, country, city) => {
  const sql = `
  ${getWarehouseFullInfo}
  WHERE w.street = ? AND 
  w.country_id = ${countryValueToId(country)} AND
  w.city_id = ${cityValueToId(city)}
  AND w.isDeleted = ?
  `;

  return await pool.query(sql, [street, IS_DELETED_FALSE]);
};

/**
 * Makes sql request to find if a warehouse exists by iD
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} warehouseId - the id of the warehouse
 * @return {object} => an object with the full info of the warehouse
 */

const findWarehouseById = async (warehouseId) => {
  const sql = `
  ${getWarehouseFullInfo}
  WHERE w.id = ? AND w.isDeleted = ?
  `;

  return await pool.query(sql, [warehouseId, IS_DELETED_FALSE]);
};

/**
 * Makes sql request to create warehouse
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} street - the name of the street
 * @param {string} country - the name of the country
 * @param {string} city - the name of the city
 * @return {object} => an object with the full info of the newly created warehouse
 */

const createWarehouse = async (street, country, city) => {
  const sql = `
  INSERT INTO warehouses(street, country_id, city_id)
  VALUES (
  ?, 
  ${countryValueToId(country)},
  ${cityValueToId(city)}
  )`;

  const creatingWarehouse = await pool.query(sql, [street]);
  const createdWarehouse = await findWarehouse(street, country, city);
  return createdWarehouse;
};

/**
 * Makes sql request to update warehouse
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} warehouseId - the id of the warehouse
 * @param {string} street - the name of the street
 * @param {string} country - the name of the country
 * @param {string} city - the name of the city
 * @return {object} => an object with the full info of the updated warehouse
 */

const updateWarehouse = async (warehouseId, street, country, city) => {
  const sql = `
  UPDATE warehouses 
  SET 
      street = ?,
      country_id = ${countryValueToId(country)},
      city_id = ${cityValueToId(city)}

  WHERE
     id = ?
  `;

  const updatingWarehouse = await pool.query(sql, [street, warehouseId]);
  const updatedWarehouse = await findWarehouseById(warehouseId);

  return updatedWarehouse;
};

/**
 * Makes sql request to delete warehouse
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {number} warehouseId - the id of the warehouse
 * @return {object} => an object with the full info of the deleted warehouse
 */

const deleteWarehouse = async (warehouseId) => {
  const sql = `
  UPDATE warehouses 
  SET 
    isDeleted = ?
  WHERE id = ?
  `;

  const deletedWarehouse = await findWarehouseById(warehouseId);
  const deletingWarehouse = await pool.query(sql, [
    IS_DELETED_TRUE,
    warehouseId,
  ]);

  return deletedWarehouse;
};

export default {
  getWarehouses,
  createWarehouse,
  findWarehouse,
  updateWarehouse,
  findWarehouseById,
  deleteWarehouse,
};
