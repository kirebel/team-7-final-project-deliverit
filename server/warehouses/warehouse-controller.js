import express from 'express';
import {
  authMiddleware,
  roleMiddleware,
} from '../authentication/auth-middleware.js';
import { createValidator } from '../middleware/create.validator.js';
import { warehouseSchema } from '../validators/warehouse.js';
import warehouseService from './warehouse-service.js';

const warehousesController = express.Router();

warehousesController

  .get('/', async (req, res) => {
    const warehouses = await warehouseService.getWarehouses();

    if (warehouses) {
      return res.status(200).send(warehouses);
    }

    return res
      .status(200)
      .send({ message: 'No warehouses found in the database!' });
  })

  .post(
    '/',
    authMiddleware,
    roleMiddleware('employee'),
    createValidator(warehouseSchema),
    async (req, res) => {
      const { street, country, city } = req.body;

      const warehouseExists = await warehouseService.findWarehouse(
        street,
        country,
        city
      );

      if ([...warehouseExists].length !== 0) {
        return res.status(400).send({ error: 'Warehouse already exists!' });
      }

      const newWarehouse = await warehouseService.createWarehouse(
        street,
        country,
        city
      );

      return res.status(201).json(newWarehouse);
    }
  )

  .put(
    '/:warehouseId',
    authMiddleware,
    roleMiddleware('employee'),
    createValidator(warehouseSchema),
    async (req, res) => {
      const { warehouseId } = req.params;
      const { street, country, city } = req.body;

      const updatedWarehouse = await warehouseService.updateWarehouse(
        +warehouseId,
        street,
        country,
        city
      );
      if (updatedWarehouse.error) {
        return res.status(400).json({ error: updatedWarehouse.error });
      }

      return res.status(201).json(updatedWarehouse.value);
    }
  )

  .delete(
    '/:warehouseId',
    authMiddleware,
    roleMiddleware('employee'),
    async (req, res) => {
      const { warehouseId } = req.params;
      const deletedWarehouse = await warehouseService.deleteWarehouse(
        warehouseId
      );

      if (deletedWarehouse.error) {
        return res.status(400).json({ error: deletedWarehouse.error });
      }

      const deletedWarehouseStreet = deletedWarehouse.value[0].warehouse_street;
      const deletedWarehouseCity = deletedWarehouse.value[0].warehouse_city;
      const deletedWarehouseCountry =
        deletedWarehouse.value[0].warehouse_country;
      return res.status(201).send({
        message: `The warehouse on '${deletedWarehouseStreet}'/ ${deletedWarehouseCity} / ${deletedWarehouseCountry} was deleted successfully!`,
      });
    }
  );

export default warehousesController;
