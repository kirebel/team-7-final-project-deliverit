 # Team 7 final project - deliverIT


## 1. Description

This is a web application which is used for working with deliveries in Europe (Bulgaria and Spain for now).
The main focus of the application is to facilitate the work of the employees. The application has server part
(main server and http server used for live communication with socket.io npm package) and a client part. Working with deliveries
part - creating, modifying, searching parcels, shipments and warehouses. Users part - there are three types of 
users in the application - customers, employees and support (more information below). 

## 2. Workflow
 
### Employee  can:
* Registry new parcels
* Update and delete existing parcels
* Distribute parcels into shipments
* Create new shipment
* Dispatch a shipment
* Create and update warehouses
* Look at all of the users details and parcels
 
### **Support** can:
*  Answer the questions of the customers in the customer support chat. 
* Look at the parcels and shipments full information. 
 
### **Customer** can:
* View the information about ordered parcels
* Look  for the locations of warehouses via google maps
* Ask any questions and seek for help via support chat
* Update own account details

## 3. Project information
* Language and version: JavaScript ES2020
* Platform and version: Node 14.0+
* Core Packages: Express.js, React.js

## 4. Project architecture
* database part  - MariaDB server
* main-server part - build on Express.js package
* chat-room server part - build on Express.js package
* client part - build on React.js package

## 5. Main server documentation
* The main server is documented via Swagger. The location of the generated .json file
is: /server/documentation/main-server-documentation.json . For visualizing the documentation
in the browser you can visit this link: [https://app.swaggerhub.com/apis/Ivaylo-Garnev/deliverIT/1.0.0#/](https://app.swaggerhub.com/apis/Ivaylo-Garnev/deliverIT/1.0.0#/)
## 6. Setup
1. Import the database (/server/database.sql) to your database tool (MySQL Workbench, ect.)
2. Go inside the server folder.
3. Create a .env file with the following content:
```
PORT=5555
HOST=localhost
DB_PORT=3306
USER=root
PASSWORD= *Your MariaDB password*
DATABASE=deliverit_database
SECRET_KEY=s3cret_k3y
TOKEN_LIFETIME = "360000"
```
4. Run npm install to restore all dependencies
5. Go inside the chatRoom folder.
6. Run npm install to restore all dependencies.
7. Go inside the client folder.
8. Run npm install to restore all dependencies

9. Starting the main server: <br>
In the server folder run:
```
  npm run start (shorthand: npm start) - Runs the src/app.js file  - for normal start of the server
  npm run start:dev - Runs the src/app.js file - for developer mode start of the server
 ```
10. Starting the chat-room server: <br>
In the chatRoom folder run:
```
  npm run start (shorthand: npm start) - Runs the src/index.js file  - for normal start of the server
  npm run start:dev - Runs the src/index.js file - for developer mode start of the server
 ```
11. Starting the client:
```
In the client folder run:
 npm run start(shorthand :npm start) - Runs the src/main.js file 
```

12. To reveal the full functionality of the application you can use these accounts:

Employee account:
```
username: employee1
password: employee123*
```

Support account:
```
username: support
password: support123*
```
Customer account (default role when register):
```
username:customer1
password: customer123*
```
### Our team wishes you happy surfing and enjoy :)

