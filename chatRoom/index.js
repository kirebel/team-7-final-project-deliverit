import express from 'express';
import cors from 'cors';

import { createServer } from 'http';
import { Server } from 'socket.io';

const PORT = 4000;

const app = express();

app.use(express.json());

app.use(cors());

const httpServer = createServer(app);

const io = new Server(httpServer, {
  cors: true,
  origins: ['localhost:4000'],
});

const connections = new Map();

io.on('connection', (socket) => {
  socket.on('message', ({ username, message }) => {
    io.emit('message', { username, message });
  });

  socket.on('setUserInfo', (data) => {
    const { username } = data;
    connections.set(username, socket.client.id);
  });

  socket.on('disconnect', () => {
    for (const [key, value] of connections) {
      if (value === socket.client.id) {
        connections.delete(key);
      }
    }
  });
});

httpServer.listen(PORT, () => {
  console.log(`Socket.IO server running at http://localhost:${PORT}/`);
});
